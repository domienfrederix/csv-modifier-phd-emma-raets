package com.company;
public class CsvLine {

    private String SEX;
    private String AGE;
    private String TENORG;
    private String TENLG;
    private String FUNCTIE;
    private String DIPL;
    private String V1_1;
    private String V1_2;
    private String V1_3;
    private String V1_4;
    private String V1_5;
    private String V1_6;
    private String V1_7;
    private String V1_8;
    private String V1_9;
    private String V1_10;
    private String V2_1;
    private String V2_2;
    private String V2_3;
    private String V2_4;
    private String V2_5;
    private String V2_6;
    private String V2_7;
    private String V2_8;
    private String V2_9;
    private String V2_10;
    private String V3_1;
    private String V3_2;
    private String V3_3;
    private String V3_4;
    private String V3_5;
    private String V3_6;
    private String V3_7;
    private String V3_8;
    private String V3_9;
    private String V3_10;
    private String V4_1;
    private String V4_2;
    private String V4_3;
    private String V4_4;
    private String V4_5;
    private String V4_6;
    private String V4_7;
    private String V4_8;
    private String V4_9;
    private String V4_10;
    private String V5_1;
    private String V5_2;
    private String V5_3;
    private String V5_4;
    private String V5_5;
    private String V5_6;
    private String V5_7;
    private String V5_8;
    private String V5_9;
    private String V5_10;
    private String V6_1;
    private String V6_2;
    private String V6_3;
    private String V6_4;
    private String V6_5;
    private String V6_6;
    private String V6_7;
    private String V6_8;
    private String V6_9;
    private String V6_10;
    private String V7_1;
    private String V7_2;
    private String V7_3;
    private String V7_4;
    private String V7_5;
    private String V7_6;
    private String V7_7;
    private String V7_8;
    private String V7_9;
    private String V7_10;
    private String V8_1;
    private String V8_2;
    private String V8_3;
    private String V8_4;
    private String V8_5;
    private String V8_6;
    private String V8_7;
    private String V8_8;
    private String V8_9;
    private String V8_10;
    private String V9_1;
    private String V9_2;
    private String V9_3;
    private String V9_4;
    private String V9_5;
    private String V9_6;
    private String V9_7;
    private String V9_8;
    private String V9_9;
    private String V9_10;
    private String V10_1;
    private String V10_2;
    private String V10_3;
    private String V10_4;
    private String V10_5;
    private String V10_6;
    private String V10_7;
    private String V10_8;
    private String V10_9;
    private String V10_10;
    private String V11_1;
    private String V11_2;
    private String V11_3;
    private String V11_4;
    private String V11_5;
    private String V11_6;
    private String V11_7;
    private String V11_8;
    private String V11_9;
    private String V11_10;
    private String V12_1;
    private String V12_2;
    private String V12_3;
    private String V12_4;
    private String V12_5;
    private String V12_6;
    private String V12_7;
    private String V12_8;
    private String V12_9;
    private String V12_10;
    private String V13_1;
    private String V13_2;
    private String V13_3;
    private String V13_4;
    private String V13_5;
    private String V13_6;
    private String V13_7;
    private String V13_8;
    private String V13_9;
    private String V13_10;
    private String V14_1;
    private String V14_2;
    private String V14_3;
    private String V14_4;
    private String V14_5;
    private String V14_6;
    private String V14_7;
    private String V14_8;
    private String V14_9;
    private String V14_10;
    private String V15_1;
    private String V15_2;
    private String V15_3;
    private String V15_4;
    private String V15_5;
    private String V15_6;
    private String V15_7;
    private String V15_8;
    private String V15_9;
    private String V15_10;
    private String V16_1;
    private String V16_2;
    private String V16_3;
    private String V16_4;
    private String V16_5;
    private String V16_6;
    private String V16_7;
    private String V16_8;
    private String V16_9;
    private String V16_10;
    private String V17_1;
    private String V17_2;
    private String V17_3;
    private String V17_4;
    private String V17_5;
    private String V17_6;
    private String V17_7;
    private String V17_8;
    private String V17_9;
    private String V17_10;
    private String V18_1;
    private String V18_2;
    private String V18_3;
    private String V18_4;
    private String V18_5;
    private String V18_6;
    private String V18_7;
    private String V18_8;
    private String V18_9;
    private String V18_10;
    private String V19_1;
    private String V19_2;
    private String V19_3;
    private String V19_4;

    public static int followNumber = 1;

    public String getSEX() {
        return SEX;
    }

    public void setSEX(String SEX) {
        this.SEX = SEX;
    }

    public String getAGE() {
        return AGE;
    }

    public void setAGE(String AGE) {
        this.AGE = AGE;
    }

    public String getTENORG() {
        return TENORG;
    }

    public void setTENORG(String TENORG) {
        this.TENORG = TENORG;
    }

    public String getTENLG() {
        return TENLG;
    }

    public void setTENLG(String TENLG) {
        this.TENLG = TENLG;
    }

    public String getFUNCTIE() {
        return FUNCTIE;
    }

    public void setFUNCTIE(String FUNCTIE) {
        this.FUNCTIE = FUNCTIE;
    }

    public String getDIPL() {
        return DIPL;
    }

    public void setDIPL(String DIPL) {
        this.DIPL = DIPL;
    }

    public String getV1_1() {
        return V1_1;
    }

    public void setV1_1(String v1_1) {
        V1_1 = v1_1;
    }

    public String getV1_2() {
        return V1_2;
    }

    public void setV1_2(String v1_2) {
        V1_2 = v1_2;
    }

    public String getV1_3() {
        return V1_3;
    }

    public void setV1_3(String v1_3) {
        V1_3 = v1_3;
    }

    public String getV1_4() {
        return V1_4;
    }

    public void setV1_4(String v1_4) {
        V1_4 = v1_4;
    }

    public String getV1_5() {
        return V1_5;
    }

    public void setV1_5(String v1_5) {
        V1_5 = v1_5;
    }

    public String getV1_6() {
        return V1_6;
    }

    public void setV1_6(String v1_6) {
        V1_6 = v1_6;
    }

    public String getV1_7() {
        return V1_7;
    }

    public void setV1_7(String v1_7) {
        V1_7 = v1_7;
    }

    public String getV1_8() {
        return V1_8;
    }

    public void setV1_8(String v1_8) {
        V1_8 = v1_8;
    }

    public String getV1_9() {
        return V1_9;
    }

    public void setV1_9(String v1_9) {
        V1_9 = v1_9;
    }

    public String getV1_10() {
        return V1_10;
    }

    public void setV1_10(String v1_10) {
        V1_10 = v1_10;
    }

    public String getV2_1() {
        return V2_1;
    }

    public void setV2_1(String v2_1) {
        V2_1 = v2_1;
    }

    public String getV2_2() {
        return V2_2;
    }

    public void setV2_2(String v2_2) {
        V2_2 = v2_2;
    }

    public String getV2_3() {
        return V2_3;
    }

    public void setV2_3(String v2_3) {
        V2_3 = v2_3;
    }

    public String getV2_4() {
        return V2_4;
    }

    public void setV2_4(String v2_4) {
        V2_4 = v2_4;
    }

    public String getV2_5() {
        return V2_5;
    }

    public void setV2_5(String v2_5) {
        V2_5 = v2_5;
    }

    public String getV2_6() {
        return V2_6;
    }

    public void setV2_6(String v2_6) {
        V2_6 = v2_6;
    }

    public String getV2_7() {
        return V2_7;
    }

    public void setV2_7(String v2_7) {
        V2_7 = v2_7;
    }

    public String getV2_8() {
        return V2_8;
    }

    public void setV2_8(String v2_8) {
        V2_8 = v2_8;
    }

    public String getV2_9() {
        return V2_9;
    }

    public void setV2_9(String v2_9) {
        V2_9 = v2_9;
    }

    public String getV2_10() {
        return V2_10;
    }

    public void setV2_10(String v2_10) {
        V2_10 = v2_10;
    }

    public String getV3_1() {
        return V3_1;
    }

    public void setV3_1(String v3_1) {
        V3_1 = v3_1;
    }

    public String getV3_2() {
        return V3_2;
    }

    public void setV3_2(String v3_2) {
        V3_2 = v3_2;
    }

    public String getV3_3() {
        return V3_3;
    }

    public void setV3_3(String v3_3) {
        V3_3 = v3_3;
    }

    public String getV3_4() {
        return V3_4;
    }

    public void setV3_4(String v3_4) {
        V3_4 = v3_4;
    }

    public String getV3_5() {
        return V3_5;
    }

    public void setV3_5(String v3_5) {
        V3_5 = v3_5;
    }

    public String getV3_6() {
        return V3_6;
    }

    public void setV3_6(String v3_6) {
        V3_6 = v3_6;
    }

    public String getV3_7() {
        return V3_7;
    }

    public void setV3_7(String v3_7) {
        V3_7 = v3_7;
    }

    public String getV3_8() {
        return V3_8;
    }

    public void setV3_8(String v3_8) {
        V3_8 = v3_8;
    }

    public String getV3_9() {
        return V3_9;
    }

    public void setV3_9(String v3_9) {
        V3_9 = v3_9;
    }

    public String getV3_10() {
        return V3_10;
    }

    public void setV3_10(String v3_10) {
        V3_10 = v3_10;
    }

    public String getV4_1() {
        return V4_1;
    }

    public void setV4_1(String v4_1) {
        V4_1 = v4_1;
    }

    public String getV4_2() {
        return V4_2;
    }

    public void setV4_2(String v4_2) {
        V4_2 = v4_2;
    }

    public String getV4_3() {
        return V4_3;
    }

    public void setV4_3(String v4_3) {
        V4_3 = v4_3;
    }

    public String getV4_4() {
        return V4_4;
    }

    public void setV4_4(String v4_4) {
        V4_4 = v4_4;
    }

    public String getV4_5() {
        return V4_5;
    }

    public void setV4_5(String v4_5) {
        V4_5 = v4_5;
    }

    public String getV4_6() {
        return V4_6;
    }

    public void setV4_6(String v4_6) {
        V4_6 = v4_6;
    }

    public String getV4_7() {
        return V4_7;
    }

    public void setV4_7(String v4_7) {
        V4_7 = v4_7;
    }

    public String getV4_8() {
        return V4_8;
    }

    public void setV4_8(String v4_8) {
        V4_8 = v4_8;
    }

    public String getV4_9() {
        return V4_9;
    }

    public void setV4_9(String v4_9) {
        V4_9 = v4_9;
    }

    public String getV4_10() {
        return V4_10;
    }

    public void setV4_10(String v4_10) {
        V4_10 = v4_10;
    }

    public String getV5_1() {
        return V5_1;
    }

    public void setV5_1(String v5_1) {
        V5_1 = v5_1;
    }

    public String getV5_2() {
        return V5_2;
    }

    public void setV5_2(String v5_2) {
        V5_2 = v5_2;
    }

    public String getV5_3() {
        return V5_3;
    }

    public void setV5_3(String v5_3) {
        V5_3 = v5_3;
    }

    public String getV5_4() {
        return V5_4;
    }

    public void setV5_4(String v5_4) {
        V5_4 = v5_4;
    }

    public String getV5_5() {
        return V5_5;
    }

    public void setV5_5(String v5_5) {
        V5_5 = v5_5;
    }

    public String getV5_6() {
        return V5_6;
    }

    public void setV5_6(String v5_6) {
        V5_6 = v5_6;
    }

    public String getV5_7() {
        return V5_7;
    }

    public void setV5_7(String v5_7) {
        V5_7 = v5_7;
    }

    public String getV5_8() {
        return V5_8;
    }

    public void setV5_8(String v5_8) {
        V5_8 = v5_8;
    }

    public String getV5_9() {
        return V5_9;
    }

    public void setV5_9(String v5_9) {
        V5_9 = v5_9;
    }

    public String getV5_10() {
        return V5_10;
    }

    public void setV5_10(String v5_10) {
        V5_10 = v5_10;
    }

    public String getV6_1() {
        return V6_1;
    }

    public void setV6_1(String v6_1) {
        V6_1 = v6_1;
    }

    public String getV6_2() {
        return V6_2;
    }

    public void setV6_2(String v6_2) {
        V6_2 = v6_2;
    }

    public String getV6_3() {
        return V6_3;
    }

    public void setV6_3(String v6_3) {
        V6_3 = v6_3;
    }

    public String getV6_4() {
        return V6_4;
    }

    public void setV6_4(String v6_4) {
        V6_4 = v6_4;
    }

    public String getV6_5() {
        return V6_5;
    }

    public void setV6_5(String v6_5) {
        V6_5 = v6_5;
    }

    public String getV6_6() {
        return V6_6;
    }

    public void setV6_6(String v6_6) {
        V6_6 = v6_6;
    }

    public String getV6_7() {
        return V6_7;
    }

    public void setV6_7(String v6_7) {
        V6_7 = v6_7;
    }

    public String getV6_8() {
        return V6_8;
    }

    public void setV6_8(String v6_8) {
        V6_8 = v6_8;
    }

    public String getV6_9() {
        return V6_9;
    }

    public void setV6_9(String v6_9) {
        V6_9 = v6_9;
    }

    public String getV6_10() {
        return V6_10;
    }

    public void setV6_10(String v6_10) {
        V6_10 = v6_10;
    }

    public String getV7_1() {
        return V7_1;
    }

    public void setV7_1(String v7_1) {
        V7_1 = v7_1;
    }

    public String getV7_2() {
        return V7_2;
    }

    public void setV7_2(String v7_2) {
        V7_2 = v7_2;
    }

    public String getV7_3() {
        return V7_3;
    }

    public void setV7_3(String v7_3) {
        V7_3 = v7_3;
    }

    public String getV7_4() {
        return V7_4;
    }

    public void setV7_4(String v7_4) {
        V7_4 = v7_4;
    }

    public String getV7_5() {
        return V7_5;
    }

    public void setV7_5(String v7_5) {
        V7_5 = v7_5;
    }

    public String getV7_6() {
        return V7_6;
    }

    public void setV7_6(String v7_6) {
        V7_6 = v7_6;
    }

    public String getV7_7() {
        return V7_7;
    }

    public void setV7_7(String v7_7) {
        V7_7 = v7_7;
    }

    public String getV7_8() {
        return V7_8;
    }

    public void setV7_8(String v7_8) {
        V7_8 = v7_8;
    }

    public String getV7_9() {
        return V7_9;
    }

    public void setV7_9(String v7_9) {
        V7_9 = v7_9;
    }

    public String getV7_10() {
        return V7_10;
    }

    public void setV7_10(String v7_10) {
        V7_10 = v7_10;
    }

    public String getV8_1() {
        return V8_1;
    }

    public void setV8_1(String v8_1) {
        V8_1 = v8_1;
    }

    public String getV8_2() {
        return V8_2;
    }

    public void setV8_2(String v8_2) {
        V8_2 = v8_2;
    }

    public String getV8_3() {
        return V8_3;
    }

    public void setV8_3(String v8_3) {
        V8_3 = v8_3;
    }

    public String getV8_4() {
        return V8_4;
    }

    public void setV8_4(String v8_4) {
        V8_4 = v8_4;
    }

    public String getV8_5() {
        return V8_5;
    }

    public void setV8_5(String v8_5) {
        V8_5 = v8_5;
    }

    public String getV8_6() {
        return V8_6;
    }

    public void setV8_6(String v8_6) {
        V8_6 = v8_6;
    }

    public String getV8_7() {
        return V8_7;
    }

    public void setV8_7(String v8_7) {
        V8_7 = v8_7;
    }

    public String getV8_8() {
        return V8_8;
    }

    public void setV8_8(String v8_8) {
        V8_8 = v8_8;
    }

    public String getV8_9() {
        return V8_9;
    }

    public void setV8_9(String v8_9) {
        V8_9 = v8_9;
    }

    public String getV8_10() {
        return V8_10;
    }

    public void setV8_10(String v8_10) {
        V8_10 = v8_10;
    }

    public String getV9_1() {
        return V9_1;
    }

    public void setV9_1(String v9_1) {
        V9_1 = v9_1;
    }

    public String getV9_2() {
        return V9_2;
    }

    public void setV9_2(String v9_2) {
        V9_2 = v9_2;
    }

    public String getV9_3() {
        return V9_3;
    }

    public void setV9_3(String v9_3) {
        V9_3 = v9_3;
    }

    public String getV9_4() {
        return V9_4;
    }

    public void setV9_4(String v9_4) {
        V9_4 = v9_4;
    }

    public String getV9_5() {
        return V9_5;
    }

    public void setV9_5(String v9_5) {
        V9_5 = v9_5;
    }

    public String getV9_6() {
        return V9_6;
    }

    public void setV9_6(String v9_6) {
        V9_6 = v9_6;
    }

    public String getV9_7() {
        return V9_7;
    }

    public void setV9_7(String v9_7) {
        V9_7 = v9_7;
    }

    public String getV9_8() {
        return V9_8;
    }

    public void setV9_8(String v9_8) {
        V9_8 = v9_8;
    }

    public String getV9_9() {
        return V9_9;
    }

    public void setV9_9(String v9_9) {
        V9_9 = v9_9;
    }

    public String getV9_10() {
        return V9_10;
    }

    public void setV9_10(String v9_10) {
        V9_10 = v9_10;
    }

    public String getV10_1() {
        return V10_1;
    }

    public void setV10_1(String v10_1) {
        V10_1 = v10_1;
    }

    public String getV10_2() {
        return V10_2;
    }

    public void setV10_2(String v10_2) {
        V10_2 = v10_2;
    }

    public String getV10_3() {
        return V10_3;
    }

    public void setV10_3(String v10_3) {
        V10_3 = v10_3;
    }

    public String getV10_4() {
        return V10_4;
    }

    public void setV10_4(String v10_4) {
        V10_4 = v10_4;
    }

    public String getV10_5() {
        return V10_5;
    }

    public void setV10_5(String v10_5) {
        V10_5 = v10_5;
    }

    public String getV10_6() {
        return V10_6;
    }

    public void setV10_6(String v10_6) {
        V10_6 = v10_6;
    }

    public String getV10_7() {
        return V10_7;
    }

    public void setV10_7(String v10_7) {
        V10_7 = v10_7;
    }

    public String getV10_8() {
        return V10_8;
    }

    public void setV10_8(String v10_8) {
        V10_8 = v10_8;
    }

    public String getV10_9() {
        return V10_9;
    }

    public void setV10_9(String v10_9) {
        V10_9 = v10_9;
    }

    public String getV10_10() {
        return V10_10;
    }

    public void setV10_10(String v10_10) {
        V10_10 = v10_10;
    }

    public String getV11_1() {
        return V11_1;
    }

    public void setV11_1(String v11_1) {
        V11_1 = v11_1;
    }

    public String getV11_2() {
        return V11_2;
    }

    public void setV11_2(String v11_2) {
        V11_2 = v11_2;
    }

    public String getV11_3() {
        return V11_3;
    }

    public void setV11_3(String v11_3) {
        V11_3 = v11_3;
    }

    public String getV11_4() {
        return V11_4;
    }

    public void setV11_4(String v11_4) {
        V11_4 = v11_4;
    }

    public String getV11_5() {
        return V11_5;
    }

    public void setV11_5(String v11_5) {
        V11_5 = v11_5;
    }

    public String getV11_6() {
        return V11_6;
    }

    public void setV11_6(String v11_6) {
        V11_6 = v11_6;
    }

    public String getV11_7() {
        return V11_7;
    }

    public void setV11_7(String v11_7) {
        V11_7 = v11_7;
    }

    public String getV11_8() {
        return V11_8;
    }

    public void setV11_8(String v11_8) {
        V11_8 = v11_8;
    }

    public String getV11_9() {
        return V11_9;
    }

    public void setV11_9(String v11_9) {
        V11_9 = v11_9;
    }

    public String getV11_10() {
        return V11_10;
    }

    public void setV11_10(String v11_10) {
        V11_10 = v11_10;
    }

    public String getV12_1() {
        return V12_1;
    }

    public void setV12_1(String v12_1) {
        V12_1 = v12_1;
    }

    public String getV12_2() {
        return V12_2;
    }

    public void setV12_2(String v12_2) {
        V12_2 = v12_2;
    }

    public String getV12_3() {
        return V12_3;
    }

    public void setV12_3(String v12_3) {
        V12_3 = v12_3;
    }

    public String getV12_4() {
        return V12_4;
    }

    public void setV12_4(String v12_4) {
        V12_4 = v12_4;
    }

    public String getV12_5() {
        return V12_5;
    }

    public void setV12_5(String v12_5) {
        V12_5 = v12_5;
    }

    public String getV12_6() {
        return V12_6;
    }

    public void setV12_6(String v12_6) {
        V12_6 = v12_6;
    }

    public String getV12_7() {
        return V12_7;
    }

    public void setV12_7(String v12_7) {
        V12_7 = v12_7;
    }

    public String getV12_8() {
        return V12_8;
    }

    public void setV12_8(String v12_8) {
        V12_8 = v12_8;
    }

    public String getV12_9() {
        return V12_9;
    }

    public void setV12_9(String v12_9) {
        V12_9 = v12_9;
    }

    public String getV12_10() {
        return V12_10;
    }

    public void setV12_10(String v12_10) {
        V12_10 = v12_10;
    }

    public String getV13_1() {
        return V13_1;
    }

    public void setV13_1(String v13_1) {
        V13_1 = v13_1;
    }

    public String getV13_2() {
        return V13_2;
    }

    public void setV13_2(String v13_2) {
        V13_2 = v13_2;
    }

    public String getV13_3() {
        return V13_3;
    }

    public void setV13_3(String v13_3) {
        V13_3 = v13_3;
    }

    public String getV13_4() {
        return V13_4;
    }

    public void setV13_4(String v13_4) {
        V13_4 = v13_4;
    }

    public String getV13_5() {
        return V13_5;
    }

    public void setV13_5(String v13_5) {
        V13_5 = v13_5;
    }

    public String getV13_6() {
        return V13_6;
    }

    public void setV13_6(String v13_6) {
        V13_6 = v13_6;
    }

    public String getV13_7() {
        return V13_7;
    }

    public void setV13_7(String v13_7) {
        V13_7 = v13_7;
    }

    public String getV13_8() {
        return V13_8;
    }

    public void setV13_8(String v13_8) {
        V13_8 = v13_8;
    }

    public String getV13_9() {
        return V13_9;
    }

    public void setV13_9(String v13_9) {
        V13_9 = v13_9;
    }

    public String getV13_10() {
        return V13_10;
    }

    public void setV13_10(String v13_10) {
        V13_10 = v13_10;
    }

    public String getV14_1() {
        return V14_1;
    }

    public void setV14_1(String v14_1) {
        V14_1 = v14_1;
    }

    public String getV14_2() {
        return V14_2;
    }

    public void setV14_2(String v14_2) {
        V14_2 = v14_2;
    }

    public String getV14_3() {
        return V14_3;
    }

    public void setV14_3(String v14_3) {
        V14_3 = v14_3;
    }

    public String getV14_4() {
        return V14_4;
    }

    public void setV14_4(String v14_4) {
        V14_4 = v14_4;
    }

    public String getV14_5() {
        return V14_5;
    }

    public void setV14_5(String v14_5) {
        V14_5 = v14_5;
    }

    public String getV14_6() {
        return V14_6;
    }

    public void setV14_6(String v14_6) {
        V14_6 = v14_6;
    }

    public String getV14_7() {
        return V14_7;
    }

    public void setV14_7(String v14_7) {
        V14_7 = v14_7;
    }

    public String getV14_8() {
        return V14_8;
    }

    public void setV14_8(String v14_8) {
        V14_8 = v14_8;
    }

    public String getV14_9() {
        return V14_9;
    }

    public void setV14_9(String v14_9) {
        V14_9 = v14_9;
    }

    public String getV14_10() {
        return V14_10;
    }

    public void setV14_10(String v14_10) {
        V14_10 = v14_10;
    }

    public String getV15_1() {
        return V15_1;
    }

    public void setV15_1(String v15_1) {
        V15_1 = v15_1;
    }

    public String getV15_2() {
        return V15_2;
    }

    public void setV15_2(String v15_2) {
        V15_2 = v15_2;
    }

    public String getV15_3() {
        return V15_3;
    }

    public void setV15_3(String v15_3) {
        V15_3 = v15_3;
    }

    public String getV15_4() {
        return V15_4;
    }

    public void setV15_4(String v15_4) {
        V15_4 = v15_4;
    }

    public String getV15_5() {
        return V15_5;
    }

    public void setV15_5(String v15_5) {
        V15_5 = v15_5;
    }

    public String getV15_6() {
        return V15_6;
    }

    public void setV15_6(String v15_6) {
        V15_6 = v15_6;
    }

    public String getV15_7() {
        return V15_7;
    }

    public void setV15_7(String v15_7) {
        V15_7 = v15_7;
    }

    public String getV15_8() {
        return V15_8;
    }

    public void setV15_8(String v15_8) {
        V15_8 = v15_8;
    }

    public String getV15_9() {
        return V15_9;
    }

    public void setV15_9(String v15_9) {
        V15_9 = v15_9;
    }

    public String getV15_10() {
        return V15_10;
    }

    public void setV15_10(String v15_10) {
        V15_10 = v15_10;
    }

    public String getV16_1() {
        return V16_1;
    }

    public void setV16_1(String v16_1) {
        V16_1 = v16_1;
    }

    public String getV16_2() {
        return V16_2;
    }

    public void setV16_2(String v16_2) {
        V16_2 = v16_2;
    }

    public String getV16_3() {
        return V16_3;
    }

    public void setV16_3(String v16_3) {
        V16_3 = v16_3;
    }

    public String getV16_4() {
        return V16_4;
    }

    public void setV16_4(String v16_4) {
        V16_4 = v16_4;
    }

    public String getV16_5() {
        return V16_5;
    }

    public void setV16_5(String v16_5) {
        V16_5 = v16_5;
    }

    public String getV16_6() {
        return V16_6;
    }

    public void setV16_6(String v16_6) {
        V16_6 = v16_6;
    }

    public String getV16_7() {
        return V16_7;
    }

    public void setV16_7(String v16_7) {
        V16_7 = v16_7;
    }

    public String getV16_8() {
        return V16_8;
    }

    public void setV16_8(String v16_8) {
        V16_8 = v16_8;
    }

    public String getV16_9() {
        return V16_9;
    }

    public void setV16_9(String v16_9) {
        V16_9 = v16_9;
    }

    public String getV16_10() {
        return V16_10;
    }

    public void setV16_10(String v16_10) {
        V16_10 = v16_10;
    }

    public String getV17_1() {
        return V17_1;
    }

    public void setV17_1(String v17_1) {
        V17_1 = v17_1;
    }

    public String getV17_2() {
        return V17_2;
    }

    public void setV17_2(String v17_2) {
        V17_2 = v17_2;
    }

    public String getV17_3() {
        return V17_3;
    }

    public void setV17_3(String v17_3) {
        V17_3 = v17_3;
    }

    public String getV17_4() {
        return V17_4;
    }

    public void setV17_4(String v17_4) {
        V17_4 = v17_4;
    }

    public String getV17_5() {
        return V17_5;
    }

    public void setV17_5(String v17_5) {
        V17_5 = v17_5;
    }

    public String getV17_6() {
        return V17_6;
    }

    public void setV17_6(String v17_6) {
        V17_6 = v17_6;
    }

    public String getV17_7() {
        return V17_7;
    }

    public void setV17_7(String v17_7) {
        V17_7 = v17_7;
    }

    public String getV17_8() {
        return V17_8;
    }

    public void setV17_8(String v17_8) {
        V17_8 = v17_8;
    }

    public String getV17_9() {
        return V17_9;
    }

    public void setV17_9(String v17_9) {
        V17_9 = v17_9;
    }

    public String getV17_10() {
        return V17_10;
    }

    public void setV17_10(String v17_10) {
        V17_10 = v17_10;
    }

    public String getV18_1() {
        return V18_1;
    }

    public void setV18_1(String v18_1) {
        V18_1 = v18_1;
    }

    public String getV18_2() {
        return V18_2;
    }

    public void setV18_2(String v18_2) {
        V18_2 = v18_2;
    }

    public String getV18_3() {
        return V18_3;
    }

    public void setV18_3(String v18_3) {
        V18_3 = v18_3;
    }

    public String getV18_4() {
        return V18_4;
    }

    public void setV18_4(String v18_4) {
        V18_4 = v18_4;
    }

    public String getV18_5() {
        return V18_5;
    }

    public void setV18_5(String v18_5) {
        V18_5 = v18_5;
    }

    public String getV18_6() {
        return V18_6;
    }

    public void setV18_6(String v18_6) {
        V18_6 = v18_6;
    }

    public String getV18_7() {
        return V18_7;
    }

    public void setV18_7(String v18_7) {
        V18_7 = v18_7;
    }

    public String getV18_8() {
        return V18_8;
    }

    public void setV18_8(String v18_8) {
        V18_8 = v18_8;
    }

    public String getV18_9() {
        return V18_9;
    }

    public void setV18_9(String v18_9) {
        V18_9 = v18_9;
    }

    public String getV18_10() {
        return V18_10;
    }

    public void setV18_10(String v18_10) {
        V18_10 = v18_10;
    }

    public String getV19_1() {
        return V19_1;
    }

    public void setV19_1(String v19_1) {
        V19_1 = v19_1;
    }

    public String getV19_2() {
        return V19_2;
    }

    public void setV19_2(String v19_2) {
        V19_2 = v19_2;
    }

    public String getV19_3() {
        return V19_3;
    }

    public void setV19_3(String v19_3) {
        V19_3 = v19_3;
    }

    public String getV19_4() {
        return V19_4;
    }

    public void setV19_4(String v19_4) {
        V19_4 = v19_4;
    }

    public String getV19_5() {
        return V19_5;
    }

    public void setV19_5(String v19_5) {
        V19_5 = v19_5;
    }

    public String getV19_6() {
        return V19_6;
    }

    public void setV19_6(String v19_6) {
        V19_6 = v19_6;
    }

    public String getV19_7() {
        return V19_7;
    }

    public void setV19_7(String v19_7) {
        V19_7 = v19_7;
    }

    public String getV19_8() {
        return V19_8;
    }

    public void setV19_8(String v19_8) {
        V19_8 = v19_8;
    }

    public String getV19_9() {
        return V19_9;
    }

    public void setV19_9(String v19_9) {
        V19_9 = v19_9;
    }

    public String getV19_10() {
        return V19_10;
    }

    public void setV19_10(String v19_10) {
        V19_10 = v19_10;
    }

    public String getV20_1() {
        return V20_1;
    }

    public void setV20_1(String v20_1) {
        V20_1 = v20_1;
    }

    public String getV20_2() {
        return V20_2;
    }

    public void setV20_2(String v20_2) {
        V20_2 = v20_2;
    }

    public String getV20_3() {
        return V20_3;
    }

    public void setV20_3(String v20_3) {
        V20_3 = v20_3;
    }

    public String getV20_4() {
        return V20_4;
    }

    public void setV20_4(String v20_4) {
        V20_4 = v20_4;
    }

    public String getV20_5() {
        return V20_5;
    }

    public void setV20_5(String v20_5) {
        V20_5 = v20_5;
    }

    public String getV20_6() {
        return V20_6;
    }

    public void setV20_6(String v20_6) {
        V20_6 = v20_6;
    }

    public String getV20_7() {
        return V20_7;
    }

    public void setV20_7(String v20_7) {
        V20_7 = v20_7;
    }

    public String getV20_8() {
        return V20_8;
    }

    public void setV20_8(String v20_8) {
        V20_8 = v20_8;
    }

    public String getV20_9() {
        return V20_9;
    }

    public void setV20_9(String v20_9) {
        V20_9 = v20_9;
    }

    public String getV20_10() {
        return V20_10;
    }

    public void setV20_10(String v20_10) {
        V20_10 = v20_10;
    }

    public String getV21_1() {
        return V21_1;
    }

    public void setV21_1(String v21_1) {
        V21_1 = v21_1;
    }

    public String getV21_2() {
        return V21_2;
    }

    public void setV21_2(String v21_2) {
        V21_2 = v21_2;
    }

    public String getV21_3() {
        return V21_3;
    }

    public void setV21_3(String v21_3) {
        V21_3 = v21_3;
    }

    public String getV21_4() {
        return V21_4;
    }

    public void setV21_4(String v21_4) {
        V21_4 = v21_4;
    }

    public String getV21_5() {
        return V21_5;
    }

    public void setV21_5(String v21_5) {
        V21_5 = v21_5;
    }

    public String getV21_6() {
        return V21_6;
    }

    public void setV21_6(String v21_6) {
        V21_6 = v21_6;
    }

    public String getV21_7() {
        return V21_7;
    }

    public void setV21_7(String v21_7) {
        V21_7 = v21_7;
    }

    public String getV21_8() {
        return V21_8;
    }

    public void setV21_8(String v21_8) {
        V21_8 = v21_8;
    }

    public String getV21_9() {
        return V21_9;
    }

    public void setV21_9(String v21_9) {
        V21_9 = v21_9;
    }

    public String getV21_10() {
        return V21_10;
    }

    public void setV21_10(String v21_10) {
        V21_10 = v21_10;
    }

    public String getV22_1() {
        return V22_1;
    }

    public void setV22_1(String v22_1) {
        V22_1 = v22_1;
    }

    public String getV22_2() {
        return V22_2;
    }

    public void setV22_2(String v22_2) {
        V22_2 = v22_2;
    }

    public String getV22_3() {
        return V22_3;
    }

    public void setV22_3(String v22_3) {
        V22_3 = v22_3;
    }

    public String getV22_4() {
        return V22_4;
    }

    public void setV22_4(String v22_4) {
        V22_4 = v22_4;
    }

    public String getV22_5() {
        return V22_5;
    }

    public void setV22_5(String v22_5) {
        V22_5 = v22_5;
    }

    public String getV22_6() {
        return V22_6;
    }

    public void setV22_6(String v22_6) {
        V22_6 = v22_6;
    }

    public String getV22_7() {
        return V22_7;
    }

    public void setV22_7(String v22_7) {
        V22_7 = v22_7;
    }

    public String getV22_8() {
        return V22_8;
    }

    public void setV22_8(String v22_8) {
        V22_8 = v22_8;
    }

    public String getV22_9() {
        return V22_9;
    }

    public void setV22_9(String v22_9) {
        V22_9 = v22_9;
    }

    public String getV22_10() {
        return V22_10;
    }

    public void setV22_10(String v22_10) {
        V22_10 = v22_10;
    }

    public String getV23_1() {
        return V23_1;
    }

    public void setV23_1(String v23_1) {
        V23_1 = v23_1;
    }

    public String getV23_2() {
        return V23_2;
    }

    public void setV23_2(String v23_2) {
        V23_2 = v23_2;
    }

    public String getV23_3() {
        return V23_3;
    }

    public void setV23_3(String v23_3) {
        V23_3 = v23_3;
    }

    public String getV23_4() {
        return V23_4;
    }

    public void setV23_4(String v23_4) {
        V23_4 = v23_4;
    }

    public String getV23_5() {
        return V23_5;
    }

    public void setV23_5(String v23_5) {
        V23_5 = v23_5;
    }

    public String getV23_6() {
        return V23_6;
    }

    public void setV23_6(String v23_6) {
        V23_6 = v23_6;
    }

    public String getV23_7() {
        return V23_7;
    }

    public void setV23_7(String v23_7) {
        V23_7 = v23_7;
    }

    public String getV23_8() {
        return V23_8;
    }

    public void setV23_8(String v23_8) {
        V23_8 = v23_8;
    }

    public String getV23_9() {
        return V23_9;
    }

    public void setV23_9(String v23_9) {
        V23_9 = v23_9;
    }

    public String getV23_10() {
        return V23_10;
    }

    public void setV23_10(String v23_10) {
        V23_10 = v23_10;
    }

    public String getV24_1() {
        return V24_1;
    }

    public void setV24_1(String v24_1) {
        V24_1 = v24_1;
    }

    public String getV24_2() {
        return V24_2;
    }

    public void setV24_2(String v24_2) {
        V24_2 = v24_2;
    }

    public String getV24_3() {
        return V24_3;
    }

    public void setV24_3(String v24_3) {
        V24_3 = v24_3;
    }

    public String getV24_4() {
        return V24_4;
    }

    public void setV24_4(String v24_4) {
        V24_4 = v24_4;
    }

    public String getV24_5() {
        return V24_5;
    }

    public void setV24_5(String v24_5) {
        V24_5 = v24_5;
    }

    public String getV24_6() {
        return V24_6;
    }

    public void setV24_6(String v24_6) {
        V24_6 = v24_6;
    }

    public String getV24_7() {
        return V24_7;
    }

    public void setV24_7(String v24_7) {
        V24_7 = v24_7;
    }

    public String getV24_8() {
        return V24_8;
    }

    public void setV24_8(String v24_8) {
        V24_8 = v24_8;
    }

    public String getV24_9() {
        return V24_9;
    }

    public void setV24_9(String v24_9) {
        V24_9 = v24_9;
    }

    public String getV24_10() {
        return V24_10;
    }

    public void setV24_10(String v24_10) {
        V24_10 = v24_10;
    }

    public String getV25_1() {
        return V25_1;
    }

    public void setV25_1(String v25_1) {
        V25_1 = v25_1;
    }

    public String getV25_2() {
        return V25_2;
    }

    public void setV25_2(String v25_2) {
        V25_2 = v25_2;
    }

    public String getV25_3() {
        return V25_3;
    }

    public void setV25_3(String v25_3) {
        V25_3 = v25_3;
    }

    public String getV25_4() {
        return V25_4;
    }

    public void setV25_4(String v25_4) {
        V25_4 = v25_4;
    }

    public String getV25_5() {
        return V25_5;
    }

    public void setV25_5(String v25_5) {
        V25_5 = v25_5;
    }

    public String getV25_6() {
        return V25_6;
    }

    public void setV25_6(String v25_6) {
        V25_6 = v25_6;
    }

    public String getV25_7() {
        return V25_7;
    }

    public void setV25_7(String v25_7) {
        V25_7 = v25_7;
    }

    public String getV25_8() {
        return V25_8;
    }

    public void setV25_8(String v25_8) {
        V25_8 = v25_8;
    }

    public String getV25_9() {
        return V25_9;
    }

    public void setV25_9(String v25_9) {
        V25_9 = v25_9;
    }

    public String getV25_10() {
        return V25_10;
    }

    public void setV25_10(String v25_10) {
        V25_10 = v25_10;
    }

    public String getV26_1() {
        return V26_1;
    }

    public void setV26_1(String v26_1) {
        V26_1 = v26_1;
    }

    public String getV26_2() {
        return V26_2;
    }

    public void setV26_2(String v26_2) {
        V26_2 = v26_2;
    }

    public String getV26_3() {
        return V26_3;
    }

    public void setV26_3(String v26_3) {
        V26_3 = v26_3;
    }

    public String getV26_4() {
        return V26_4;
    }

    public void setV26_4(String v26_4) {
        V26_4 = v26_4;
    }

    public String getV26_5() {
        return V26_5;
    }

    public void setV26_5(String v26_5) {
        V26_5 = v26_5;
    }

    public String getV26_6() {
        return V26_6;
    }

    public void setV26_6(String v26_6) {
        V26_6 = v26_6;
    }

    public String getV26_7() {
        return V26_7;
    }

    public void setV26_7(String v26_7) {
        V26_7 = v26_7;
    }

    public String getV26_8() {
        return V26_8;
    }

    public void setV26_8(String v26_8) {
        V26_8 = v26_8;
    }

    public String getV26_9() {
        return V26_9;
    }

    public void setV26_9(String v26_9) {
        V26_9 = v26_9;
    }

    public String getV26_10() {
        return V26_10;
    }

    public void setV26_10(String v26_10) {
        V26_10 = v26_10;
    }

    public String getV27_1() {
        return V27_1;
    }

    public void setV27_1(String v27_1) {
        V27_1 = v27_1;
    }

    public String getV27_2() {
        return V27_2;
    }

    public void setV27_2(String v27_2) {
        V27_2 = v27_2;
    }

    public String getV27_3() {
        return V27_3;
    }

    public void setV27_3(String v27_3) {
        V27_3 = v27_3;
    }

    public String getV27_4() {
        return V27_4;
    }

    public void setV27_4(String v27_4) {
        V27_4 = v27_4;
    }

    public String getV27_5() {
        return V27_5;
    }

    public void setV27_5(String v27_5) {
        V27_5 = v27_5;
    }

    public String getV27_6() {
        return V27_6;
    }

    public void setV27_6(String v27_6) {
        V27_6 = v27_6;
    }

    public String getV27_7() {
        return V27_7;
    }

    public void setV27_7(String v27_7) {
        V27_7 = v27_7;
    }

    public String getV27_8() {
        return V27_8;
    }

    public void setV27_8(String v27_8) {
        V27_8 = v27_8;
    }

    public String getV27_9() {
        return V27_9;
    }

    public void setV27_9(String v27_9) {
        V27_9 = v27_9;
    }

    public String getV27_10() {
        return V27_10;
    }

    public void setV27_10(String v27_10) {
        V27_10 = v27_10;
    }

    public String getV28_1() {
        return V28_1;
    }

    public void setV28_1(String v28_1) {
        V28_1 = v28_1;
    }

    public String getV28_2() {
        return V28_2;
    }

    public void setV28_2(String v28_2) {
        V28_2 = v28_2;
    }

    public String getV28_3() {
        return V28_3;
    }

    public void setV28_3(String v28_3) {
        V28_3 = v28_3;
    }

    public String getV28_4() {
        return V28_4;
    }

    public void setV28_4(String v28_4) {
        V28_4 = v28_4;
    }

    public String getV28_5() {
        return V28_5;
    }

    public void setV28_5(String v28_5) {
        V28_5 = v28_5;
    }

    public String getV28_6() {
        return V28_6;
    }

    public void setV28_6(String v28_6) {
        V28_6 = v28_6;
    }

    public String getV28_7() {
        return V28_7;
    }

    public void setV28_7(String v28_7) {
        V28_7 = v28_7;
    }

    public String getV28_8() {
        return V28_8;
    }

    public void setV28_8(String v28_8) {
        V28_8 = v28_8;
    }

    public String getV28_9() {
        return V28_9;
    }

    public void setV28_9(String v28_9) {
        V28_9 = v28_9;
    }

    public String getV28_10() {
        return V28_10;
    }

    public void setV28_10(String v28_10) {
        V28_10 = v28_10;
    }

    public String getV29_1() {
        return V29_1;
    }

    public void setV29_1(String v29_1) {
        V29_1 = v29_1;
    }

    public String getV29_2() {
        return V29_2;
    }

    public void setV29_2(String v29_2) {
        V29_2 = v29_2;
    }

    public String getV29_3() {
        return V29_3;
    }

    public void setV29_3(String v29_3) {
        V29_3 = v29_3;
    }

    public String getV29_4() {
        return V29_4;
    }

    public void setV29_4(String v29_4) {
        V29_4 = v29_4;
    }

    public String getV29_5() {
        return V29_5;
    }

    public void setV29_5(String v29_5) {
        V29_5 = v29_5;
    }

    public String getV29_6() {
        return V29_6;
    }

    public void setV29_6(String v29_6) {
        V29_6 = v29_6;
    }

    public String getV29_7() {
        return V29_7;
    }

    public void setV29_7(String v29_7) {
        V29_7 = v29_7;
    }

    public String getV29_8() {
        return V29_8;
    }

    public void setV29_8(String v29_8) {
        V29_8 = v29_8;
    }

    public String getV29_9() {
        return V29_9;
    }

    public void setV29_9(String v29_9) {
        V29_9 = v29_9;
    }

    public String getV29_10() {
        return V29_10;
    }

    public void setV29_10(String v29_10) {
        V29_10 = v29_10;
    }

    public String getV30_1() {
        return V30_1;
    }

    public void setV30_1(String v30_1) {
        V30_1 = v30_1;
    }

    public String getV30_2() {
        return V30_2;
    }

    public void setV30_2(String v30_2) {
        V30_2 = v30_2;
    }

    public String getV30_3() {
        return V30_3;
    }

    public void setV30_3(String v30_3) {
        V30_3 = v30_3;
    }

    public String getV30_4() {
        return V30_4;
    }

    public void setV30_4(String v30_4) {
        V30_4 = v30_4;
    }

    public String getV30_5() {
        return V30_5;
    }

    public void setV30_5(String v30_5) {
        V30_5 = v30_5;
    }

    public String getV30_6() {
        return V30_6;
    }

    public void setV30_6(String v30_6) {
        V30_6 = v30_6;
    }

    public String getV30_7() {
        return V30_7;
    }

    public void setV30_7(String v30_7) {
        V30_7 = v30_7;
    }

    public String getV30_8() {
        return V30_8;
    }

    public void setV30_8(String v30_8) {
        V30_8 = v30_8;
    }

    public String getV30_9() {
        return V30_9;
    }

    public void setV30_9(String v30_9) {
        V30_9 = v30_9;
    }

    public String getV30_10() {
        return V30_10;
    }

    public void setV30_10(String v30_10) {
        V30_10 = v30_10;
    }

    public String getV31_1() {
        return V31_1;
    }

    public void setV31_1(String v31_1) {
        V31_1 = v31_1;
    }

    public String getV31_2() {
        return V31_2;
    }

    public void setV31_2(String v31_2) {
        V31_2 = v31_2;
    }

    public String getV31_3() {
        return V31_3;
    }

    public void setV31_3(String v31_3) {
        V31_3 = v31_3;
    }

    public String getV31_4() {
        return V31_4;
    }

    public void setV31_4(String v31_4) {
        V31_4 = v31_4;
    }

    public String getV31_5() {
        return V31_5;
    }

    public void setV31_5(String v31_5) {
        V31_5 = v31_5;
    }

    public String getV31_6() {
        return V31_6;
    }

    public void setV31_6(String v31_6) {
        V31_6 = v31_6;
    }

    public String getV31_7() {
        return V31_7;
    }

    public void setV31_7(String v31_7) {
        V31_7 = v31_7;
    }

    public String getV31_8() {
        return V31_8;
    }

    public void setV31_8(String v31_8) {
        V31_8 = v31_8;
    }

    public String getV31_9() {
        return V31_9;
    }

    public void setV31_9(String v31_9) {
        V31_9 = v31_9;
    }

    public String getV31_10() {
        return V31_10;
    }

    public void setV31_10(String v31_10) {
        V31_10 = v31_10;
    }

    public String getV32_1() {
        return V32_1;
    }

    public void setV32_1(String v32_1) {
        V32_1 = v32_1;
    }

    public String getV32_2() {
        return V32_2;
    }

    public void setV32_2(String v32_2) {
        V32_2 = v32_2;
    }

    public String getV32_3() {
        return V32_3;
    }

    public void setV32_3(String v32_3) {
        V32_3 = v32_3;
    }

    public String getV32_4() {
        return V32_4;
    }

    public void setV32_4(String v32_4) {
        V32_4 = v32_4;
    }

    public String getV32_5() {
        return V32_5;
    }

    public void setV32_5(String v32_5) {
        V32_5 = v32_5;
    }

    public String getV32_6() {
        return V32_6;
    }

    public void setV32_6(String v32_6) {
        V32_6 = v32_6;
    }

    public String getV32_7() {
        return V32_7;
    }

    public void setV32_7(String v32_7) {
        V32_7 = v32_7;
    }

    public String getV32_8() {
        return V32_8;
    }

    public void setV32_8(String v32_8) {
        V32_8 = v32_8;
    }

    public String getV32_9() {
        return V32_9;
    }

    public void setV32_9(String v32_9) {
        V32_9 = v32_9;
    }

    public String getV32_10() {
        return V32_10;
    }

    public void setV32_10(String v32_10) {
        V32_10 = v32_10;
    }

    public String getV33_1() {
        return V33_1;
    }

    public void setV33_1(String v33_1) {
        V33_1 = v33_1;
    }

    public String getV33_2() {
        return V33_2;
    }

    public void setV33_2(String v33_2) {
        V33_2 = v33_2;
    }

    public String getV33_3() {
        return V33_3;
    }

    public void setV33_3(String v33_3) {
        V33_3 = v33_3;
    }

    public String getV33_4() {
        return V33_4;
    }

    public void setV33_4(String v33_4) {
        V33_4 = v33_4;
    }

    public String getV33_5() {
        return V33_5;
    }

    public void setV33_5(String v33_5) {
        V33_5 = v33_5;
    }

    public String getV33_6() {
        return V33_6;
    }

    public void setV33_6(String v33_6) {
        V33_6 = v33_6;
    }

    public String getV33_7() {
        return V33_7;
    }

    public void setV33_7(String v33_7) {
        V33_7 = v33_7;
    }

    public String getV33_8() {
        return V33_8;
    }

    public void setV33_8(String v33_8) {
        V33_8 = v33_8;
    }

    public String getV33_9() {
        return V33_9;
    }

    public void setV33_9(String v33_9) {
        V33_9 = v33_9;
    }

    public String getV33_10() {
        return V33_10;
    }

    public void setV33_10(String v33_10) {
        V33_10 = v33_10;
    }

    public String getV34_1() {
        return V34_1;
    }

    public void setV34_1(String v34_1) {
        V34_1 = v34_1;
    }

    public String getV34_2() {
        return V34_2;
    }

    public void setV34_2(String v34_2) {
        V34_2 = v34_2;
    }

    public String getV34_3() {
        return V34_3;
    }

    public void setV34_3(String v34_3) {
        V34_3 = v34_3;
    }

    public String getV34_4() {
        return V34_4;
    }

    public void setV34_4(String v34_4) {
        V34_4 = v34_4;
    }

    public String getV34_5() {
        return V34_5;
    }

    public void setV34_5(String v34_5) {
        V34_5 = v34_5;
    }

    public String getV34_6() {
        return V34_6;
    }

    public void setV34_6(String v34_6) {
        V34_6 = v34_6;
    }

    public String getV34_7() {
        return V34_7;
    }

    public void setV34_7(String v34_7) {
        V34_7 = v34_7;
    }

    public String getV34_8() {
        return V34_8;
    }

    public void setV34_8(String v34_8) {
        V34_8 = v34_8;
    }

    public String getV34_9() {
        return V34_9;
    }

    public void setV34_9(String v34_9) {
        V34_9 = v34_9;
    }

    public String getV34_10() {
        return V34_10;
    }

    public void setV34_10(String v34_10) {
        V34_10 = v34_10;
    }

    public String getV35_1() {
        return V35_1;
    }

    public void setV35_1(String v35_1) {
        V35_1 = v35_1;
    }

    public String getV35_2() {
        return V35_2;
    }

    public void setV35_2(String v35_2) {
        V35_2 = v35_2;
    }

    public String getV35_3() {
        return V35_3;
    }

    public void setV35_3(String v35_3) {
        V35_3 = v35_3;
    }

    public String getV35_4() {
        return V35_4;
    }

    public void setV35_4(String v35_4) {
        V35_4 = v35_4;
    }

    public String getV35_5() {
        return V35_5;
    }

    public void setV35_5(String v35_5) {
        V35_5 = v35_5;
    }

    public String getV35_6() {
        return V35_6;
    }

    public void setV35_6(String v35_6) {
        V35_6 = v35_6;
    }

    public String getV35_7() {
        return V35_7;
    }

    public void setV35_7(String v35_7) {
        V35_7 = v35_7;
    }

    public String getV35_8() {
        return V35_8;
    }

    public void setV35_8(String v35_8) {
        V35_8 = v35_8;
    }

    public String getV35_9() {
        return V35_9;
    }

    public void setV35_9(String v35_9) {
        V35_9 = v35_9;
    }

    public String getV35_10() {
        return V35_10;
    }

    public void setV35_10(String v35_10) {
        V35_10 = v35_10;
    }

    public String getV36_1() {
        return V36_1;
    }

    public void setV36_1(String v36_1) {
        V36_1 = v36_1;
    }

    public String getV36_2() {
        return V36_2;
    }

    public void setV36_2(String v36_2) {
        V36_2 = v36_2;
    }

    public String getV36_3() {
        return V36_3;
    }

    public void setV36_3(String v36_3) {
        V36_3 = v36_3;
    }

    public String getV36_4() {
        return V36_4;
    }

    public void setV36_4(String v36_4) {
        V36_4 = v36_4;
    }

    public String getV36_5() {
        return V36_5;
    }

    public void setV36_5(String v36_5) {
        V36_5 = v36_5;
    }

    public String getV36_6() {
        return V36_6;
    }

    public void setV36_6(String v36_6) {
        V36_6 = v36_6;
    }

    public String getV36_7() {
        return V36_7;
    }

    public void setV36_7(String v36_7) {
        V36_7 = v36_7;
    }

    public String getV36_8() {
        return V36_8;
    }

    public void setV36_8(String v36_8) {
        V36_8 = v36_8;
    }

    public String getV36_9() {
        return V36_9;
    }

    public void setV36_9(String v36_9) {
        V36_9 = v36_9;
    }

    public String getV36_10() {
        return V36_10;
    }

    public void setV36_10(String v36_10) {
        V36_10 = v36_10;
    }

    public String getV37_1() {
        return V37_1;
    }

    public void setV37_1(String v37_1) {
        V37_1 = v37_1;
    }

    public String getV37_2() {
        return V37_2;
    }

    public void setV37_2(String v37_2) {
        V37_2 = v37_2;
    }

    public String getV37_3() {
        return V37_3;
    }

    public void setV37_3(String v37_3) {
        V37_3 = v37_3;
    }

    public String getV37_4() {
        return V37_4;
    }

    public void setV37_4(String v37_4) {
        V37_4 = v37_4;
    }

    public String getV37_5() {
        return V37_5;
    }

    public void setV37_5(String v37_5) {
        V37_5 = v37_5;
    }

    public String getV37_6() {
        return V37_6;
    }

    public void setV37_6(String v37_6) {
        V37_6 = v37_6;
    }

    public String getV37_7() {
        return V37_7;
    }

    public void setV37_7(String v37_7) {
        V37_7 = v37_7;
    }

    public String getV37_8() {
        return V37_8;
    }

    public void setV37_8(String v37_8) {
        V37_8 = v37_8;
    }

    public String getV37_9() {
        return V37_9;
    }

    public void setV37_9(String v37_9) {
        V37_9 = v37_9;
    }

    public String getV37_10() {
        return V37_10;
    }

    public void setV37_10(String v37_10) {
        V37_10 = v37_10;
    }

    public String getV38_1() {
        return V38_1;
    }

    public void setV38_1(String v38_1) {
        V38_1 = v38_1;
    }

    public String getV38_2() {
        return V38_2;
    }

    public void setV38_2(String v38_2) {
        V38_2 = v38_2;
    }

    public String getV38_3() {
        return V38_3;
    }

    public void setV38_3(String v38_3) {
        V38_3 = v38_3;
    }

    public String getV38_4() {
        return V38_4;
    }

    public void setV38_4(String v38_4) {
        V38_4 = v38_4;
    }

    public String getV38_5() {
        return V38_5;
    }

    public void setV38_5(String v38_5) {
        V38_5 = v38_5;
    }

    public String getV38_6() {
        return V38_6;
    }

    public void setV38_6(String v38_6) {
        V38_6 = v38_6;
    }

    public String getV38_7() {
        return V38_7;
    }

    public void setV38_7(String v38_7) {
        V38_7 = v38_7;
    }

    public String getV38_8() {
        return V38_8;
    }

    public void setV38_8(String v38_8) {
        V38_8 = v38_8;
    }

    public String getV38_9() {
        return V38_9;
    }

    public void setV38_9(String v38_9) {
        V38_9 = v38_9;
    }

    public String getV38_10() {
        return V38_10;
    }

    public void setV38_10(String v38_10) {
        V38_10 = v38_10;
    }

    public String getV39_1() {
        return V39_1;
    }

    public void setV39_1(String v39_1) {
        V39_1 = v39_1;
    }

    public String getV39_2() {
        return V39_2;
    }

    public void setV39_2(String v39_2) {
        V39_2 = v39_2;
    }

    public String getV39_3() {
        return V39_3;
    }

    public void setV39_3(String v39_3) {
        V39_3 = v39_3;
    }

    public String getV39_4() {
        return V39_4;
    }

    public void setV39_4(String v39_4) {
        V39_4 = v39_4;
    }

    public String getV39_5() {
        return V39_5;
    }

    public void setV39_5(String v39_5) {
        V39_5 = v39_5;
    }

    public String getV39_6() {
        return V39_6;
    }

    public void setV39_6(String v39_6) {
        V39_6 = v39_6;
    }

    public String getV39_7() {
        return V39_7;
    }

    public void setV39_7(String v39_7) {
        V39_7 = v39_7;
    }

    public String getV39_8() {
        return V39_8;
    }

    public void setV39_8(String v39_8) {
        V39_8 = v39_8;
    }

    public String getV39_9() {
        return V39_9;
    }

    public void setV39_9(String v39_9) {
        V39_9 = v39_9;
    }

    public String getV39_10() {
        return V39_10;
    }

    public void setV39_10(String v39_10) {
        V39_10 = v39_10;
    }

    public String getV40_1() {
        return V40_1;
    }

    public void setV40_1(String v40_1) {
        V40_1 = v40_1;
    }

    public String getV40_2() {
        return V40_2;
    }

    public void setV40_2(String v40_2) {
        V40_2 = v40_2;
    }

    public String getV40_3() {
        return V40_3;
    }

    public void setV40_3(String v40_3) {
        V40_3 = v40_3;
    }

    public String getV40_4() {
        return V40_4;
    }

    public void setV40_4(String v40_4) {
        V40_4 = v40_4;
    }

    public String getV40_5() {
        return V40_5;
    }

    public void setV40_5(String v40_5) {
        V40_5 = v40_5;
    }

    public String getV40_6() {
        return V40_6;
    }

    public void setV40_6(String v40_6) {
        V40_6 = v40_6;
    }

    public String getV40_7() {
        return V40_7;
    }

    public void setV40_7(String v40_7) {
        V40_7 = v40_7;
    }

    public String getV40_8() {
        return V40_8;
    }

    public void setV40_8(String v40_8) {
        V40_8 = v40_8;
    }

    public String getV40_9() {
        return V40_9;
    }

    public void setV40_9(String v40_9) {
        V40_9 = v40_9;
    }

    public String getV40_10() {
        return V40_10;
    }

    public void setV40_10(String v40_10) {
        V40_10 = v40_10;
    }

    public String getV41_1() {
        return V41_1;
    }

    public void setV41_1(String v41_1) {
        V41_1 = v41_1;
    }

    public String getV41_2() {
        return V41_2;
    }

    public void setV41_2(String v41_2) {
        V41_2 = v41_2;
    }

    public String getV41_3() {
        return V41_3;
    }

    public void setV41_3(String v41_3) {
        V41_3 = v41_3;
    }

    public String getV41_4() {
        return V41_4;
    }

    public void setV41_4(String v41_4) {
        V41_4 = v41_4;
    }

    public String getV41_5() {
        return V41_5;
    }

    public void setV41_5(String v41_5) {
        V41_5 = v41_5;
    }

    public String getV41_6() {
        return V41_6;
    }

    public void setV41_6(String v41_6) {
        V41_6 = v41_6;
    }

    public String getV41_7() {
        return V41_7;
    }

    public void setV41_7(String v41_7) {
        V41_7 = v41_7;
    }

    public String getV41_8() {
        return V41_8;
    }

    public void setV41_8(String v41_8) {
        V41_8 = v41_8;
    }

    public String getV41_9() {
        return V41_9;
    }

    public void setV41_9(String v41_9) {
        V41_9 = v41_9;
    }

    public String getV41_10() {
        return V41_10;
    }

    public void setV41_10(String v41_10) {
        V41_10 = v41_10;
    }

    public String getV42_1() {
        return V42_1;
    }

    public void setV42_1(String v42_1) {
        V42_1 = v42_1;
    }

    public String getV42_2() {
        return V42_2;
    }

    public void setV42_2(String v42_2) {
        V42_2 = v42_2;
    }

    public String getV42_3() {
        return V42_3;
    }

    public void setV42_3(String v42_3) {
        V42_3 = v42_3;
    }

    public String getV42_4() {
        return V42_4;
    }

    public void setV42_4(String v42_4) {
        V42_4 = v42_4;
    }

    public String getV42_5() {
        return V42_5;
    }

    public void setV42_5(String v42_5) {
        V42_5 = v42_5;
    }

    public String getV42_6() {
        return V42_6;
    }

    public void setV42_6(String v42_6) {
        V42_6 = v42_6;
    }

    public String getV42_7() {
        return V42_7;
    }

    public void setV42_7(String v42_7) {
        V42_7 = v42_7;
    }

    public String getV42_8() {
        return V42_8;
    }

    public void setV42_8(String v42_8) {
        V42_8 = v42_8;
    }

    public String getV42_9() {
        return V42_9;
    }

    public void setV42_9(String v42_9) {
        V42_9 = v42_9;
    }

    public String getV42_10() {
        return V42_10;
    }

    public void setV42_10(String v42_10) {
        V42_10 = v42_10;
    }

    public String getV43_1() {
        return V43_1;
    }

    public void setV43_1(String v43_1) {
        V43_1 = v43_1;
    }

    public String getV43_2() {
        return V43_2;
    }

    public void setV43_2(String v43_2) {
        V43_2 = v43_2;
    }

    public String getV43_3() {
        return V43_3;
    }

    public void setV43_3(String v43_3) {
        V43_3 = v43_3;
    }

    public String getV43_4() {
        return V43_4;
    }

    public void setV43_4(String v43_4) {
        V43_4 = v43_4;
    }

    public String getV43_5() {
        return V43_5;
    }

    public void setV43_5(String v43_5) {
        V43_5 = v43_5;
    }

    public String getV43_6() {
        return V43_6;
    }

    public void setV43_6(String v43_6) {
        V43_6 = v43_6;
    }

    public String getV43_7() {
        return V43_7;
    }

    public void setV43_7(String v43_7) {
        V43_7 = v43_7;
    }

    public String getV43_8() {
        return V43_8;
    }

    public void setV43_8(String v43_8) {
        V43_8 = v43_8;
    }

    public String getV43_9() {
        return V43_9;
    }

    public void setV43_9(String v43_9) {
        V43_9 = v43_9;
    }

    public String getV43_10() {
        return V43_10;
    }

    public void setV43_10(String v43_10) {
        V43_10 = v43_10;
    }

    public String getV44_1() {
        return V44_1;
    }

    public void setV44_1(String v44_1) {
        V44_1 = v44_1;
    }

    public String getV44_2() {
        return V44_2;
    }

    public void setV44_2(String v44_2) {
        V44_2 = v44_2;
    }

    public String getV44_3() {
        return V44_3;
    }

    public void setV44_3(String v44_3) {
        V44_3 = v44_3;
    }

    public String getV44_4() {
        return V44_4;
    }

    public void setV44_4(String v44_4) {
        V44_4 = v44_4;
    }

    public String getV44_5() {
        return V44_5;
    }

    public void setV44_5(String v44_5) {
        V44_5 = v44_5;
    }

    public String getV44_6() {
        return V44_6;
    }

    public void setV44_6(String v44_6) {
        V44_6 = v44_6;
    }

    public String getV44_7() {
        return V44_7;
    }

    public void setV44_7(String v44_7) {
        V44_7 = v44_7;
    }

    public String getV44_8() {
        return V44_8;
    }

    public void setV44_8(String v44_8) {
        V44_8 = v44_8;
    }

    public String getV44_9() {
        return V44_9;
    }

    public void setV44_9(String v44_9) {
        V44_9 = v44_9;
    }

    public String getV44_10() {
        return V44_10;
    }

    public void setV44_10(String v44_10) {
        V44_10 = v44_10;
    }

    public String getV45_1() {
        return V45_1;
    }

    public void setV45_1(String v45_1) {
        V45_1 = v45_1;
    }

    public String getV45_2() {
        return V45_2;
    }

    public void setV45_2(String v45_2) {
        V45_2 = v45_2;
    }

    public String getV45_3() {
        return V45_3;
    }

    public void setV45_3(String v45_3) {
        V45_3 = v45_3;
    }

    public String getV45_4() {
        return V45_4;
    }

    public void setV45_4(String v45_4) {
        V45_4 = v45_4;
    }

    public String getV45_5() {
        return V45_5;
    }

    public void setV45_5(String v45_5) {
        V45_5 = v45_5;
    }

    public String getV45_6() {
        return V45_6;
    }

    public void setV45_6(String v45_6) {
        V45_6 = v45_6;
    }

    public String getV45_7() {
        return V45_7;
    }

    public void setV45_7(String v45_7) {
        V45_7 = v45_7;
    }

    public String getV45_8() {
        return V45_8;
    }

    public void setV45_8(String v45_8) {
        V45_8 = v45_8;
    }

    public String getV45_9() {
        return V45_9;
    }

    public void setV45_9(String v45_9) {
        V45_9 = v45_9;
    }

    public String getV45_10() {
        return V45_10;
    }

    public void setV45_10(String v45_10) {
        V45_10 = v45_10;
    }

    public String getV46_1() {
        return V46_1;
    }

    public void setV46_1(String v46_1) {
        V46_1 = v46_1;
    }

    public String getV46_2() {
        return V46_2;
    }

    public void setV46_2(String v46_2) {
        V46_2 = v46_2;
    }

    public String getV46_3() {
        return V46_3;
    }

    public void setV46_3(String v46_3) {
        V46_3 = v46_3;
    }

    public String getV46_4() {
        return V46_4;
    }

    public void setV46_4(String v46_4) {
        V46_4 = v46_4;
    }

    public String getV46_5() {
        return V46_5;
    }

    public void setV46_5(String v46_5) {
        V46_5 = v46_5;
    }

    public String getV46_6() {
        return V46_6;
    }

    public void setV46_6(String v46_6) {
        V46_6 = v46_6;
    }

    public String getV46_7() {
        return V46_7;
    }

    public void setV46_7(String v46_7) {
        V46_7 = v46_7;
    }

    public String getV46_8() {
        return V46_8;
    }

    public void setV46_8(String v46_8) {
        V46_8 = v46_8;
    }

    public String getV46_9() {
        return V46_9;
    }

    public void setV46_9(String v46_9) {
        V46_9 = v46_9;
    }

    public String getV46_10() {
        return V46_10;
    }

    public void setV46_10(String v46_10) {
        V46_10 = v46_10;
    }

    public String getV47_1() {
        return V47_1;
    }

    public void setV47_1(String v47_1) {
        V47_1 = v47_1;
    }

    public String getV47_2() {
        return V47_2;
    }

    public void setV47_2(String v47_2) {
        V47_2 = v47_2;
    }

    public String getV47_3() {
        return V47_3;
    }

    public void setV47_3(String v47_3) {
        V47_3 = v47_3;
    }

    public String getV47_4() {
        return V47_4;
    }

    public void setV47_4(String v47_4) {
        V47_4 = v47_4;
    }

    public String getV47_5() {
        return V47_5;
    }

    public void setV47_5(String v47_5) {
        V47_5 = v47_5;
    }

    public String getV47_6() {
        return V47_6;
    }

    public void setV47_6(String v47_6) {
        V47_6 = v47_6;
    }

    public String getV47_7() {
        return V47_7;
    }

    public void setV47_7(String v47_7) {
        V47_7 = v47_7;
    }

    public String getV47_8() {
        return V47_8;
    }

    public void setV47_8(String v47_8) {
        V47_8 = v47_8;
    }

    public String getV47_9() {
        return V47_9;
    }

    public void setV47_9(String v47_9) {
        V47_9 = v47_9;
    }

    public String getV47_10() {
        return V47_10;
    }

    public void setV47_10(String v47_10) {
        V47_10 = v47_10;
    }

    public String getV48_1() {
        return V48_1;
    }

    public void setV48_1(String v48_1) {
        V48_1 = v48_1;
    }

    public String getV48_2() {
        return V48_2;
    }

    public void setV48_2(String v48_2) {
        V48_2 = v48_2;
    }

    public String getV48_3() {
        return V48_3;
    }

    public void setV48_3(String v48_3) {
        V48_3 = v48_3;
    }

    public String getV48_4() {
        return V48_4;
    }

    public void setV48_4(String v48_4) {
        V48_4 = v48_4;
    }

    public String getV48_5() {
        return V48_5;
    }

    public void setV48_5(String v48_5) {
        V48_5 = v48_5;
    }

    public String getV48_6() {
        return V48_6;
    }

    public void setV48_6(String v48_6) {
        V48_6 = v48_6;
    }

    public String getV48_7() {
        return V48_7;
    }

    public void setV48_7(String v48_7) {
        V48_7 = v48_7;
    }

    public String getV48_8() {
        return V48_8;
    }

    public void setV48_8(String v48_8) {
        V48_8 = v48_8;
    }

    public String getV48_9() {
        return V48_9;
    }

    public void setV48_9(String v48_9) {
        V48_9 = v48_9;
    }

    public String getV48_10() {
        return V48_10;
    }

    public void setV48_10(String v48_10) {
        V48_10 = v48_10;
    }

    public String getV49_1() {
        return V49_1;
    }

    public void setV49_1(String v49_1) {
        V49_1 = v49_1;
    }

    public String getV49_2() {
        return V49_2;
    }

    public void setV49_2(String v49_2) {
        V49_2 = v49_2;
    }

    public String getV49_3() {
        return V49_3;
    }

    public void setV49_3(String v49_3) {
        V49_3 = v49_3;
    }

    public String getV49_4() {
        return V49_4;
    }

    public void setV49_4(String v49_4) {
        V49_4 = v49_4;
    }

    public String getV49_5() {
        return V49_5;
    }

    public void setV49_5(String v49_5) {
        V49_5 = v49_5;
    }

    public String getV49_6() {
        return V49_6;
    }

    public void setV49_6(String v49_6) {
        V49_6 = v49_6;
    }

    public String getV49_7() {
        return V49_7;
    }

    public void setV49_7(String v49_7) {
        V49_7 = v49_7;
    }

    public String getV49_8() {
        return V49_8;
    }

    public void setV49_8(String v49_8) {
        V49_8 = v49_8;
    }

    public String getV49_9() {
        return V49_9;
    }

    public void setV49_9(String v49_9) {
        V49_9 = v49_9;
    }

    public String getV49_10() {
        return V49_10;
    }

    public void setV49_10(String v49_10) {
        V49_10 = v49_10;
    }

    public String getV50_1() {
        return V50_1;
    }

    public void setV50_1(String v50_1) {
        V50_1 = v50_1;
    }

    public String getV50_2() {
        return V50_2;
    }

    public void setV50_2(String v50_2) {
        V50_2 = v50_2;
    }

    public String getV50_3() {
        return V50_3;
    }

    public void setV50_3(String v50_3) {
        V50_3 = v50_3;
    }

    public String getV50_4() {
        return V50_4;
    }

    public void setV50_4(String v50_4) {
        V50_4 = v50_4;
    }

    public String getV50_5() {
        return V50_5;
    }

    public void setV50_5(String v50_5) {
        V50_5 = v50_5;
    }

    public String getV50_6() {
        return V50_6;
    }

    public void setV50_6(String v50_6) {
        V50_6 = v50_6;
    }

    public String getV50_7() {
        return V50_7;
    }

    public void setV50_7(String v50_7) {
        V50_7 = v50_7;
    }

    public String getV50_8() {
        return V50_8;
    }

    public void setV50_8(String v50_8) {
        V50_8 = v50_8;
    }

    public String getV50_9() {
        return V50_9;
    }

    public void setV50_9(String v50_9) {
        V50_9 = v50_9;
    }

    public String getV50_10() {
        return V50_10;
    }

    public void setV50_10(String v50_10) {
        V50_10 = v50_10;
    }

    public String getV51_1() {
        return V51_1;
    }

    public void setV51_1(String v51_1) {
        V51_1 = v51_1;
    }

    public String getV51_2() {
        return V51_2;
    }

    public void setV51_2(String v51_2) {
        V51_2 = v51_2;
    }

    public String getV51_3() {
        return V51_3;
    }

    public void setV51_3(String v51_3) {
        V51_3 = v51_3;
    }

    public String getV51_4() {
        return V51_4;
    }

    public void setV51_4(String v51_4) {
        V51_4 = v51_4;
    }

    public String getV51_5() {
        return V51_5;
    }

    public void setV51_5(String v51_5) {
        V51_5 = v51_5;
    }

    public String getV51_6() {
        return V51_6;
    }

    public void setV51_6(String v51_6) {
        V51_6 = v51_6;
    }

    public String getV51_7() {
        return V51_7;
    }

    public void setV51_7(String v51_7) {
        V51_7 = v51_7;
    }

    public String getV51_8() {
        return V51_8;
    }

    public void setV51_8(String v51_8) {
        V51_8 = v51_8;
    }

    public String getV51_9() {
        return V51_9;
    }

    public void setV51_9(String v51_9) {
        V51_9 = v51_9;
    }

    public String getV51_10() {
        return V51_10;
    }

    public void setV51_10(String v51_10) {
        V51_10 = v51_10;
    }

    public String getV52_1() {
        return V52_1;
    }

    public void setV52_1(String v52_1) {
        V52_1 = v52_1;
    }

    public String getV52_2() {
        return V52_2;
    }

    public void setV52_2(String v52_2) {
        V52_2 = v52_2;
    }

    public String getV52_3() {
        return V52_3;
    }

    public void setV52_3(String v52_3) {
        V52_3 = v52_3;
    }

    public String getV52_4() {
        return V52_4;
    }

    public void setV52_4(String v52_4) {
        V52_4 = v52_4;
    }

    public String getV52_5() {
        return V52_5;
    }

    public void setV52_5(String v52_5) {
        V52_5 = v52_5;
    }

    public String getV52_6() {
        return V52_6;
    }

    public void setV52_6(String v52_6) {
        V52_6 = v52_6;
    }

    public String getV52_7() {
        return V52_7;
    }

    public void setV52_7(String v52_7) {
        V52_7 = v52_7;
    }

    public String getV52_8() {
        return V52_8;
    }

    public void setV52_8(String v52_8) {
        V52_8 = v52_8;
    }

    public String getV52_9() {
        return V52_9;
    }

    public void setV52_9(String v52_9) {
        V52_9 = v52_9;
    }

    public String getV52_10() {
        return V52_10;
    }

    public void setV52_10(String v52_10) {
        V52_10 = v52_10;
    }

    public String getV53_1() {
        return V53_1;
    }

    public void setV53_1(String v53_1) {
        V53_1 = v53_1;
    }

    public String getV53_2() {
        return V53_2;
    }

    public void setV53_2(String v53_2) {
        V53_2 = v53_2;
    }

    public String getV53_3() {
        return V53_3;
    }

    public void setV53_3(String v53_3) {
        V53_3 = v53_3;
    }

    public String getV53_4() {
        return V53_4;
    }

    public void setV53_4(String v53_4) {
        V53_4 = v53_4;
    }

    public String getV53_5() {
        return V53_5;
    }

    public void setV53_5(String v53_5) {
        V53_5 = v53_5;
    }

    public String getV53_6() {
        return V53_6;
    }

    public void setV53_6(String v53_6) {
        V53_6 = v53_6;
    }

    public String getV53_7() {
        return V53_7;
    }

    public void setV53_7(String v53_7) {
        V53_7 = v53_7;
    }

    public String getV53_8() {
        return V53_8;
    }

    public void setV53_8(String v53_8) {
        V53_8 = v53_8;
    }

    public String getV53_9() {
        return V53_9;
    }

    public void setV53_9(String v53_9) {
        V53_9 = v53_9;
    }

    public String getV53_10() {
        return V53_10;
    }

    public void setV53_10(String v53_10) {
        V53_10 = v53_10;
    }

    public String getV54_1() {
        return V54_1;
    }

    public void setV54_1(String v54_1) {
        V54_1 = v54_1;
    }

    public String getV54_2() {
        return V54_2;
    }

    public void setV54_2(String v54_2) {
        V54_2 = v54_2;
    }

    public String getV54_3() {
        return V54_3;
    }

    public void setV54_3(String v54_3) {
        V54_3 = v54_3;
    }

    public String getV54_4() {
        return V54_4;
    }

    public void setV54_4(String v54_4) {
        V54_4 = v54_4;
    }

    public String getV54_5() {
        return V54_5;
    }

    public void setV54_5(String v54_5) {
        V54_5 = v54_5;
    }

    public String getV54_6() {
        return V54_6;
    }

    public void setV54_6(String v54_6) {
        V54_6 = v54_6;
    }

    public String getV54_7() {
        return V54_7;
    }

    public void setV54_7(String v54_7) {
        V54_7 = v54_7;
    }

    public String getV54_8() {
        return V54_8;
    }

    public void setV54_8(String v54_8) {
        V54_8 = v54_8;
    }

    public String getV54_9() {
        return V54_9;
    }

    public void setV54_9(String v54_9) {
        V54_9 = v54_9;
    }

    public String getV54_10() {
        return V54_10;
    }

    public void setV54_10(String v54_10) {
        V54_10 = v54_10;
    }

    public String getV55_1() {
        return V55_1;
    }

    public void setV55_1(String v55_1) {
        V55_1 = v55_1;
    }

    public String getV55_2() {
        return V55_2;
    }

    public void setV55_2(String v55_2) {
        V55_2 = v55_2;
    }

    public String getV55_3() {
        return V55_3;
    }

    public void setV55_3(String v55_3) {
        V55_3 = v55_3;
    }

    public String getV55_4() {
        return V55_4;
    }

    public void setV55_4(String v55_4) {
        V55_4 = v55_4;
    }

    public String getV55_5() {
        return V55_5;
    }

    public void setV55_5(String v55_5) {
        V55_5 = v55_5;
    }

    public String getV55_6() {
        return V55_6;
    }

    public void setV55_6(String v55_6) {
        V55_6 = v55_6;
    }

    public String getV55_7() {
        return V55_7;
    }

    public void setV55_7(String v55_7) {
        V55_7 = v55_7;
    }

    public String getV55_8() {
        return V55_8;
    }

    public void setV55_8(String v55_8) {
        V55_8 = v55_8;
    }

    public String getV55_9() {
        return V55_9;
    }

    public void setV55_9(String v55_9) {
        V55_9 = v55_9;
    }

    public String getV55_10() {
        return V55_10;
    }

    public void setV55_10(String v55_10) {
        V55_10 = v55_10;
    }

    public String getV56_1() {
        return V56_1;
    }

    public void setV56_1(String v56_1) {
        V56_1 = v56_1;
    }

    public String getV56_2() {
        return V56_2;
    }

    public void setV56_2(String v56_2) {
        V56_2 = v56_2;
    }

    public String getV56_3() {
        return V56_3;
    }

    public void setV56_3(String v56_3) {
        V56_3 = v56_3;
    }

    public String getV56_4() {
        return V56_4;
    }

    public void setV56_4(String v56_4) {
        V56_4 = v56_4;
    }

    public String getV56_5() {
        return V56_5;
    }

    public void setV56_5(String v56_5) {
        V56_5 = v56_5;
    }

    public String getV56_6() {
        return V56_6;
    }

    public void setV56_6(String v56_6) {
        V56_6 = v56_6;
    }

    public String getV56_7() {
        return V56_7;
    }

    public void setV56_7(String v56_7) {
        V56_7 = v56_7;
    }

    public String getV56_8() {
        return V56_8;
    }

    public void setV56_8(String v56_8) {
        V56_8 = v56_8;
    }

    public String getV56_9() {
        return V56_9;
    }

    public void setV56_9(String v56_9) {
        V56_9 = v56_9;
    }

    public String getV56_10() {
        return V56_10;
    }

    public void setV56_10(String v56_10) {
        V56_10 = v56_10;
    }

    public String getV57_1() {
        return V57_1;
    }

    public void setV57_1(String v57_1) {
        V57_1 = v57_1;
    }

    public String getV57_2() {
        return V57_2;
    }

    public void setV57_2(String v57_2) {
        V57_2 = v57_2;
    }

    public String getV57_3() {
        return V57_3;
    }

    public void setV57_3(String v57_3) {
        V57_3 = v57_3;
    }

    public String getV57_4() {
        return V57_4;
    }

    public void setV57_4(String v57_4) {
        V57_4 = v57_4;
    }

    public String getV57_5() {
        return V57_5;
    }

    public void setV57_5(String v57_5) {
        V57_5 = v57_5;
    }

    public String getV57_6() {
        return V57_6;
    }

    public void setV57_6(String v57_6) {
        V57_6 = v57_6;
    }

    public String getV57_7() {
        return V57_7;
    }

    public void setV57_7(String v57_7) {
        V57_7 = v57_7;
    }

    public String getV57_8() {
        return V57_8;
    }

    public void setV57_8(String v57_8) {
        V57_8 = v57_8;
    }

    public String getV57_9() {
        return V57_9;
    }

    public void setV57_9(String v57_9) {
        V57_9 = v57_9;
    }

    public String getV57_10() {
        return V57_10;
    }

    public void setV57_10(String v57_10) {
        V57_10 = v57_10;
    }

    public String getV58_1() {
        return V58_1;
    }

    public void setV58_1(String v58_1) {
        V58_1 = v58_1;
    }

    public String getV58_2() {
        return V58_2;
    }

    public void setV58_2(String v58_2) {
        V58_2 = v58_2;
    }

    public String getV58_3() {
        return V58_3;
    }

    public void setV58_3(String v58_3) {
        V58_3 = v58_3;
    }

    public String getV58_4() {
        return V58_4;
    }

    public void setV58_4(String v58_4) {
        V58_4 = v58_4;
    }

    public String getV58_5() {
        return V58_5;
    }

    public void setV58_5(String v58_5) {
        V58_5 = v58_5;
    }

    public String getV58_6() {
        return V58_6;
    }

    public void setV58_6(String v58_6) {
        V58_6 = v58_6;
    }

    public String getV58_7() {
        return V58_7;
    }

    public void setV58_7(String v58_7) {
        V58_7 = v58_7;
    }

    public String getV58_8() {
        return V58_8;
    }

    public void setV58_8(String v58_8) {
        V58_8 = v58_8;
    }

    public String getV58_9() {
        return V58_9;
    }

    public void setV58_9(String v58_9) {
        V58_9 = v58_9;
    }

    public String getV58_10() {
        return V58_10;
    }

    public void setV58_10(String v58_10) {
        V58_10 = v58_10;
    }

    public String getV59_1() {
        return V59_1;
    }

    public void setV59_1(String v59_1) {
        V59_1 = v59_1;
    }

    public String getV59_2() {
        return V59_2;
    }

    public void setV59_2(String v59_2) {
        V59_2 = v59_2;
    }

    public String getV59_3() {
        return V59_3;
    }

    public void setV59_3(String v59_3) {
        V59_3 = v59_3;
    }

    public String getV59_4() {
        return V59_4;
    }

    public void setV59_4(String v59_4) {
        V59_4 = v59_4;
    }

    public String getV59_5() {
        return V59_5;
    }

    public void setV59_5(String v59_5) {
        V59_5 = v59_5;
    }

    public String getV59_6() {
        return V59_6;
    }

    public void setV59_6(String v59_6) {
        V59_6 = v59_6;
    }

    public String getV59_7() {
        return V59_7;
    }

    public void setV59_7(String v59_7) {
        V59_7 = v59_7;
    }

    public String getV59_8() {
        return V59_8;
    }

    public void setV59_8(String v59_8) {
        V59_8 = v59_8;
    }

    public String getV59_9() {
        return V59_9;
    }

    public void setV59_9(String v59_9) {
        V59_9 = v59_9;
    }

    public String getV59_10() {
        return V59_10;
    }

    public void setV59_10(String v59_10) {
        V59_10 = v59_10;
    }

    public String getV60_1() {
        return V60_1;
    }

    public void setV60_1(String v60_1) {
        V60_1 = v60_1;
    }

    public String getV60_2() {
        return V60_2;
    }

    public void setV60_2(String v60_2) {
        V60_2 = v60_2;
    }

    public String getV60_3() {
        return V60_3;
    }

    public void setV60_3(String v60_3) {
        V60_3 = v60_3;
    }

    public String getV60_4() {
        return V60_4;
    }

    public void setV60_4(String v60_4) {
        V60_4 = v60_4;
    }

    public String getV60_5() {
        return V60_5;
    }

    public void setV60_5(String v60_5) {
        V60_5 = v60_5;
    }

    public String getV60_6() {
        return V60_6;
    }

    public void setV60_6(String v60_6) {
        V60_6 = v60_6;
    }

    public String getV60_7() {
        return V60_7;
    }

    public void setV60_7(String v60_7) {
        V60_7 = v60_7;
    }

    public String getV60_8() {
        return V60_8;
    }

    public void setV60_8(String v60_8) {
        V60_8 = v60_8;
    }

    public String getV60_9() {
        return V60_9;
    }

    public void setV60_9(String v60_9) {
        V60_9 = v60_9;
    }

    public String getV60_10() {
        return V60_10;
    }

    public void setV60_10(String v60_10) {
        V60_10 = v60_10;
    }

    public String getV61_1() {
        return V61_1;
    }

    public void setV61_1(String v61_1) {
        V61_1 = v61_1;
    }

    public String getV61_2() {
        return V61_2;
    }

    public void setV61_2(String v61_2) {
        V61_2 = v61_2;
    }

    public String getV61_3() {
        return V61_3;
    }

    public void setV61_3(String v61_3) {
        V61_3 = v61_3;
    }

    public String getV61_4() {
        return V61_4;
    }

    public void setV61_4(String v61_4) {
        V61_4 = v61_4;
    }

    public String getV61_5() {
        return V61_5;
    }

    public void setV61_5(String v61_5) {
        V61_5 = v61_5;
    }

    public String getV61_6() {
        return V61_6;
    }

    public void setV61_6(String v61_6) {
        V61_6 = v61_6;
    }

    public String getV61_7() {
        return V61_7;
    }

    public void setV61_7(String v61_7) {
        V61_7 = v61_7;
    }

    public String getV61_8() {
        return V61_8;
    }

    public void setV61_8(String v61_8) {
        V61_8 = v61_8;
    }

    public String getV61_9() {
        return V61_9;
    }

    public void setV61_9(String v61_9) {
        V61_9 = v61_9;
    }

    public String getV61_10() {
        return V61_10;
    }

    public void setV61_10(String v61_10) {
        V61_10 = v61_10;
    }

    public String getV62_1() {
        return V62_1;
    }

    public void setV62_1(String v62_1) {
        V62_1 = v62_1;
    }

    public String getV62_2() {
        return V62_2;
    }

    public void setV62_2(String v62_2) {
        V62_2 = v62_2;
    }

    public String getV62_3() {
        return V62_3;
    }

    public void setV62_3(String v62_3) {
        V62_3 = v62_3;
    }

    public String getV62_4() {
        return V62_4;
    }

    public void setV62_4(String v62_4) {
        V62_4 = v62_4;
    }

    public String getV62_5() {
        return V62_5;
    }

    public void setV62_5(String v62_5) {
        V62_5 = v62_5;
    }

    public String getV62_6() {
        return V62_6;
    }

    public void setV62_6(String v62_6) {
        V62_6 = v62_6;
    }

    public String getV62_7() {
        return V62_7;
    }

    public void setV62_7(String v62_7) {
        V62_7 = v62_7;
    }

    public String getV62_8() {
        return V62_8;
    }

    public void setV62_8(String v62_8) {
        V62_8 = v62_8;
    }

    public String getV62_9() {
        return V62_9;
    }

    public void setV62_9(String v62_9) {
        V62_9 = v62_9;
    }

    public String getV62_10() {
        return V62_10;
    }

    public void setV62_10(String v62_10) {
        V62_10 = v62_10;
    }

    public String getV63_1() {
        return V63_1;
    }

    public void setV63_1(String v63_1) {
        V63_1 = v63_1;
    }

    public String getV63_2() {
        return V63_2;
    }

    public void setV63_2(String v63_2) {
        V63_2 = v63_2;
    }

    public String getV63_3() {
        return V63_3;
    }

    public void setV63_3(String v63_3) {
        V63_3 = v63_3;
    }

    public String getV63_4() {
        return V63_4;
    }

    public void setV63_4(String v63_4) {
        V63_4 = v63_4;
    }

    public String getV63_5() {
        return V63_5;
    }

    public void setV63_5(String v63_5) {
        V63_5 = v63_5;
    }

    public String getV63_6() {
        return V63_6;
    }

    public void setV63_6(String v63_6) {
        V63_6 = v63_6;
    }

    public String getV63_7() {
        return V63_7;
    }

    public void setV63_7(String v63_7) {
        V63_7 = v63_7;
    }

    public String getV63_8() {
        return V63_8;
    }

    public void setV63_8(String v63_8) {
        V63_8 = v63_8;
    }

    public String getV63_9() {
        return V63_9;
    }

    public void setV63_9(String v63_9) {
        V63_9 = v63_9;
    }

    public String getV63_10() {
        return V63_10;
    }

    public void setV63_10(String v63_10) {
        V63_10 = v63_10;
    }

    public String getV64_1() {
        return V64_1;
    }

    public void setV64_1(String v64_1) {
        V64_1 = v64_1;
    }

    public String getV64_2() {
        return V64_2;
    }

    public void setV64_2(String v64_2) {
        V64_2 = v64_2;
    }

    public String getV64_3() {
        return V64_3;
    }

    public void setV64_3(String v64_3) {
        V64_3 = v64_3;
    }

    public String getV64_4() {
        return V64_4;
    }

    public void setV64_4(String v64_4) {
        V64_4 = v64_4;
    }

    public String getV64_5() {
        return V64_5;
    }

    public void setV64_5(String v64_5) {
        V64_5 = v64_5;
    }

    public String getV64_6() {
        return V64_6;
    }

    public void setV64_6(String v64_6) {
        V64_6 = v64_6;
    }

    public String getV64_7() {
        return V64_7;
    }

    public void setV64_7(String v64_7) {
        V64_7 = v64_7;
    }

    public String getV64_8() {
        return V64_8;
    }

    public void setV64_8(String v64_8) {
        V64_8 = v64_8;
    }

    public String getV64_9() {
        return V64_9;
    }

    public void setV64_9(String v64_9) {
        V64_9 = v64_9;
    }

    public String getV64_10() {
        return V64_10;
    }

    public void setV64_10(String v64_10) {
        V64_10 = v64_10;
    }

    public String getMEDL() {
        return MEDL;
    }

    public void setMEDL(String MEDL) {
        this.MEDL = MEDL;
    }

    public String getLMX_1() {
        return LMX_1;
    }

    public void setLMX_1(String LMX_1) {
        this.LMX_1 = LMX_1;
    }

    public String getLMX_2() {
        return LMX_2;
    }

    public void setLMX_2(String LMX_2) {
        this.LMX_2 = LMX_2;
    }

    public String getLMX_3() {
        return LMX_3;
    }

    public void setLMX_3(String LMX_3) {
        this.LMX_3 = LMX_3;
    }

    public String getLMX_4() {
        return LMX_4;
    }

    public void setLMX_4(String LMX_4) {
        this.LMX_4 = LMX_4;
    }

    public String getLMX_5() {
        return LMX_5;
    }

    public void setLMX_5(String LMX_5) {
        this.LMX_5 = LMX_5;
    }

    public String getLMX_6() {
        return LMX_6;
    }

    public void setLMX_6(String LMX_6) {
        this.LMX_6 = LMX_6;
    }

    public String getLMX_7() {
        return LMX_7;
    }

    public void setLMX_7(String LMX_7) {
        this.LMX_7 = LMX_7;
    }

    public String getMANCHECK_1() {
        return MANCHECK_1;
    }

    public void setMANCHECK_1(String MANCHECK_1) {
        this.MANCHECK_1 = MANCHECK_1;
    }

    public String getMANCHECK_2() {
        return MANCHECK_2;
    }

    public void setMANCHECK_2(String MANCHECK_2) {
        this.MANCHECK_2 = MANCHECK_2;
    }

    public String getMANCHECK_3() {
        return MANCHECK_3;
    }

    public void setMANCHECK_3(String MANCHECK_3) {
        this.MANCHECK_3 = MANCHECK_3;
    }

    public String getMANCHECK_4() {
        return MANCHECK_4;
    }

    public void setMANCHECK_4(String MANCHECK_4) {
        this.MANCHECK_4 = MANCHECK_4;
    }

    public String getMANCHECK_5() {
        return MANCHECK_5;
    }

    public void setMANCHECK_5(String MANCHECK_5) {
        this.MANCHECK_5 = MANCHECK_5;
    }

    public String getMANCHECK_6() {
        return MANCHECK_6;
    }

    public void setMANCHECK_6(String MANCHECK_6) {
        this.MANCHECK_6 = MANCHECK_6;
    }

    public String getMANCHECK_7() {
        return MANCHECK_7;
    }

    public void setMANCHECK_7(String MANCHECK_7) {
        this.MANCHECK_7 = MANCHECK_7;
    }

    public String getMANCHECK_8() {
        return MANCHECK_8;
    }

    public void setMANCHECK_8(String MANCHECK_8) {
        this.MANCHECK_8 = MANCHECK_8;
    }

    private String V19_5;
    private String V19_6;
    private String V19_7;
    private String V19_8;
    private String V19_9;
    private String V19_10;
    private String V20_1;
    private String V20_2;
    private String V20_3;
    private String V20_4;
    private String V20_5;
    private String V20_6;
    private String V20_7;
    private String V20_8;
    private String V20_9;
    private String V20_10;
    private String V21_1;
    private String V21_2;
    private String V21_3;
    private String V21_4;
    private String V21_5;
    private String V21_6;
    private String V21_7;
    private String V21_8;
    private String V21_9;
    private String V21_10;
    private String V22_1;
    private String V22_2;
    private String V22_3;
    private String V22_4;
    private String V22_5;
    private String V22_6;
    private String V22_7;
    private String V22_8;
    private String V22_9;
    private String V22_10;
    private String V23_1;
    private String V23_2;
    private String V23_3;
    private String V23_4;
    private String V23_5;
    private String V23_6;
    private String V23_7;
    private String V23_8;
    private String V23_9;
    private String V23_10;
    private String V24_1;
    private String V24_2;
    private String V24_3;
    private String V24_4;
    private String V24_5;
    private String V24_6;
    private String V24_7;
    private String V24_8;
    private String V24_9;
    private String V24_10;
    private String V25_1;
    private String V25_2;
    private String V25_3;
    private String V25_4;
    private String V25_5;
    private String V25_6;
    private String V25_7;
    private String V25_8;
    private String V25_9;
    private String V25_10;
    private String V26_1;
    private String V26_2;
    private String V26_3;
    private String V26_4;
    private String V26_5;
    private String V26_6;
    private String V26_7;
    private String V26_8;
    private String V26_9;
    private String V26_10;
    private String V27_1;
    private String V27_2;
    private String V27_3;
    private String V27_4;
    private String V27_5;
    private String V27_6;
    private String V27_7;
    private String V27_8;
    private String V27_9;
    private String V27_10;
    private String V28_1;
    private String V28_2;
    private String V28_3;
    private String V28_4;
    private String V28_5;
    private String V28_6;
    private String V28_7;
    private String V28_8;
    private String V28_9;
    private String V28_10;
    private String V29_1;
    private String V29_2;
    private String V29_3;
    private String V29_4;
    private String V29_5;
    private String V29_6;
    private String V29_7;
    private String V29_8;
    private String V29_9;
    private String V29_10;
    private String V30_1;
    private String V30_2;
    private String V30_3;
    private String V30_4;
    private String V30_5;
    private String V30_6;
    private String V30_7;
    private String V30_8;
    private String V30_9;
    private String V30_10;
    private String V31_1;
    private String V31_2;
    private String V31_3;
    private String V31_4;
    private String V31_5;
    private String V31_6;
    private String V31_7;
    private String V31_8;
    private String V31_9;
    private String V31_10;
    private String V32_1;
    private String V32_2;
    private String V32_3;
    private String V32_4;
    private String V32_5;
    private String V32_6;
    private String V32_7;
    private String V32_8;
    private String V32_9;
    private String V32_10;
    private String V33_1;
    private String V33_2;
    private String V33_3;
    private String V33_4;
    private String V33_5;
    private String V33_6;
    private String V33_7;
    private String V33_8;
    private String V33_9;
    private String V33_10;
    private String V34_1;
    private String V34_2;
    private String V34_3;
    private String V34_4;
    private String V34_5;
    private String V34_6;
    private String V34_7;
    private String V34_8;
    private String V34_9;
    private String V34_10;
    private String V35_1;
    private String V35_2;
    private String V35_3;
    private String V35_4;
    private String V35_5;
    private String V35_6;
    private String V35_7;
    private String V35_8;
    private String V35_9;
    private String V35_10;
    private String V36_1;
    private String V36_2;
    private String V36_3;
    private String V36_4;
    private String V36_5;
    private String V36_6;
    private String V36_7;
    private String V36_8;
    private String V36_9;
    private String V36_10;
    private String V37_1;
    private String V37_2;
    private String V37_3;
    private String V37_4;
    private String V37_5;
    private String V37_6;
    private String V37_7;
    private String V37_8;
    private String V37_9;
    private String V37_10;
    private String V38_1;
    private String V38_2;
    private String V38_3;
    private String V38_4;
    private String V38_5;
    private String V38_6;
    private String V38_7;
    private String V38_8;
    private String V38_9;
    private String V38_10;
    private String V39_1;
    private String V39_2;
    private String V39_3;
    private String V39_4;
    private String V39_5;
    private String V39_6;
    private String V39_7;
    private String V39_8;
    private String V39_9;
    private String V39_10;
    private String V40_1;
    private String V40_2;
    private String V40_3;
    private String V40_4;
    private String V40_5;
    private String V40_6;
    private String V40_7;
    private String V40_8;
    private String V40_9;
    private String V40_10;
    private String V41_1;
    private String V41_2;
    private String V41_3;
    private String V41_4;
    private String V41_5;
    private String V41_6;
    private String V41_7;
    private String V41_8;
    private String V41_9;
    private String V41_10;
    private String V42_1;
    private String V42_2;
    private String V42_3;
    private String V42_4;
    private String V42_5;
    private String V42_6;
    private String V42_7;
    private String V42_8;
    private String V42_9;
    private String V42_10;
    private String V43_1;
    private String V43_2;
    private String V43_3;
    private String V43_4;
    private String V43_5;
    private String V43_6;
    private String V43_7;
    private String V43_8;
    private String V43_9;
    private String V43_10;
    private String V44_1;
    private String V44_2;
    private String V44_3;
    private String V44_4;
    private String V44_5;
    private String V44_6;
    private String V44_7;
    private String V44_8;
    private String V44_9;
    private String V44_10;
    private String V45_1;
    private String V45_2;
    private String V45_3;
    private String V45_4;
    private String V45_5;
    private String V45_6;
    private String V45_7;
    private String V45_8;
    private String V45_9;
    private String V45_10;
    private String V46_1;
    private String V46_2;
    private String V46_3;
    private String V46_4;
    private String V46_5;
    private String V46_6;
    private String V46_7;
    private String V46_8;
    private String V46_9;
    private String V46_10;
    private String V47_1;
    private String V47_2;
    private String V47_3;
    private String V47_4;
    private String V47_5;
    private String V47_6;
    private String V47_7;
    private String V47_8;
    private String V47_9;
    private String V47_10;
    private String V48_1;
    private String V48_2;
    private String V48_3;
    private String V48_4;
    private String V48_5;
    private String V48_6;
    private String V48_7;
    private String V48_8;
    private String V48_9;
    private String V48_10;
    private String V49_1;
    private String V49_2;
    private String V49_3;
    private String V49_4;
    private String V49_5;
    private String V49_6;
    private String V49_7;
    private String V49_8;
    private String V49_9;
    private String V49_10;
    private String V50_1;
    private String V50_2;
    private String V50_3;
    private String V50_4;
    private String V50_5;
    private String V50_6;
    private String V50_7;
    private String V50_8;
    private String V50_9;
    private String V50_10;
    private String V51_1;
    private String V51_2;
    private String V51_3;
    private String V51_4;
    private String V51_5;
    private String V51_6;
    private String V51_7;
    private String V51_8;
    private String V51_9;
    private String V51_10;
    private String V52_1;
    private String V52_2;
    private String V52_3;
    private String V52_4;
    private String V52_5;
    private String V52_6;
    private String V52_7;
    private String V52_8;
    private String V52_9;
    private String V52_10;
    private String V53_1;
    private String V53_2;
    private String V53_3;
    private String V53_4;
    private String V53_5;
    private String V53_6;
    private String V53_7;
    private String V53_8;
    private String V53_9;
    private String V53_10;
    private String V54_1;
    private String V54_2;
    private String V54_3;
    private String V54_4;
    private String V54_5;
    private String V54_6;
    private String V54_7;
    private String V54_8;
    private String V54_9;
    private String V54_10;
    private String V55_1;
    private String V55_2;
    private String V55_3;
    private String V55_4;
    private String V55_5;
    private String V55_6;
    private String V55_7;
    private String V55_8;
    private String V55_9;
    private String V55_10;
    private String V56_1;
    private String V56_2;
    private String V56_3;
    private String V56_4;
    private String V56_5;
    private String V56_6;
    private String V56_7;
    private String V56_8;
    private String V56_9;
    private String V56_10;
    private String V57_1;
    private String V57_2;
    private String V57_3;
    private String V57_4;
    private String V57_5;
    private String V57_6;
    private String V57_7;
    private String V57_8;
    private String V57_9;
    private String V57_10;
    private String V58_1;
    private String V58_2;
    private String V58_3;
    private String V58_4;
    private String V58_5;
    private String V58_6;
    private String V58_7;
    private String V58_8;
    private String V58_9;
    private String V58_10;
    private String V59_1;
    private String V59_2;
    private String V59_3;
    private String V59_4;
    private String V59_5;
    private String V59_6;
    private String V59_7;
    private String V59_8;
    private String V59_9;
    private String V59_10;
    private String V60_1;
    private String V60_2;
    private String V60_3;
    private String V60_4;
    private String V60_5;
    private String V60_6;
    private String V60_7;
    private String V60_8;
    private String V60_9;
    private String V60_10;
    private String V61_1;
    private String V61_2;
    private String V61_3;
    private String V61_4;
    private String V61_5;
    private String V61_6;
    private String V61_7;
    private String V61_8;
    private String V61_9;
    private String V61_10;
    private String V62_1;
    private String V62_2;
    private String V62_3;
    private String V62_4;
    private String V62_5;
    private String V62_6;
    private String V62_7;
    private String V62_8;
    private String V62_9;
    private String V62_10;
    private String V63_1;
    private String V63_2;
    private String V63_3;
    private String V63_4;
    private String V63_5;
    private String V63_6;
    private String V63_7;
    private String V63_8;
    private String V63_9;
    private String V63_10;
    private String V64_1;
    private String V64_2;
    private String V64_3;
    private String V64_4;
    private String V64_5;
    private String V64_6;
    private String V64_7;
    private String V64_8;
    private String V64_9;
    private String V64_10;
    private String MEDL;
    private String LMX_1;
    private String LMX_2;
    private String LMX_3;
    private String LMX_4;
    private String LMX_5;
    private String LMX_6;
    private String LMX_7;
    private String MANCHECK_1;
    private String MANCHECK_2;
    private String MANCHECK_3;
    private String MANCHECK_4;
    private String MANCHECK_5;
    private String MANCHECK_6;
    private String MANCHECK_7;
    private String MANCHECK_8;

    public int getVignetteFilled_1() {
        return vignetteFilled_1;
    }

    public void setVignetteFilled_1(int vignetteFilled_1) {
        this.vignetteFilled_1 = vignetteFilled_1;
    }

    private int vignetteFilled_1;

    public int getVignetteFilled_2() {
        return vignetteFilled_2;
    }

    public void setVignetteFilled_2(int vignetteFilled_2) {
        this.vignetteFilled_2 = vignetteFilled_2;
    }

    public int getVignetteFilled_3() {
        return vignetteFilled_3;
    }

    public void setVignetteFilled_3(int vignetteFilled_3) {
        this.vignetteFilled_3 = vignetteFilled_3;
    }

    public int getVignetteFilled_4() {
        return vignetteFilled_4;
    }

    public void setVignetteFilled_4(int vignetteFilled_4) {
        this.vignetteFilled_4 = vignetteFilled_4;
    }

    public int getVignetteFilled_5() {
        return vignetteFilled_5;
    }

    public void setVignetteFilled_5(int vignetteFilled_5) {
        this.vignetteFilled_5 = vignetteFilled_5;
    }

    public int getVignetteFilled_6() {
        return vignetteFilled_6;
    }

    public void setVignetteFilled_6(int vignetteFilled_6) {
        this.vignetteFilled_6 = vignetteFilled_6;
    }

    private int vignetteFilled_2;
    private int vignetteFilled_3;
    private int vignetteFilled_4;
    private int vignetteFilled_5;
    private int vignetteFilled_6;

    public void setVignetteFilled(int i, String content, int vignetteNumber){

        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("checking line number " + i);

        if(content.contains("1") ||
                content.contains("2") ||
                content.contains("3") ||
                content.contains("4") ||
                content.contains("5") ||
                content.contains("6") ||
                content.contains("7") ){

            setVignetteFilled(vignetteNumber);
            System.out.println("vignette " + vignetteNumber + " was filled in");

        }
        else{
            System.out.println("not filled ");
        }
        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++");

    }


    public void setVignetteFilled(int i){

        if (vignetteFilled_1 < 1){
            setVignetteFilled_1(i);
            System.out.println("vignette " + i + "  filled  in vignetteFilled_1");
        }
        else if(vignetteFilled_2 < 1){
            setVignetteFilled_2(i);
            System.out.println("vignette " + i + "  filled  in vignetteFilled_2");
        }
        else if(vignetteFilled_3 < 1){
            setVignetteFilled_3(i);
            System.out.println("vignette " + i + "  filled  in vignetteFilled_3");
        }
        else if(vignetteFilled_4  < 1){
            setVignetteFilled_4 (i);
            System.out.println("vignette " + i + "  filled  in vignetteFilled_4");
        }
        else if(vignetteFilled_5 < 1){
            setVignetteFilled_5(i);
            System.out.println("vignette " + i + "  filled  in vignetteFilled_5");
        }
        else if(vignetteFilled_6 < 1){
            setVignetteFilled_6(i);
            System.out.println("vignette " + i + "  filled  in vignetteFilled_6");

            System.out.println("----------------------------------------------------");
            System.out.println("vignette " + this.vignetteFilled_1 + "  filled  in vignetteFilled_1");
            System.out.println("vignette " + this.vignetteFilled_2 + "  filled  in vignetteFilled_2");
            System.out.println("vignette " + this.vignetteFilled_3 + "  filled  in vignetteFilled_3");
            System.out.println("vignette " + this.vignetteFilled_4 + "  filled  in vignetteFilled_4");
            System.out.println("vignette " + this.vignetteFilled_5 + "  filled  in vignetteFilled_5");
            System.out.println("vignette " + this.vignetteFilled_6 + "  filled  in vignetteFilled_6");
            System.out.println("----------------------------------------------------");
        }

    }

    public String createNewFileLine_1(int clientNumber){

        String newLine = "";

        String end =  this.generateNewLineEnd();

        System.out.println("vignette 1 was filled with " + vignetteFilled_1);

        String start = this.generateNewLineStartVignette_1(clientNumber);


        if(this.vignetteFilled_1 == 1){

            newLine = start + end + getV1_1() + ";" + getV1_2() + ";"
                    + getV1_3() + ";" + getV1_4() + ";" + getV1_5()
                    +";" + getV1_6() + ";" + getV1_7() + ";" + getV1_8()
                    + ";" + getV1_9() + ";" + getV1_10() ;

        }
        else if(vignetteFilled_1 == 2){
            newLine = start + end + getV2_1() + ";" + getV2_2() + ";"
                    + getV2_3() + ";" + getV2_4() + ";" + getV2_5()
                    +";" + getV2_6() + ";" + getV2_7() + ";" + getV2_8()
                    + ";" + getV2_9() + ";" + getV2_10() ;
        }
        else if(vignetteFilled_1 == 3){
            newLine = start + end + getV3_1() + ";" + getV3_2() + ";"
                    + getV3_3() + ";" + getV3_4() + ";" + getV3_5()
                    +";" + getV3_6() + ";" + getV3_7() + ";" + getV3_8()
                    + ";" + getV3_9() + ";" + getV3_10() ;
        }
        else if(vignetteFilled_1 == 4){
            newLine = start + end + getV4_1() + ";" + getV4_2() + ";"
                    + getV4_3() + ";" + getV4_4() + ";" + getV4_5()
                    +";" + getV4_6() + ";" + getV4_7() + ";" + getV4_8()
                    + ";" + getV4_9() + ";" + getV4_10() ;
        }
        else if(vignetteFilled_1 == 5){
            newLine = start + end + getV5_1() + ";" + getV5_2() + ";"
                    + getV5_3() + ";" + getV5_4() + ";" + getV5_5()
                    +";" + getV5_6() + ";" + getV5_7() + ";" + getV5_8()
                    + ";" + getV5_9() + ";" + getV5_10() ;
        }
        else if(vignetteFilled_1 == 6){
            newLine = start + end + getV6_1() + ";" + getV6_2() + ";"
                    + getV6_3() + ";" + getV6_4() + ";" + getV6_5()
                    +";" + getV6_6() + ";" + getV6_7() + ";" + getV6_8()
                    + ";" + getV6_9() + ";" + getV6_10() ;
        }
        else if(vignetteFilled_1 == 7){
            newLine = start + end + getV7_1() + ";" + getV7_2() + ";"
                    + getV7_3() + ";" + getV7_4() + ";" + getV7_5()
                    +";" + getV7_6() + ";" + getV7_7() + ";" + getV7_8()
                    + ";" + getV7_9() + ";" + getV7_10() ;
        }
        else if(vignetteFilled_1 == 8){
            newLine = start + end + getV8_1() + ";" + getV8_2() + ";"
                    + getV8_3() + ";" + getV8_4() + ";" + getV8_5()
                    +";" + getV8_6() + ";" + getV8_7() + ";" + getV8_8()
                    + ";" + getV8_9() + ";" + getV8_10() ;
        }
        else if(vignetteFilled_1 == 9){
            newLine = start + end + getV9_1() + ";" + getV9_2() + ";"
                    + getV9_3() + ";" + getV9_4() + ";" + getV9_5()
                    +";" + getV9_6() + ";" + getV9_7() + ";" + getV9_8()
                    + ";" + getV9_9() + ";" + getV9_10() ;
        }
        else if(vignetteFilled_1 == 10){
            newLine = start + end + getV10_1() + ";" + getV10_2() + ";"
                    + getV10_3() + ";" + getV10_4() + ";" + getV10_5()
                    +";" + getV10_6() + ";" + getV10_7() + ";" + getV10_8()
                    + ";" + getV10_9() + ";" + getV10_10() ;
        }


        else if(vignetteFilled_1 == 11){

            newLine = start + end + getV11_1() + ";" + getV11_2() + ";"
                    + getV11_3() + ";" + getV11_4() + ";" + getV11_5()
                    +";" + getV11_6() + ";" + getV11_7() + ";" + getV11_8()
                    + ";" + getV11_9() + ";" + getV11_10() ;

        }
        else if(vignetteFilled_1 == 12){
            newLine = start + end + getV12_1() + ";" + getV12_2() + ";"
                    + getV12_3() + ";" + getV12_4() + ";" + getV12_5()
                    +";" + getV12_6() + ";" + getV12_7() + ";" + getV12_8()
                    + ";" + getV12_9() + ";" + getV12_10() ;
        }
        else if(vignetteFilled_1 == 13){
            newLine = start + end + getV13_1() + ";" + getV13_2() + ";"
                    + getV13_3() + ";" + getV13_4() + ";" + getV13_5()
                    +";" + getV13_6() + ";" + getV13_7() + ";" + getV13_8()
                    + ";" + getV13_9() + ";" + getV13_10() ;
        }
        else if(vignetteFilled_1 == 14){
            newLine = start + end + getV14_1() + ";" + getV14_2() + ";"
                    + getV14_3() + ";" + getV14_4() + ";" + getV14_5()
                    +";" + getV14_6() + ";" + getV14_7() + ";" + getV14_8()
                    + ";" + getV14_9() + ";" + getV14_10() ;
        }
        else if(vignetteFilled_1 == 15){
            newLine = start + end + getV15_1() + ";" + getV15_2() + ";"
                    + getV15_3() + ";" + getV15_4() + ";" + getV15_5()
                    +";" + getV15_6() + ";" + getV15_7() + ";" + getV15_8()
                    + ";" + getV15_9() + ";" + getV15_10() ;
        }
        else if(vignetteFilled_1 == 16){
            newLine = start + end + getV16_1() + ";" + getV16_2() + ";"
                    + getV16_3() + ";" + getV16_4() + ";" + getV16_5()
                    +";" + getV16_6() + ";" + getV16_7() + ";" + getV16_8()
                    + ";" + getV16_9() + ";" + getV16_10() ;
        }
        else if(vignetteFilled_1 == 17){
            newLine = start + end + getV17_1() + ";" + getV17_2() + ";"
                    + getV17_3() + ";" + getV17_4() + ";" + getV17_5()
                    +";" + getV17_6() + ";" + getV17_7() + ";" + getV17_8()
                    + ";" + getV17_9() + ";" + getV17_10() ;
        }
        else if(vignetteFilled_1 == 18){
            newLine = start + end + getV18_1() + ";" + getV18_2() + ";"
                    + getV18_3() + ";" + getV18_4() + ";" + getV18_5()
                    +";" + getV18_6() + ";" + getV18_7() + ";" + getV18_8()
                    + ";" + getV18_9() + ";" + getV18_10() ;
        }
        else if(vignetteFilled_1 == 19){
            newLine = start + end + getV19_1() + ";" + getV19_2() + ";"
                    + getV19_3() + ";" + getV19_4() + ";" + getV19_5()
                    +";" + getV19_6() + ";" + getV19_7() + ";" + getV19_8()
                    + ";" + getV19_9() + ";" + getV19_10() ;
        }



        else if(vignetteFilled_1 == 20){
            newLine = start + end + getV20_1() + ";" + getV20_2() + ";"
                    + getV20_3() + ";" + getV20_4() + ";" + getV20_5()
                    +";" + getV20_6() + ";" + getV20_7() + ";" + getV20_8()
                    + ";" + getV20_9() + ";" + getV20_10() ;
        }
        else if(vignetteFilled_1 == 21){

            newLine = start + end + getV21_1() + ";" + getV21_2() + ";"
                    + getV21_3() + ";" + getV21_4() + ";" + getV21_5()
                    +";" + getV21_6() + ";" + getV21_7() + ";" + getV21_8()
                    + ";" + getV21_9() + ";" + getV21_10() ;

        }
        else if(vignetteFilled_1 == 22){
            newLine = start + end + getV22_1() + ";" + getV22_2() + ";"
                    + getV22_3() + ";" + getV22_4() + ";" + getV22_5()
                    +";" + getV22_6() + ";" + getV22_7() + ";" + getV22_8()
                    + ";" + getV22_9() + ";" + getV22_10() ;
        }
        else if(vignetteFilled_1 == 23){
            newLine = start + end + getV23_1() + ";" + getV23_2() + ";"
                    + getV23_3() + ";" + getV23_4() + ";" + getV23_5()
                    +";" + getV23_6() + ";" + getV23_7() + ";" + getV23_8()
                    + ";" + getV23_9() + ";" + getV23_10() ;
        }
        else if(vignetteFilled_1 == 24){
            newLine = start + end + getV24_1() + ";" + getV24_2() + ";"
                    + getV24_3() + ";" + getV24_4() + ";" + getV24_5()
                    +";" + getV24_6() + ";" + getV24_7() + ";" + getV24_8()
                    + ";" + getV24_9() + ";" + getV24_10() ;
        }
        else if(vignetteFilled_1 == 25){
            newLine = start + end + getV25_1() + ";" + getV25_2() + ";"
                    + getV25_3() + ";" + getV25_4() + ";" + getV25_5()
                    +";" + getV25_6() + ";" + getV25_7() + ";" + getV25_8()
                    + ";" + getV25_9() + ";" + getV25_10() ;
        }
        else if(vignetteFilled_1 == 26){
            newLine = start + end + getV26_1() + ";" + getV26_2() + ";"
                    + getV26_3() + ";" + getV26_4() + ";" + getV26_5()
                    +";" + getV26_6() + ";" + getV26_7() + ";" + getV26_8()
                    + ";" + getV26_9() + ";" + getV26_10() ;
        }
        else if(vignetteFilled_1 == 27){
            newLine = start + end + getV27_1() + ";" + getV27_2() + ";"
                    + getV27_3() + ";" + getV27_4() + ";" + getV27_5()
                    +";" + getV27_6() + ";" + getV27_7() + ";" + getV27_8()
                    + ";" + getV27_9() + ";" + getV27_10() ;
        }
        else if(vignetteFilled_1 == 28){
            newLine = start + end + getV28_1() + ";" + getV28_2() + ";"
                    + getV28_3() + ";" + getV28_4() + ";" + getV28_5()
                    +";" + getV28_6() + ";" + getV28_7() + ";" + getV28_8()
                    + ";" + getV28_9() + ";" + getV28_10() ;
        }
        else if(vignetteFilled_1 == 29){
            newLine = start + end + getV29_1() + ";" + getV29_2() + ";"
                    + getV29_3() + ";" + getV29_4() + ";" + getV29_5()
                    +";" + getV29_6() + ";" + getV29_7() + ";" + getV29_8()
                    + ";" + getV29_9() + ";" + getV29_10() ;
        }


        else if(vignetteFilled_1 == 30){
            newLine = start + end + getV30_1() + ";" + getV30_2() + ";"
                    + getV30_3() + ";" + getV30_4() + ";" + getV30_5()
                    +";" + getV30_6() + ";" + getV30_7() + ";" + getV30_8()
                    + ";" + getV30_9() + ";" + getV30_10() ;
        }
        else if(vignetteFilled_1 == 31){

            newLine = start + end + getV31_1() + ";" + getV31_2() + ";"
                    + getV31_3() + ";" + getV31_4() + ";" + getV31_5()
                    +";" + getV31_6() + ";" + getV31_7() + ";" + getV31_8()
                    + ";" + getV31_9() + ";" + getV31_10() ;

        }
        else if(vignetteFilled_1 == 32){
            newLine = start + end + getV32_1() + ";" + getV32_2() + ";"
                    + getV32_3() + ";" + getV32_4() + ";" + getV32_5()
                    +";" + getV32_6() + ";" + getV32_7() + ";" + getV32_8()
                    + ";" + getV32_9() + ";" + getV32_10() ;
        }
        else if(vignetteFilled_1 == 33){
            newLine = start + end + getV33_1() + ";" + getV33_2() + ";"
                    + getV33_3() + ";" + getV33_4() + ";" + getV33_5()
                    +";" + getV33_6() + ";" + getV33_7() + ";" + getV33_8()
                    + ";" + getV33_9() + ";" + getV33_10() ;
        }
        else if(vignetteFilled_1 == 34){
            newLine = start + end + getV34_1() + ";" + getV34_2() + ";"
                    + getV34_3() + ";" + getV34_4() + ";" + getV34_5()
                    +";" + getV34_6() + ";" + getV34_7() + ";" + getV34_8()
                    + ";" + getV34_9() + ";" + getV34_10() ;
        }
        else if(vignetteFilled_1 == 35){
            newLine = start + end + getV35_1() + ";" + getV35_2() + ";"
                    + getV35_3() + ";" + getV35_4() + ";" + getV35_5()
                    +";" + getV35_6() + ";" + getV35_7() + ";" + getV35_8()
                    + ";" + getV35_9() + ";" + getV35_10() ;
        }
        else if(vignetteFilled_1 == 36){
            newLine = start + end + getV36_1() + ";" + getV36_2() + ";"
                    + getV36_3() + ";" + getV36_4() + ";" + getV36_5()
                    +";" + getV36_6() + ";" + getV36_7() + ";" + getV36_8()
                    + ";" + getV36_9() + ";" + getV36_10() ;
        }
        else if(vignetteFilled_1 == 37){
            newLine = start + end + getV37_1() + ";" + getV37_2() + ";"
                    + getV37_3() + ";" + getV37_4() + ";" + getV37_5()
                    +";" + getV37_6() + ";" + getV37_7() + ";" + getV37_8()
                    + ";" + getV37_9() + ";" + getV37_10() ;
        }
        else if(vignetteFilled_1 == 38){
            newLine = start + end + getV38_1() + ";" + getV38_2() + ";"
                    + getV38_3() + ";" + getV38_4() + ";" + getV38_5()
                    +";" + getV38_6() + ";" + getV38_7() + ";" + getV38_8()
                    + ";" + getV38_9() + ";" + getV38_10() ;
        }
        else if(vignetteFilled_1 == 39){
            newLine = start + end + getV39_1() + ";" + getV39_2() + ";"
                    + getV39_3() + ";" + getV39_4() + ";" + getV39_5()
                    +";" + getV39_6() + ";" + getV39_7() + ";" + getV39_8()
                    + ";" + getV39_9() + ";" + getV39_10() ;
        }

        else if(vignetteFilled_1 == 40){
            newLine = start + end + getV40_1() + ";" + getV40_2() + ";"
                    + getV40_3() + ";" + getV40_4() + ";" + getV40_5()
                    +";" + getV40_6() + ";" + getV40_7() + ";" + getV40_8()
                    + ";" + getV40_9() + ";" + getV40_10() ;
        }
        else if(vignetteFilled_1 == 41){

            newLine = start + end + getV41_1() + ";" + getV41_2() + ";"
                    + getV41_3() + ";" + getV41_4() + ";" + getV41_5()
                    +";" + getV41_6() + ";" + getV41_7() + ";" + getV41_8()
                    + ";" + getV41_9() + ";" + getV41_10() ;

        }
        else if(vignetteFilled_1 == 42){
            newLine = start + end + getV42_1() + ";" + getV42_2() + ";"
                    + getV42_3() + ";" + getV42_4() + ";" + getV42_5()
                    +";" + getV42_6() + ";" + getV42_7() + ";" + getV42_8()
                    + ";" + getV42_9() + ";" + getV42_10() ;
        }
        else if(vignetteFilled_1 == 43){
            newLine = start + end + getV43_1() + ";" + getV43_2() + ";"
                    + getV43_3() + ";" + getV43_4() + ";" + getV43_5()
                    +";" + getV43_6() + ";" + getV43_7() + ";" + getV43_8()
                    + ";" + getV43_9() + ";" + getV43_10() ;
        }
        else if(vignetteFilled_1 == 44){
            newLine = start + end + getV44_1() + ";" + getV44_2() + ";"
                    + getV44_3() + ";" + getV44_4() + ";" + getV44_5()
                    +";" + getV44_6() + ";" + getV44_7() + ";" + getV44_8()
                    + ";" + getV44_9() + ";" + getV44_10() ;
        }
        else if(vignetteFilled_1 == 45){
            newLine = start + end + getV45_1() + ";" + getV45_2() + ";"
                    + getV45_3() + ";" + getV45_4() + ";" + getV45_5()
                    +";" + getV45_6() + ";" + getV45_7() + ";" + getV45_8()
                    + ";" + getV45_9() + ";" + getV45_10() ;
        }
        else if(vignetteFilled_1 == 46){
            newLine = start + end + getV46_1() + ";" + getV46_2() + ";"
                    + getV46_3() + ";" + getV46_4() + ";" + getV46_5()
                    +";" + getV46_6() + ";" + getV46_7() + ";" + getV46_8()
                    + ";" + getV46_9() + ";" + getV46_10() ;
        }
        else if(vignetteFilled_1 == 47){
            newLine = start + end + getV47_1() + ";" + getV47_2() + ";"
                    + getV47_3() + ";" + getV47_4() + ";" + getV47_5()
                    +";" + getV47_6() + ";" + getV47_7() + ";" + getV47_8()
                    + ";" + getV47_9() + ";" + getV47_10() ;
        }
        else if(vignetteFilled_1 == 48){
            newLine = start + end + getV48_1() + ";" + getV48_2() + ";"
                    + getV48_3() + ";" + getV48_4() + ";" + getV48_5()
                    +";" + getV48_6() + ";" + getV48_7() + ";" + getV48_8()
                    + ";" + getV48_9() + ";" + getV48_10() ;
        }
        else if(vignetteFilled_1 == 49){
            newLine = start + end + getV49_1() + ";" + getV49_2() + ";"
                    + getV49_3() + ";" + getV49_4() + ";" + getV49_5()
                    +";" + getV49_6() + ";" + getV49_7() + ";" + getV49_8()
                    + ";" + getV49_9() + ";" + getV49_10() ;
        }


        else if(vignetteFilled_1 == 50){
            newLine = start + end + getV50_1() + ";" + getV50_2() + ";"
                    + getV50_3() + ";" + getV50_4() + ";" + getV50_5()
                    +";" + getV50_6() + ";" + getV50_7() + ";" + getV50_8()
                    + ";" + getV50_9() + ";" + getV50_10() ;
        }
        else if(vignetteFilled_1 == 51){

            newLine = start + end + getV51_1() + ";" + getV51_2() + ";"
                    + getV51_3() + ";" + getV51_4() + ";" + getV51_5()
                    +";" + getV51_6() + ";" + getV51_7() + ";" + getV51_8()
                    + ";" + getV51_9() + ";" + getV51_10() ;

        }
        else if(vignetteFilled_1 == 52){
            newLine = start + end + getV52_1() + ";" + getV52_2() + ";"
                    + getV52_3() + ";" + getV52_4() + ";" + getV52_5()
                    +";" + getV52_6() + ";" + getV52_7() + ";" + getV52_8()
                    + ";" + getV52_9() + ";" + getV52_10() ;
        }
        else if(vignetteFilled_1 == 53){
            newLine = start + end + getV53_1() + ";" + getV53_2() + ";"
                    + getV53_3() + ";" + getV53_4() + ";" + getV53_5()
                    +";" + getV53_6() + ";" + getV53_7() + ";" + getV53_8()
                    + ";" + getV53_9() + ";" + getV53_10() ;
        }
        else if(vignetteFilled_1 == 54){
            newLine = start + end + getV54_1() + ";" + getV54_2() + ";"
                    + getV54_3() + ";" + getV54_4() + ";" + getV54_5()
                    +";" + getV54_6() + ";" + getV54_7() + ";" + getV54_8()
                    + ";" + getV54_9() + ";" + getV54_10() ;
        }
        else if(vignetteFilled_1 == 55){
            newLine = start + end + getV55_1() + ";" + getV55_2() + ";"
                    + getV55_3() + ";" + getV55_4() + ";" + getV55_5()
                    +";" + getV55_6() + ";" + getV55_7() + ";" + getV55_8()
                    + ";" + getV55_9() + ";" + getV55_10() ;
        }
        else if(vignetteFilled_1 == 56){
            newLine = start + end + getV56_1() + ";" + getV56_2() + ";"
                    + getV56_3() + ";" + getV56_4() + ";" + getV56_5()
                    +";" + getV56_6() + ";" + getV56_7() + ";" + getV56_8()
                    + ";" + getV56_9() + ";" + getV56_10() ;
        }
        else if(vignetteFilled_1 == 57){
            newLine = start + end + getV57_1() + ";" + getV57_2() + ";"
                    + getV57_3() + ";" + getV57_4() + ";" + getV57_5()
                    +";" + getV57_6() + ";" + getV57_7() + ";" + getV57_8()
                    + ";" + getV57_9() + ";" + getV57_10() ;
        }
        else if(vignetteFilled_1 == 58){
            newLine = start + end + getV58_1() + ";" + getV58_2() + ";"
                    + getV58_3() + ";" + getV58_4() + ";" + getV58_5()
                    +";" + getV58_6() + ";" + getV58_7() + ";" + getV58_8()
                    + ";" + getV58_9() + ";" + getV58_10() ;
        }
        else if(vignetteFilled_1 == 59){
            newLine = start + end + getV59_1() + ";" + getV59_2() + ";"
                    + getV59_3() + ";" + getV59_4() + ";" + getV59_5()
                    +";" + getV59_6() + ";" + getV59_7() + ";" + getV59_8()
                    + ";" + getV59_9() + ";" + getV59_10() ;
        }

        else if(vignetteFilled_1 == 60){
            newLine = start + end + getV60_1() + ";" + getV60_2() + ";"
                    + getV60_3() + ";" + getV60_4() + ";" + getV60_5()
                    +";" + getV60_6() + ";" + getV60_7() + ";" + getV60_8()
                    + ";" + getV60_9() + ";" + getV60_10() ;
        }
        else if(vignetteFilled_1 == 61){

            newLine = start + end + getV61_1() + ";" + getV61_2() + ";"
                    + getV61_3() + ";" + getV61_4() + ";" + getV61_5()
                    +";" + getV61_6() + ";" + getV61_7() + ";" + getV61_8()
                    + ";" + getV61_9() + ";" + getV61_10() ;

        }
        else if(vignetteFilled_1 == 62){
            newLine = start + end + getV62_1() + ";" + getV62_2() + ";"
                    + getV62_3() + ";" + getV62_4() + ";" + getV62_5()
                    +";" + getV62_6() + ";" + getV62_7() + ";" + getV62_8()
                    + ";" + getV62_9() + ";" + getV62_10() ;
        }
        else if(vignetteFilled_1 == 63){
            newLine = start + end + getV63_1() + ";" + getV63_2() + ";"
                    + getV63_3() + ";" + getV63_4() + ";" + getV63_5()
                    +";" + getV63_6() + ";" + getV63_7() + ";" + getV63_8()
                    + ";" + getV63_9() + ";" + getV63_10() ;
        }
        else if(vignetteFilled_1 == 64){
            newLine = start + end + getV64_1() + ";" + getV64_2() + ";"
                    + getV64_3() + ";" + getV64_4() + ";" + getV64_5()
                    +";" + getV64_6() + ";" + getV64_7() + ";" + getV64_8()
                    + ";" + getV64_9() + ";" + getV64_10() ;
        }

        else{
            throw new Error("No vignette one filled, impossible");
        }


        System.out.println("Resulting in new line : " + newLine );
        return newLine;

    }



    public String createNewFileLine_2(int clientNumber){

        String newLine = "";

        String end =  this.generateNewLineEnd();

        System.out.println("vignette 2 was filled with " + vignetteFilled_2);

        String start = this.generateNewLineStartVignette_2(clientNumber);


        if(this.vignetteFilled_2 == 1){

            newLine = start + end + getV1_1() + ";" + getV1_2() + ";"
                    + getV1_3() + ";" + getV1_4() + ";" + getV1_5()
                    +";" + getV1_6() + ";" + getV1_7() + ";" + getV1_8()
                    + ";" + getV1_9() + ";" + getV1_10() ;

        }
        else if(vignetteFilled_2 == 2){
            newLine = start + end + getV2_1() + ";" + getV2_2() + ";"
                    + getV2_3() + ";" + getV2_4() + ";" + getV2_5()
                    +";" + getV2_6() + ";" + getV2_7() + ";" + getV2_8()
                    + ";" + getV2_9() + ";" + getV2_10() ;
        }
        else if(vignetteFilled_2 == 3){
            newLine = start + end + getV3_1() + ";" + getV3_2() + ";"
                    + getV3_3() + ";" + getV3_4() + ";" + getV3_5()
                    +";" + getV3_6() + ";" + getV3_7() + ";" + getV3_8()
                    + ";" + getV3_9() + ";" + getV3_10() ;
        }
        else if(vignetteFilled_2 == 4){
            newLine = start + end + getV4_1() + ";" + getV4_2() + ";"
                    + getV4_3() + ";" + getV4_4() + ";" + getV4_5()
                    +";" + getV4_6() + ";" + getV4_7() + ";" + getV4_8()
                    + ";" + getV4_9() + ";" + getV4_10() ;
        }
        else if(vignetteFilled_2 == 5){
            newLine = start + end + getV5_1() + ";" + getV5_2() + ";"
                    + getV5_3() + ";" + getV5_4() + ";" + getV5_5()
                    +";" + getV5_6() + ";" + getV5_7() + ";" + getV5_8()
                    + ";" + getV5_9() + ";" + getV5_10() ;
        }
        else if(vignetteFilled_2 == 6){
            newLine = start + end + getV6_1() + ";" + getV6_2() + ";"
                    + getV6_3() + ";" + getV6_4() + ";" + getV6_5()
                    +";" + getV6_6() + ";" + getV6_7() + ";" + getV6_8()
                    + ";" + getV6_9() + ";" + getV6_10() ;
        }
        else if(vignetteFilled_2 == 7){
            newLine = start + end + getV7_1() + ";" + getV7_2() + ";"
                    + getV7_3() + ";" + getV7_4() + ";" + getV7_5()
                    +";" + getV7_6() + ";" + getV7_7() + ";" + getV7_8()
                    + ";" + getV7_9() + ";" + getV7_10() ;
        }
        else if(vignetteFilled_2 == 8){
            newLine = start + end + getV8_1() + ";" + getV8_2() + ";"
                    + getV8_3() + ";" + getV8_4() + ";" + getV8_5()
                    +";" + getV8_6() + ";" + getV8_7() + ";" + getV8_8()
                    + ";" + getV8_9() + ";" + getV8_10() ;
        }
        else if(vignetteFilled_2 == 9){
            newLine = start + end + getV9_1() + ";" + getV9_2() + ";"
                    + getV9_3() + ";" + getV9_4() + ";" + getV9_5()
                    +";" + getV9_6() + ";" + getV9_7() + ";" + getV9_8()
                    + ";" + getV9_9() + ";" + getV9_10() ;
        }
        else if(vignetteFilled_2 == 10){
            newLine = start + end + getV10_1() + ";" + getV10_2() + ";"
                    + getV10_3() + ";" + getV10_4() + ";" + getV10_5()
                    +";" + getV10_6() + ";" + getV10_7() + ";" + getV10_8()
                    + ";" + getV10_9() + ";" + getV10_10() ;
        }


        else if(vignetteFilled_2 == 11){

            newLine = start + end + getV11_1() + ";" + getV11_2() + ";"
                    + getV11_3() + ";" + getV11_4() + ";" + getV11_5()
                    +";" + getV11_6() + ";" + getV11_7() + ";" + getV11_8()
                    + ";" + getV11_9() + ";" + getV11_10() ;

        }
        else if(vignetteFilled_2 == 12){
            newLine = start + end + getV12_1() + ";" + getV12_2() + ";"
                    + getV12_3() + ";" + getV12_4() + ";" + getV12_5()
                    +";" + getV12_6() + ";" + getV12_7() + ";" + getV12_8()
                    + ";" + getV12_9() + ";" + getV12_10() ;
        }
        else if(vignetteFilled_2 == 13){
            newLine = start + end + getV13_1() + ";" + getV13_2() + ";"
                    + getV13_3() + ";" + getV13_4() + ";" + getV13_5()
                    +";" + getV13_6() + ";" + getV13_7() + ";" + getV13_8()
                    + ";" + getV13_9() + ";" + getV13_10() ;
        }
        else if(vignetteFilled_2 == 14){
            newLine = start + end + getV14_1() + ";" + getV14_2() + ";"
                    + getV14_3() + ";" + getV14_4() + ";" + getV14_5()
                    +";" + getV14_6() + ";" + getV14_7() + ";" + getV14_8()
                    + ";" + getV14_9() + ";" + getV14_10() ;
        }
        else if(vignetteFilled_2 == 15){
            newLine = start + end + getV15_1() + ";" + getV15_2() + ";"
                    + getV15_3() + ";" + getV15_4() + ";" + getV15_5()
                    +";" + getV15_6() + ";" + getV15_7() + ";" + getV15_8()
                    + ";" + getV15_9() + ";" + getV15_10() ;
        }
        else if(vignetteFilled_2 == 16){
            newLine = start + end + getV16_1() + ";" + getV16_2() + ";"
                    + getV16_3() + ";" + getV16_4() + ";" + getV16_5()
                    +";" + getV16_6() + ";" + getV16_7() + ";" + getV16_8()
                    + ";" + getV16_9() + ";" + getV16_10() ;
        }
        else if(vignetteFilled_2 == 17){
            newLine = start + end + getV17_1() + ";" + getV17_2() + ";"
                    + getV17_3() + ";" + getV17_4() + ";" + getV17_5()
                    +";" + getV17_6() + ";" + getV17_7() + ";" + getV17_8()
                    + ";" + getV17_9() + ";" + getV17_10() ;
        }
        else if(vignetteFilled_2 == 18){
            newLine = start + end + getV18_1() + ";" + getV18_2() + ";"
                    + getV18_3() + ";" + getV18_4() + ";" + getV18_5()
                    +";" + getV18_6() + ";" + getV18_7() + ";" + getV18_8()
                    + ";" + getV18_9() + ";" + getV18_10() ;
        }
        else if(vignetteFilled_2 == 19){
            newLine = start + end + getV19_1() + ";" + getV19_2() + ";"
                    + getV19_3() + ";" + getV19_4() + ";" + getV19_5()
                    +";" + getV19_6() + ";" + getV19_7() + ";" + getV19_8()
                    + ";" + getV19_9() + ";" + getV19_10() ;
        }



        else if(vignetteFilled_2 == 20){
            newLine = start + end + getV20_1() + ";" + getV20_2() + ";"
                    + getV20_3() + ";" + getV20_4() + ";" + getV20_5()
                    +";" + getV20_6() + ";" + getV20_7() + ";" + getV20_8()
                    + ";" + getV20_9() + ";" + getV20_10() ;
        }
        else if(vignetteFilled_2 == 21){

            newLine = start + end + getV21_1() + ";" + getV21_2() + ";"
                    + getV21_3() + ";" + getV21_4() + ";" + getV21_5()
                    +";" + getV21_6() + ";" + getV21_7() + ";" + getV21_8()
                    + ";" + getV21_9() + ";" + getV21_10() ;

        }
        else if(vignetteFilled_2 == 22){
            newLine = start + end + getV22_1() + ";" + getV22_2() + ";"
                    + getV22_3() + ";" + getV22_4() + ";" + getV22_5()
                    +";" + getV22_6() + ";" + getV22_7() + ";" + getV22_8()
                    + ";" + getV22_9() + ";" + getV22_10() ;
        }
        else if(vignetteFilled_2 == 23){
            newLine = start + end + getV23_1() + ";" + getV23_2() + ";"
                    + getV23_3() + ";" + getV23_4() + ";" + getV23_5()
                    +";" + getV23_6() + ";" + getV23_7() + ";" + getV23_8()
                    + ";" + getV23_9() + ";" + getV23_10() ;
        }
        else if(vignetteFilled_2 == 24){
            newLine = start + end + getV24_1() + ";" + getV24_2() + ";"
                    + getV24_3() + ";" + getV24_4() + ";" + getV24_5()
                    +";" + getV24_6() + ";" + getV24_7() + ";" + getV24_8()
                    + ";" + getV24_9() + ";" + getV24_10() ;
        }
        else if(vignetteFilled_2 == 25){
            newLine = start + end + getV25_1() + ";" + getV25_2() + ";"
                    + getV25_3() + ";" + getV25_4() + ";" + getV25_5()
                    +";" + getV25_6() + ";" + getV25_7() + ";" + getV25_8()
                    + ";" + getV25_9() + ";" + getV25_10() ;
        }
        else if(vignetteFilled_2 == 26){
            newLine = start + end + getV26_1() + ";" + getV26_2() + ";"
                    + getV26_3() + ";" + getV26_4() + ";" + getV26_5()
                    +";" + getV26_6() + ";" + getV26_7() + ";" + getV26_8()
                    + ";" + getV26_9() + ";" + getV26_10() ;
        }
        else if(vignetteFilled_2 == 27){
            newLine = start + end + getV27_1() + ";" + getV27_2() + ";"
                    + getV27_3() + ";" + getV27_4() + ";" + getV27_5()
                    +";" + getV27_6() + ";" + getV27_7() + ";" + getV27_8()
                    + ";" + getV27_9() + ";" + getV27_10() ;
        }
        else if(vignetteFilled_2 == 28){
            newLine = start + end + getV28_1() + ";" + getV28_2() + ";"
                    + getV28_3() + ";" + getV28_4() + ";" + getV28_5()
                    +";" + getV28_6() + ";" + getV28_7() + ";" + getV28_8()
                    + ";" + getV28_9() + ";" + getV28_10() ;
        }
        else if(vignetteFilled_2 == 29){
            newLine = start + end + getV29_1() + ";" + getV29_2() + ";"
                    + getV29_3() + ";" + getV29_4() + ";" + getV29_5()
                    +";" + getV29_6() + ";" + getV29_7() + ";" + getV29_8()
                    + ";" + getV29_9() + ";" + getV29_10() ;
        }


        else if(vignetteFilled_2 == 30){
            newLine = start + end + getV30_1() + ";" + getV30_2() + ";"
                    + getV30_3() + ";" + getV30_4() + ";" + getV30_5()
                    +";" + getV30_6() + ";" + getV30_7() + ";" + getV30_8()
                    + ";" + getV30_9() + ";" + getV30_10() ;
        }
        else if(vignetteFilled_2 == 31){

            newLine = start + end + getV31_1() + ";" + getV31_2() + ";"
                    + getV31_3() + ";" + getV31_4() + ";" + getV31_5()
                    +";" + getV31_6() + ";" + getV31_7() + ";" + getV31_8()
                    + ";" + getV31_9() + ";" + getV31_10() ;

        }
        else if(vignetteFilled_2 == 32){
            newLine = start + end + getV32_1() + ";" + getV32_2() + ";"
                    + getV32_3() + ";" + getV32_4() + ";" + getV32_5()
                    +";" + getV32_6() + ";" + getV32_7() + ";" + getV32_8()
                    + ";" + getV32_9() + ";" + getV32_10() ;
        }
        else if(vignetteFilled_2 == 33){
            newLine = start + end + getV33_1() + ";" + getV33_2() + ";"
                    + getV33_3() + ";" + getV33_4() + ";" + getV33_5()
                    +";" + getV33_6() + ";" + getV33_7() + ";" + getV33_8()
                    + ";" + getV33_9() + ";" + getV33_10() ;
        }
        else if(vignetteFilled_2 == 34){
            newLine = start + end + getV34_1() + ";" + getV34_2() + ";"
                    + getV34_3() + ";" + getV34_4() + ";" + getV34_5()
                    +";" + getV34_6() + ";" + getV34_7() + ";" + getV34_8()
                    + ";" + getV34_9() + ";" + getV34_10() ;
        }
        else if(vignetteFilled_2 == 35){
            newLine = start + end + getV35_1() + ";" + getV35_2() + ";"
                    + getV35_3() + ";" + getV35_4() + ";" + getV35_5()
                    +";" + getV35_6() + ";" + getV35_7() + ";" + getV35_8()
                    + ";" + getV35_9() + ";" + getV35_10() ;
        }
        else if(vignetteFilled_2 == 36){
            newLine = start + end + getV36_1() + ";" + getV36_2() + ";"
                    + getV36_3() + ";" + getV36_4() + ";" + getV36_5()
                    +";" + getV36_6() + ";" + getV36_7() + ";" + getV36_8()
                    + ";" + getV36_9() + ";" + getV36_10() ;
        }
        else if(vignetteFilled_2 == 37){
            newLine = start + end + getV37_1() + ";" + getV37_2() + ";"
                    + getV37_3() + ";" + getV37_4() + ";" + getV37_5()
                    +";" + getV37_6() + ";" + getV37_7() + ";" + getV37_8()
                    + ";" + getV37_9() + ";" + getV37_10() ;
        }
        else if(vignetteFilled_2 == 38){
            newLine = start + end + getV38_1() + ";" + getV38_2() + ";"
                    + getV38_3() + ";" + getV38_4() + ";" + getV38_5()
                    +";" + getV38_6() + ";" + getV38_7() + ";" + getV38_8()
                    + ";" + getV38_9() + ";" + getV38_10() ;
        }
        else if(vignetteFilled_2 == 39){
            newLine = start + end + getV39_1() + ";" + getV39_2() + ";"
                    + getV39_3() + ";" + getV39_4() + ";" + getV39_5()
                    +";" + getV39_6() + ";" + getV39_7() + ";" + getV39_8()
                    + ";" + getV39_9() + ";" + getV39_10() ;
        }

        else if(vignetteFilled_2 == 40){
            newLine = start + end + getV40_1() + ";" + getV40_2() + ";"
                    + getV40_3() + ";" + getV40_4() + ";" + getV40_5()
                    +";" + getV40_6() + ";" + getV40_7() + ";" + getV40_8()
                    + ";" + getV40_9() + ";" + getV40_10() ;
        }
        else if(vignetteFilled_2 == 41){

            newLine = start + end + getV41_1() + ";" + getV41_2() + ";"
                    + getV41_3() + ";" + getV41_4() + ";" + getV41_5()
                    +";" + getV41_6() + ";" + getV41_7() + ";" + getV41_8()
                    + ";" + getV41_9() + ";" + getV41_10() ;

        }
        else if(vignetteFilled_2 == 42){
            newLine = start + end + getV42_1() + ";" + getV42_2() + ";"
                    + getV42_3() + ";" + getV42_4() + ";" + getV42_5()
                    +";" + getV42_6() + ";" + getV42_7() + ";" + getV42_8()
                    + ";" + getV42_9() + ";" + getV42_10() ;
        }
        else if(vignetteFilled_2 == 43){
            newLine = start + end + getV43_1() + ";" + getV43_2() + ";"
                    + getV43_3() + ";" + getV43_4() + ";" + getV43_5()
                    +";" + getV43_6() + ";" + getV43_7() + ";" + getV43_8()
                    + ";" + getV43_9() + ";" + getV43_10() ;
        }
        else if(vignetteFilled_2 == 44){
            newLine = start + end + getV44_1() + ";" + getV44_2() + ";"
                    + getV44_3() + ";" + getV44_4() + ";" + getV44_5()
                    +";" + getV44_6() + ";" + getV44_7() + ";" + getV44_8()
                    + ";" + getV44_9() + ";" + getV44_10() ;
        }
        else if(vignetteFilled_2 == 45){
            newLine = start + end + getV45_1() + ";" + getV45_2() + ";"
                    + getV45_3() + ";" + getV45_4() + ";" + getV45_5()
                    +";" + getV45_6() + ";" + getV45_7() + ";" + getV45_8()
                    + ";" + getV45_9() + ";" + getV45_10() ;
        }
        else if(vignetteFilled_2 == 46){
            newLine = start + end + getV46_1() + ";" + getV46_2() + ";"
                    + getV46_3() + ";" + getV46_4() + ";" + getV46_5()
                    +";" + getV46_6() + ";" + getV46_7() + ";" + getV46_8()
                    + ";" + getV46_9() + ";" + getV46_10() ;
        }
        else if(vignetteFilled_2 == 47){
            newLine = start + end + getV47_1() + ";" + getV47_2() + ";"
                    + getV47_3() + ";" + getV47_4() + ";" + getV47_5()
                    +";" + getV47_6() + ";" + getV47_7() + ";" + getV47_8()
                    + ";" + getV47_9() + ";" + getV47_10() ;
        }
        else if(vignetteFilled_2 == 48){
            newLine = start + end + getV48_1() + ";" + getV48_2() + ";"
                    + getV48_3() + ";" + getV48_4() + ";" + getV48_5()
                    +";" + getV48_6() + ";" + getV48_7() + ";" + getV48_8()
                    + ";" + getV48_9() + ";" + getV48_10() ;
        }
        else if(vignetteFilled_2 == 49){
            newLine = start + end + getV49_1() + ";" + getV49_2() + ";"
                    + getV49_3() + ";" + getV49_4() + ";" + getV49_5()
                    +";" + getV49_6() + ";" + getV49_7() + ";" + getV49_8()
                    + ";" + getV49_9() + ";" + getV49_10() ;
        }


        else if(vignetteFilled_2 == 50){
            newLine = start + end + getV50_1() + ";" + getV50_2() + ";"
                    + getV50_3() + ";" + getV50_4() + ";" + getV50_5()
                    +";" + getV50_6() + ";" + getV50_7() + ";" + getV50_8()
                    + ";" + getV50_9() + ";" + getV50_10() ;
        }
        else if(vignetteFilled_2 == 51){

            newLine = start + end + getV51_1() + ";" + getV51_2() + ";"
                    + getV51_3() + ";" + getV51_4() + ";" + getV51_5()
                    +";" + getV51_6() + ";" + getV51_7() + ";" + getV51_8()
                    + ";" + getV51_9() + ";" + getV51_10() ;

        }
        else if(vignetteFilled_2 == 52){
            newLine = start + end + getV52_1() + ";" + getV52_2() + ";"
                    + getV52_3() + ";" + getV52_4() + ";" + getV52_5()
                    +";" + getV52_6() + ";" + getV52_7() + ";" + getV52_8()
                    + ";" + getV52_9() + ";" + getV52_10() ;
        }
        else if(vignetteFilled_2 == 53){
            newLine = start + end + getV53_1() + ";" + getV53_2() + ";"
                    + getV53_3() + ";" + getV53_4() + ";" + getV53_5()
                    +";" + getV53_6() + ";" + getV53_7() + ";" + getV53_8()
                    + ";" + getV53_9() + ";" + getV53_10() ;
        }
        else if(vignetteFilled_2 == 54){
            newLine = start + end + getV54_1() + ";" + getV54_2() + ";"
                    + getV54_3() + ";" + getV54_4() + ";" + getV54_5()
                    +";" + getV54_6() + ";" + getV54_7() + ";" + getV54_8()
                    + ";" + getV54_9() + ";" + getV54_10() ;
        }
        else if(vignetteFilled_2 == 55){
            newLine = start + end + getV55_1() + ";" + getV55_2() + ";"
                    + getV55_3() + ";" + getV55_4() + ";" + getV55_5()
                    +";" + getV55_6() + ";" + getV55_7() + ";" + getV55_8()
                    + ";" + getV55_9() + ";" + getV55_10() ;
        }
        else if(vignetteFilled_2 == 56){
            newLine = start + end + getV56_1() + ";" + getV56_2() + ";"
                    + getV56_3() + ";" + getV56_4() + ";" + getV56_5()
                    +";" + getV56_6() + ";" + getV56_7() + ";" + getV56_8()
                    + ";" + getV56_9() + ";" + getV56_10() ;
        }
        else if(vignetteFilled_2 == 57){
            newLine = start + end + getV57_1() + ";" + getV57_2() + ";"
                    + getV57_3() + ";" + getV57_4() + ";" + getV57_5()
                    +";" + getV57_6() + ";" + getV57_7() + ";" + getV57_8()
                    + ";" + getV57_9() + ";" + getV57_10() ;
        }
        else if(vignetteFilled_2 == 58){
            newLine = start + end + getV58_1() + ";" + getV58_2() + ";"
                    + getV58_3() + ";" + getV58_4() + ";" + getV58_5()
                    +";" + getV58_6() + ";" + getV58_7() + ";" + getV58_8()
                    + ";" + getV58_9() + ";" + getV58_10() ;
        }
        else if(vignetteFilled_2 == 59){
            newLine = start + end + getV59_1() + ";" + getV59_2() + ";"
                    + getV59_3() + ";" + getV59_4() + ";" + getV59_5()
                    +";" + getV59_6() + ";" + getV59_7() + ";" + getV59_8()
                    + ";" + getV59_9() + ";" + getV59_10() ;
        }

        else if(vignetteFilled_2 == 60){
            newLine = start + end + getV60_1() + ";" + getV60_2() + ";"
                    + getV60_3() + ";" + getV60_4() + ";" + getV60_5()
                    +";" + getV60_6() + ";" + getV60_7() + ";" + getV60_8()
                    + ";" + getV60_9() + ";" + getV60_10() ;
        }
        else if(vignetteFilled_2 == 61){

            newLine = start + end + getV61_1() + ";" + getV61_2() + ";"
                    + getV61_3() + ";" + getV61_4() + ";" + getV61_5()
                    +";" + getV61_6() + ";" + getV61_7() + ";" + getV61_8()
                    + ";" + getV61_9() + ";" + getV61_10() ;

        }
        else if(vignetteFilled_2 == 62){
            newLine = start + end + getV62_1() + ";" + getV62_2() + ";"
                    + getV62_3() + ";" + getV62_4() + ";" + getV62_5()
                    +";" + getV62_6() + ";" + getV62_7() + ";" + getV62_8()
                    + ";" + getV62_9() + ";" + getV62_10() ;
        }
        else if(vignetteFilled_2 == 63){
            newLine = start + end + getV63_1() + ";" + getV63_2() + ";"
                    + getV63_3() + ";" + getV63_4() + ";" + getV63_5()
                    +";" + getV63_6() + ";" + getV63_7() + ";" + getV63_8()
                    + ";" + getV63_9() + ";" + getV63_10() ;
        }
        else if(vignetteFilled_2 == 64){
            newLine = start + end + getV64_1() + ";" + getV64_2() + ";"
                    + getV64_3() + ";" + getV64_4() + ";" + getV64_5()
                    +";" + getV64_6() + ";" + getV64_7() + ";" + getV64_8()
                    + ";" + getV64_9() + ";" + getV64_10() ;
        }

        else{
            throw new Error("No vignette one filled, impossible");
        }


        System.out.println("Resulting in new line : " + newLine );
        return newLine;

    }


    public String generatePredictors(int vignetteNumber){

        String predictors = "";

        if(vignetteNumber == 1){

            predictors = "1;1;1;1;";

        }
        if(vignetteNumber == 2){

            predictors = "1;1;1;2;";

        }
        if(vignetteNumber == 3){

            predictors = "1;1;1;3;";

        }
        if(vignetteNumber == 4){

            predictors = "1;1;1;4;";

        }
        if(vignetteNumber == 5){

            predictors = "1;0;1;1;";

        }
        if(vignetteNumber == 6){

            predictors = "1;0;1;2;";

        }
        if(vignetteNumber == 7){

            predictors = "1;0;1;3;";

        }
        if(vignetteNumber == 8){

            predictors = "1;0;1;4;";

        }
        if(vignetteNumber == 9){

            predictors = "1;1;2;1;";

        }
        if(vignetteNumber == 10){

            predictors = "1;1;2;2;";

        }
        if(vignetteNumber == 11){

            predictors = "1;1;2;3;";

        }
        if(vignetteNumber == 12){

            predictors = "1;1;2;4;";

        }
        if(vignetteNumber == 13){

            predictors = "1;0;2;1;";

        }
        if(vignetteNumber == 14){

            predictors = "1;0;2;2;";

        }
        if(vignetteNumber == 15){

            predictors = "1;0;2;3;";

        }
        if(vignetteNumber == 16){

            predictors = "1;0;2;4;";

        }

        if(vignetteNumber == 17){

            predictors = "1;1;3;1;";

        }
        if(vignetteNumber == 18){

            predictors = "1;1;3;2;";

        }

        if(vignetteNumber == 19){

            predictors = "1;1;3;3;";

        }

        if(vignetteNumber == 20){

            predictors = "1;1;3;4;";

        }

        if(vignetteNumber == 21){

            predictors = "1;0;3;1;";

        }

        if(vignetteNumber == 22){

            predictors = "1;0;3;2;";

        }

        if(vignetteNumber == 23){

            predictors = "1;0;3;3;";

        }

        if(vignetteNumber == 24){

            predictors = "1;0;3;4;";

        }

        if(vignetteNumber == 25){

            predictors = "1;1;4;1;";

        }

        if(vignetteNumber == 26){

            predictors = "1;1;4;2;";

        }

        if(vignetteNumber == 27){

            predictors = "1;1;4;3;";

        }

        if(vignetteNumber == 28){

            predictors = "1;1;4;4;";

        }

        if(vignetteNumber == 29){

            predictors = "1;0;4;1;";

        }

        if(vignetteNumber == 30){

            predictors = "1;0;4;2;";

        }

        if(vignetteNumber == 31){

            predictors = "1;0;4;3;";

        }

        if(vignetteNumber == 32){

            predictors = "1;0;4;4;";

        }


        if(vignetteNumber == 33){

            predictors = "0;1;1;1;";

        }

        if(vignetteNumber == 34){

            predictors = "0;1;1;2;";

        }

        if(vignetteNumber == 35){

            predictors = "0;1;1;3;";

        }

        if(vignetteNumber == 36){

            predictors = "0;1;1;4;";

        }


        if(vignetteNumber == 37){

            predictors = "0;0;1;1;";

        }

        if(vignetteNumber == 38){

            predictors = "0;0;1;2;";

        }

        if(vignetteNumber == 39){

            predictors = "0;0;1;3;";

        }

        if(vignetteNumber == 40){

            predictors = "0;0;1;4;";

        }

        if(vignetteNumber == 41){

            predictors = "0;1;2;1;";

        }

        if(vignetteNumber == 42){

            predictors = "0;1;2;2;";

        }

        if(vignetteNumber == 43){

            predictors = "0;1;2;3;";

        }

        if(vignetteNumber == 44){

            predictors = "0;1;2;4;";

        }

        if(vignetteNumber == 45){

            predictors = "0;0;2;1;";

        }

        if(vignetteNumber == 46){

            predictors = "0;0;2;2;";

        }

        if(vignetteNumber == 47){

            predictors = "0;0;2;3;";

        }

        if(vignetteNumber == 48){

            predictors = "0;0;2;4;";

        }


        if(vignetteNumber == 49){

            predictors = "0;1;3;1;";

        }

        if(vignetteNumber == 50){

            predictors = "0;1;3;2;";

        }

        if(vignetteNumber == 51){

            predictors = "0;1;3;3;";

        }

        if(vignetteNumber == 52){

            predictors = "0;1;3;4;";

        }

        if(vignetteNumber == 53){

            predictors = "0;0;3;1;";

        }

        if(vignetteNumber == 54){

            predictors = "0;0;3;2;";

        }

        if(vignetteNumber == 55){

            predictors = "0;0;3;3;";

        }

        if(vignetteNumber == 56){

            predictors = "0;0;3;4;";

        }

        if(vignetteNumber == 57){

            predictors = "0;1;4;1;";

        }

        if(vignetteNumber == 58){

            predictors = "0;1;4;2;";

        }

        if(vignetteNumber == 59){

            predictors = "0;1;4;3;";

        }

        if(vignetteNumber == 60){

            predictors = "0;1;4;4;";

        }

        if(vignetteNumber == 61){

            predictors = "0;0;4;1;";

        }

        if(vignetteNumber == 62){

            predictors = "0;0;4;2;";

        }

        if(vignetteNumber == 63){

            predictors = "0;0;4;3;";

        }
        if(vignetteNumber == 64){

            predictors = "0;0;4;4;";

        }




        return predictors;


    }


    public String generateNewLineStartVignette_1(int clientNumber){

        String predictors = generatePredictors(this.vignetteFilled_1);

        String start = followNumber + ";" + clientNumber + ";" + this.vignetteFilled_1 + ";" + predictors+ this.getSEX() + ";" + this.getAGE() + ";" + this.getTENORG() + ";" + this.getTENLG() +";" + this.getFUNCTIE() + ";" + this.getDIPL() + ";";
        followNumber++;
        return start;
    }

    public String generateNewLineStartVignette_2(int clientNumber){

        String predictors = generatePredictors(this.vignetteFilled_2);

        String start = followNumber + ";" + clientNumber + ";" + this.vignetteFilled_2 + ";" + predictors+ this.getSEX() + ";" + this.getAGE() + ";" + this.getTENORG() + ";" + this.getTENLG() +";" + this.getFUNCTIE() + ";" + this.getDIPL() + ";";
        followNumber++;
        return start;
    }

    public String generateNewLineStartVignette_3(int clientNumber){

        String predictors = generatePredictors(this.vignetteFilled_3);

        String start = followNumber + ";" + clientNumber + ";" + this.vignetteFilled_3 + ";" + predictors+ this.getSEX() + ";" + this.getAGE() + ";" + this.getTENORG() + ";" + this.getTENLG() +";" + this.getFUNCTIE() + ";" + this.getDIPL() + ";";
        followNumber++;
        return start;
    }

    public String generateNewLineStartVignette_4(int clientNumber){

        String predictors = generatePredictors(this.vignetteFilled_4);

        String start = followNumber + ";" + clientNumber + ";" + this.vignetteFilled_4 + ";" + predictors+ this.getSEX() + ";" + this.getAGE() + ";" + this.getTENORG() + ";" + this.getTENLG() +";" + this.getFUNCTIE() + ";" + this.getDIPL() + ";";
        followNumber++;
        return start;
    }

    public String generateNewLineStartVignette_5(int clientNumber){

        String predictors = generatePredictors(this.vignetteFilled_5);

        String start = followNumber + ";" + clientNumber + ";" + this.vignetteFilled_5 + ";" + predictors+ this.getSEX() + ";" + this.getAGE() + ";" + this.getTENORG() + ";" + this.getTENLG() +";" + this.getFUNCTIE() + ";" + this.getDIPL() + ";";
        followNumber++;
        return start;
    }

    public String generateNewLineStartVignette_6(int clientNumber){

        String predictors = generatePredictors(this.vignetteFilled_6);

        String start = followNumber + ";" + clientNumber + ";" + this.vignetteFilled_6 + ";" + predictors+ this.getSEX() + ";" + this.getAGE() + ";" + this.getTENORG() + ";" + this.getTENLG() +";" + this.getFUNCTIE() + ";" + this.getDIPL() + ";";
        followNumber++;
        return start;
    }


    public String generateNewLineEnd(){

        String end = this.getMEDL() + ";" + this.getLMX_1() + ";"
                + this.getLMX_2() + ";" +this.getLMX_3() + ";" + this.getLMX_4() + ";"
                + this.getLMX_5() + ";" + this.getLMX_6()  + ";"+ this.getLMX_7() + ";"
                + this.getMANCHECK_1() + ";" + this.getMANCHECK_2()  + ";" + this.getMANCHECK_3() + ";" +
                this.getMANCHECK_4() + ";" + this.getMANCHECK_5() + ";" + this.getMANCHECK_6() + ";"
                + this.getMANCHECK_7() + ";" + this.getMANCHECK_8() + ";";

        return end;
    }


    public String createNewFileLine_3(int clientNumber){

        String newLine = "";

        String end =  this.generateNewLineEnd();

        System.out.println("vignette 3 was filled with " + vignetteFilled_3);

        String start = this.generateNewLineStartVignette_3(clientNumber);


        if(this.vignetteFilled_3 == 1){

            newLine = start + end + getV1_1() + ";" + getV1_2() + ";"
                    + getV1_3() + ";" + getV1_4() + ";" + getV1_5()
                    +";" + getV1_6() + ";" + getV1_7() + ";" + getV1_8()
                    + ";" + getV1_9() + ";" + getV1_10() ;

        }
        else if(vignetteFilled_3 == 2){
            newLine = start + end + getV2_1() + ";" + getV2_2() + ";"
                    + getV2_3() + ";" + getV2_4() + ";" + getV2_5()
                    +";" + getV2_6() + ";" + getV2_7() + ";" + getV2_8()
                    + ";" + getV2_9() + ";" + getV2_10() ;
        }
        else if(vignetteFilled_3 == 3){
            newLine = start + end + getV3_1() + ";" + getV3_2() + ";"
                    + getV3_3() + ";" + getV3_4() + ";" + getV3_5()
                    +";" + getV3_6() + ";" + getV3_7() + ";" + getV3_8()
                    + ";" + getV3_9() + ";" + getV3_10() ;
        }
        else if(vignetteFilled_3 == 4){
            newLine = start + end + getV4_1() + ";" + getV4_2() + ";"
                    + getV4_3() + ";" + getV4_4() + ";" + getV4_5()
                    +";" + getV4_6() + ";" + getV4_7() + ";" + getV4_8()
                    + ";" + getV4_9() + ";" + getV4_10() ;
        }
        else if(vignetteFilled_3 == 5){
            newLine = start + end + getV5_1() + ";" + getV5_2() + ";"
                    + getV5_3() + ";" + getV5_4() + ";" + getV5_5()
                    +";" + getV5_6() + ";" + getV5_7() + ";" + getV5_8()
                    + ";" + getV5_9() + ";" + getV5_10() ;
        }
        else if(vignetteFilled_3 == 6){
            newLine = start + end + getV6_1() + ";" + getV6_2() + ";"
                    + getV6_3() + ";" + getV6_4() + ";" + getV6_5()
                    +";" + getV6_6() + ";" + getV6_7() + ";" + getV6_8()
                    + ";" + getV6_9() + ";" + getV6_10() ;
        }
        else if(vignetteFilled_3 == 7){
            newLine = start + end + getV7_1() + ";" + getV7_2() + ";"
                    + getV7_3() + ";" + getV7_4() + ";" + getV7_5()
                    +";" + getV7_6() + ";" + getV7_7() + ";" + getV7_8()
                    + ";" + getV7_9() + ";" + getV7_10() ;
        }
        else if(vignetteFilled_3 == 8){
            newLine = start + end + getV8_1() + ";" + getV8_2() + ";"
                    + getV8_3() + ";" + getV8_4() + ";" + getV8_5()
                    +";" + getV8_6() + ";" + getV8_7() + ";" + getV8_8()
                    + ";" + getV8_9() + ";" + getV8_10() ;
        }
        else if(vignetteFilled_3 == 9){
            newLine = start + end + getV9_1() + ";" + getV9_2() + ";"
                    + getV9_3() + ";" + getV9_4() + ";" + getV9_5()
                    +";" + getV9_6() + ";" + getV9_7() + ";" + getV9_8()
                    + ";" + getV9_9() + ";" + getV9_10() ;
        }
        else if(vignetteFilled_3 == 10){
            newLine = start + end + getV10_1() + ";" + getV10_2() + ";"
                    + getV10_3() + ";" + getV10_4() + ";" + getV10_5()
                    +";" + getV10_6() + ";" + getV10_7() + ";" + getV10_8()
                    + ";" + getV10_9() + ";" + getV10_10() ;
        }


        else if(vignetteFilled_3 == 11){

            newLine = start + end + getV11_1() + ";" + getV11_2() + ";"
                    + getV11_3() + ";" + getV11_4() + ";" + getV11_5()
                    +";" + getV11_6() + ";" + getV11_7() + ";" + getV11_8()
                    + ";" + getV11_9() + ";" + getV11_10() ;

        }
        else if(vignetteFilled_3 == 12){
            newLine = start + end + getV12_1() + ";" + getV12_2() + ";"
                    + getV12_3() + ";" + getV12_4() + ";" + getV12_5()
                    +";" + getV12_6() + ";" + getV12_7() + ";" + getV12_8()
                    + ";" + getV12_9() + ";" + getV12_10() ;
        }
        else if(vignetteFilled_3 == 13){
            newLine = start + end + getV13_1() + ";" + getV13_2() + ";"
                    + getV13_3() + ";" + getV13_4() + ";" + getV13_5()
                    +";" + getV13_6() + ";" + getV13_7() + ";" + getV13_8()
                    + ";" + getV13_9() + ";" + getV13_10() ;
        }
        else if(vignetteFilled_3 == 14){
            newLine = start + end + getV14_1() + ";" + getV14_2() + ";"
                    + getV14_3() + ";" + getV14_4() + ";" + getV14_5()
                    +";" + getV14_6() + ";" + getV14_7() + ";" + getV14_8()
                    + ";" + getV14_9() + ";" + getV14_10() ;
        }
        else if(vignetteFilled_3 == 15){
            newLine = start + end + getV15_1() + ";" + getV15_2() + ";"
                    + getV15_3() + ";" + getV15_4() + ";" + getV15_5()
                    +";" + getV15_6() + ";" + getV15_7() + ";" + getV15_8()
                    + ";" + getV15_9() + ";" + getV15_10() ;
        }
        else if(vignetteFilled_3 == 16){
            newLine = start + end + getV16_1() + ";" + getV16_2() + ";"
                    + getV16_3() + ";" + getV16_4() + ";" + getV16_5()
                    +";" + getV16_6() + ";" + getV16_7() + ";" + getV16_8()
                    + ";" + getV16_9() + ";" + getV16_10() ;
        }
        else if(vignetteFilled_3 == 17){
            newLine = start + end + getV17_1() + ";" + getV17_2() + ";"
                    + getV17_3() + ";" + getV17_4() + ";" + getV17_5()
                    +";" + getV17_6() + ";" + getV17_7() + ";" + getV17_8()
                    + ";" + getV17_9() + ";" + getV17_10() ;
        }
        else if(vignetteFilled_3 == 18){
            newLine = start + end + getV18_1() + ";" + getV18_2() + ";"
                    + getV18_3() + ";" + getV18_4() + ";" + getV18_5()
                    +";" + getV18_6() + ";" + getV18_7() + ";" + getV18_8()
                    + ";" + getV18_9() + ";" + getV18_10() ;
        }
        else if(vignetteFilled_3 == 19){
            newLine = start + end + getV19_1() + ";" + getV19_2() + ";"
                    + getV19_3() + ";" + getV19_4() + ";" + getV19_5()
                    +";" + getV19_6() + ";" + getV19_7() + ";" + getV19_8()
                    + ";" + getV19_9() + ";" + getV19_10() ;
        }



        else if(vignetteFilled_3 == 20){
            newLine = start + end + getV20_1() + ";" + getV20_2() + ";"
                    + getV20_3() + ";" + getV20_4() + ";" + getV20_5()
                    +";" + getV20_6() + ";" + getV20_7() + ";" + getV20_8()
                    + ";" + getV20_9() + ";" + getV20_10() ;
        }
        else if(vignetteFilled_3 == 21){

            newLine = start + end + getV21_1() + ";" + getV21_2() + ";"
                    + getV21_3() + ";" + getV21_4() + ";" + getV21_5()
                    +";" + getV21_6() + ";" + getV21_7() + ";" + getV21_8()
                    + ";" + getV21_9() + ";" + getV21_10() ;

        }
        else if(vignetteFilled_3 == 22){
            newLine = start + end + getV22_1() + ";" + getV22_2() + ";"
                    + getV22_3() + ";" + getV22_4() + ";" + getV22_5()
                    +";" + getV22_6() + ";" + getV22_7() + ";" + getV22_8()
                    + ";" + getV22_9() + ";" + getV22_10() ;
        }
        else if(vignetteFilled_3 == 23){
            newLine = start + end + getV23_1() + ";" + getV23_2() + ";"
                    + getV23_3() + ";" + getV23_4() + ";" + getV23_5()
                    +";" + getV23_6() + ";" + getV23_7() + ";" + getV23_8()
                    + ";" + getV23_9() + ";" + getV23_10() ;
        }
        else if(vignetteFilled_3 == 24){
            newLine = start + end + getV24_1() + ";" + getV24_2() + ";"
                    + getV24_3() + ";" + getV24_4() + ";" + getV24_5()
                    +";" + getV24_6() + ";" + getV24_7() + ";" + getV24_8()
                    + ";" + getV24_9() + ";" + getV24_10() ;
        }
        else if(vignetteFilled_3 == 25){
            newLine = start + end + getV25_1() + ";" + getV25_2() + ";"
                    + getV25_3() + ";" + getV25_4() + ";" + getV25_5()
                    +";" + getV25_6() + ";" + getV25_7() + ";" + getV25_8()
                    + ";" + getV25_9() + ";" + getV25_10() ;
        }
        else if(vignetteFilled_3 == 26){
            newLine = start + end + getV26_1() + ";" + getV26_2() + ";"
                    + getV26_3() + ";" + getV26_4() + ";" + getV26_5()
                    +";" + getV26_6() + ";" + getV26_7() + ";" + getV26_8()
                    + ";" + getV26_9() + ";" + getV26_10() ;
        }
        else if(vignetteFilled_3 == 27){
            newLine = start + end + getV27_1() + ";" + getV27_2() + ";"
                    + getV27_3() + ";" + getV27_4() + ";" + getV27_5()
                    +";" + getV27_6() + ";" + getV27_7() + ";" + getV27_8()
                    + ";" + getV27_9() + ";" + getV27_10() ;
        }
        else if(vignetteFilled_3 == 28){
            newLine = start + end + getV28_1() + ";" + getV28_2() + ";"
                    + getV28_3() + ";" + getV28_4() + ";" + getV28_5()
                    +";" + getV28_6() + ";" + getV28_7() + ";" + getV28_8()
                    + ";" + getV28_9() + ";" + getV28_10() ;
        }
        else if(vignetteFilled_3 == 29){
            newLine = start + end + getV29_1() + ";" + getV29_2() + ";"
                    + getV29_3() + ";" + getV29_4() + ";" + getV29_5()
                    +";" + getV29_6() + ";" + getV29_7() + ";" + getV29_8()
                    + ";" + getV29_9() + ";" + getV29_10() ;
        }


        else if(vignetteFilled_3 == 30){
            newLine = start + end + getV30_1() + ";" + getV30_2() + ";"
                    + getV30_3() + ";" + getV30_4() + ";" + getV30_5()
                    +";" + getV30_6() + ";" + getV30_7() + ";" + getV30_8()
                    + ";" + getV30_9() + ";" + getV30_10() ;
        }
        else if(vignetteFilled_3 == 31){

            newLine = start + end + getV31_1() + ";" + getV31_2() + ";"
                    + getV31_3() + ";" + getV31_4() + ";" + getV31_5()
                    +";" + getV31_6() + ";" + getV31_7() + ";" + getV31_8()
                    + ";" + getV31_9() + ";" + getV31_10() ;

        }
        else if(vignetteFilled_3 == 32){
            newLine = start + end + getV32_1() + ";" + getV32_2() + ";"
                    + getV32_3() + ";" + getV32_4() + ";" + getV32_5()
                    +";" + getV32_6() + ";" + getV32_7() + ";" + getV32_8()
                    + ";" + getV32_9() + ";" + getV32_10() ;
        }
        else if(vignetteFilled_3 == 33){
            newLine = start + end + getV33_1() + ";" + getV33_2() + ";"
                    + getV33_3() + ";" + getV33_4() + ";" + getV33_5()
                    +";" + getV33_6() + ";" + getV33_7() + ";" + getV33_8()
                    + ";" + getV33_9() + ";" + getV33_10() ;
        }
        else if(vignetteFilled_3 == 34){
            newLine = start + end + getV34_1() + ";" + getV34_2() + ";"
                    + getV34_3() + ";" + getV34_4() + ";" + getV34_5()
                    +";" + getV34_6() + ";" + getV34_7() + ";" + getV34_8()
                    + ";" + getV34_9() + ";" + getV34_10() ;
        }
        else if(vignetteFilled_3 == 35){
            newLine = start + end + getV35_1() + ";" + getV35_2() + ";"
                    + getV35_3() + ";" + getV35_4() + ";" + getV35_5()
                    +";" + getV35_6() + ";" + getV35_7() + ";" + getV35_8()
                    + ";" + getV35_9() + ";" + getV35_10() ;
        }
        else if(vignetteFilled_3 == 36){
            newLine = start + end + getV36_1() + ";" + getV36_2() + ";"
                    + getV36_3() + ";" + getV36_4() + ";" + getV36_5()
                    +";" + getV36_6() + ";" + getV36_7() + ";" + getV36_8()
                    + ";" + getV36_9() + ";" + getV36_10() ;
        }
        else if(vignetteFilled_3 == 37){
            newLine = start + end + getV37_1() + ";" + getV37_2() + ";"
                    + getV37_3() + ";" + getV37_4() + ";" + getV37_5()
                    +";" + getV37_6() + ";" + getV37_7() + ";" + getV37_8()
                    + ";" + getV37_9() + ";" + getV37_10() ;
        }
        else if(vignetteFilled_3 == 38){
            newLine = start + end + getV38_1() + ";" + getV38_2() + ";"
                    + getV38_3() + ";" + getV38_4() + ";" + getV38_5()
                    +";" + getV38_6() + ";" + getV38_7() + ";" + getV38_8()
                    + ";" + getV38_9() + ";" + getV38_10() ;
        }
        else if(vignetteFilled_3 == 39){
            newLine = start + end + getV39_1() + ";" + getV39_2() + ";"
                    + getV39_3() + ";" + getV39_4() + ";" + getV39_5()
                    +";" + getV39_6() + ";" + getV39_7() + ";" + getV39_8()
                    + ";" + getV39_9() + ";" + getV39_10() ;
        }

        else if(vignetteFilled_3 == 40){
            newLine = start + end + getV40_1() + ";" + getV40_2() + ";"
                    + getV40_3() + ";" + getV40_4() + ";" + getV40_5()
                    +";" + getV40_6() + ";" + getV40_7() + ";" + getV40_8()
                    + ";" + getV40_9() + ";" + getV40_10() ;
        }
        else if(vignetteFilled_3 == 41){

            newLine = start + end + getV41_1() + ";" + getV41_2() + ";"
                    + getV41_3() + ";" + getV41_4() + ";" + getV41_5()
                    +";" + getV41_6() + ";" + getV41_7() + ";" + getV41_8()
                    + ";" + getV41_9() + ";" + getV41_10() ;

        }
        else if(vignetteFilled_3 == 42){
            newLine = start + end + getV42_1() + ";" + getV42_2() + ";"
                    + getV42_3() + ";" + getV42_4() + ";" + getV42_5()
                    +";" + getV42_6() + ";" + getV42_7() + ";" + getV42_8()
                    + ";" + getV42_9() + ";" + getV42_10() ;
        }
        else if(vignetteFilled_3 == 43){
            newLine = start + end + getV43_1() + ";" + getV43_2() + ";"
                    + getV43_3() + ";" + getV43_4() + ";" + getV43_5()
                    +";" + getV43_6() + ";" + getV43_7() + ";" + getV43_8()
                    + ";" + getV43_9() + ";" + getV43_10() ;
        }
        else if(vignetteFilled_3 == 44){
            newLine = start + end + getV44_1() + ";" + getV44_2() + ";"
                    + getV44_3() + ";" + getV44_4() + ";" + getV44_5()
                    +";" + getV44_6() + ";" + getV44_7() + ";" + getV44_8()
                    + ";" + getV44_9() + ";" + getV44_10() ;
        }
        else if(vignetteFilled_3 == 45){
            newLine = start + end + getV45_1() + ";" + getV45_2() + ";"
                    + getV45_3() + ";" + getV45_4() + ";" + getV45_5()
                    +";" + getV45_6() + ";" + getV45_7() + ";" + getV45_8()
                    + ";" + getV45_9() + ";" + getV45_10() ;
        }
        else if(vignetteFilled_3 == 46){
            newLine = start + end + getV46_1() + ";" + getV46_2() + ";"
                    + getV46_3() + ";" + getV46_4() + ";" + getV46_5()
                    +";" + getV46_6() + ";" + getV46_7() + ";" + getV46_8()
                    + ";" + getV46_9() + ";" + getV46_10() ;
        }
        else if(vignetteFilled_3 == 47){
            newLine = start + end + getV47_1() + ";" + getV47_2() + ";"
                    + getV47_3() + ";" + getV47_4() + ";" + getV47_5()
                    +";" + getV47_6() + ";" + getV47_7() + ";" + getV47_8()
                    + ";" + getV47_9() + ";" + getV47_10() ;
        }
        else if(vignetteFilled_3 == 48){
            newLine = start + end + getV48_1() + ";" + getV48_2() + ";"
                    + getV48_3() + ";" + getV48_4() + ";" + getV48_5()
                    +";" + getV48_6() + ";" + getV48_7() + ";" + getV48_8()
                    + ";" + getV48_9() + ";" + getV48_10() ;
        }
        else if(vignetteFilled_3 == 49){
            newLine = start + end + getV49_1() + ";" + getV49_2() + ";"
                    + getV49_3() + ";" + getV49_4() + ";" + getV49_5()
                    +";" + getV49_6() + ";" + getV49_7() + ";" + getV49_8()
                    + ";" + getV49_9() + ";" + getV49_10() ;
        }


        else if(vignetteFilled_3 == 50){
            newLine = start + end + getV50_1() + ";" + getV50_2() + ";"
                    + getV50_3() + ";" + getV50_4() + ";" + getV50_5()
                    +";" + getV50_6() + ";" + getV50_7() + ";" + getV50_8()
                    + ";" + getV50_9() + ";" + getV50_10() ;
        }
        else if(vignetteFilled_3 == 51){

            newLine = start + end + getV51_1() + ";" + getV51_2() + ";"
                    + getV51_3() + ";" + getV51_4() + ";" + getV51_5()
                    +";" + getV51_6() + ";" + getV51_7() + ";" + getV51_8()
                    + ";" + getV51_9() + ";" + getV51_10() ;

        }
        else if(vignetteFilled_3 == 52){
            newLine = start + end + getV52_1() + ";" + getV52_2() + ";"
                    + getV52_3() + ";" + getV52_4() + ";" + getV52_5()
                    +";" + getV52_6() + ";" + getV52_7() + ";" + getV52_8()
                    + ";" + getV52_9() + ";" + getV52_10() ;
        }
        else if(vignetteFilled_3 == 53){
            newLine = start + end + getV53_1() + ";" + getV53_2() + ";"
                    + getV53_3() + ";" + getV53_4() + ";" + getV53_5()
                    +";" + getV53_6() + ";" + getV53_7() + ";" + getV53_8()
                    + ";" + getV53_9() + ";" + getV53_10() ;
        }
        else if(vignetteFilled_3 == 54){
            newLine = start + end + getV54_1() + ";" + getV54_2() + ";"
                    + getV54_3() + ";" + getV54_4() + ";" + getV54_5()
                    +";" + getV54_6() + ";" + getV54_7() + ";" + getV54_8()
                    + ";" + getV54_9() + ";" + getV54_10() ;
        }
        else if(vignetteFilled_3 == 55){
            newLine = start + end + getV55_1() + ";" + getV55_2() + ";"
                    + getV55_3() + ";" + getV55_4() + ";" + getV55_5()
                    +";" + getV55_6() + ";" + getV55_7() + ";" + getV55_8()
                    + ";" + getV55_9() + ";" + getV55_10() ;
        }
        else if(vignetteFilled_3 == 56){
            newLine = start + end + getV56_1() + ";" + getV56_2() + ";"
                    + getV56_3() + ";" + getV56_4() + ";" + getV56_5()
                    +";" + getV56_6() + ";" + getV56_7() + ";" + getV56_8()
                    + ";" + getV56_9() + ";" + getV56_10() ;
        }
        else if(vignetteFilled_3 == 57){
            newLine = start + end + getV57_1() + ";" + getV57_2() + ";"
                    + getV57_3() + ";" + getV57_4() + ";" + getV57_5()
                    +";" + getV57_6() + ";" + getV57_7() + ";" + getV57_8()
                    + ";" + getV57_9() + ";" + getV57_10() ;
        }
        else if(vignetteFilled_3 == 58){
            newLine = start + end + getV58_1() + ";" + getV58_2() + ";"
                    + getV58_3() + ";" + getV58_4() + ";" + getV58_5()
                    +";" + getV58_6() + ";" + getV58_7() + ";" + getV58_8()
                    + ";" + getV58_9() + ";" + getV58_10() ;
        }
        else if(vignetteFilled_3 == 59){
            newLine = start + end + getV59_1() + ";" + getV59_2() + ";"
                    + getV59_3() + ";" + getV59_4() + ";" + getV59_5()
                    +";" + getV59_6() + ";" + getV59_7() + ";" + getV59_8()
                    + ";" + getV59_9() + ";" + getV59_10() ;
        }

        else if(vignetteFilled_3 == 60){
            newLine = start + end + getV60_1() + ";" + getV60_2() + ";"
                    + getV60_3() + ";" + getV60_4() + ";" + getV60_5()
                    +";" + getV60_6() + ";" + getV60_7() + ";" + getV60_8()
                    + ";" + getV60_9() + ";" + getV60_10() ;
        }
        else if(vignetteFilled_3 == 61){

            newLine = start + end + getV61_1() + ";" + getV61_2() + ";"
                    + getV61_3() + ";" + getV61_4() + ";" + getV61_5()
                    +";" + getV61_6() + ";" + getV61_7() + ";" + getV61_8()
                    + ";" + getV61_9() + ";" + getV61_10() ;

        }
        else if(vignetteFilled_3 == 62){
            newLine = start + end + getV62_1() + ";" + getV62_2() + ";"
                    + getV62_3() + ";" + getV62_4() + ";" + getV62_5()
                    +";" + getV62_6() + ";" + getV62_7() + ";" + getV62_8()
                    + ";" + getV62_9() + ";" + getV62_10() ;
        }
        else if(vignetteFilled_3 == 63){
            newLine = start + end + getV63_1() + ";" + getV63_2() + ";"
                    + getV63_3() + ";" + getV63_4() + ";" + getV63_5()
                    +";" + getV63_6() + ";" + getV63_7() + ";" + getV63_8()
                    + ";" + getV63_9() + ";" + getV63_10() ;
        }
        else if(vignetteFilled_3 == 64){
            newLine = start + end + getV64_1() + ";" + getV64_2() + ";"
                    + getV64_3() + ";" + getV64_4() + ";" + getV64_5()
                    +";" + getV64_6() + ";" + getV64_7() + ";" + getV64_8()
                    + ";" + getV64_9() + ";" + getV64_10() ;
        }

        else{
            throw new Error("No vignette one filled, impossible");
        }


        System.out.println("Resulting in new line : " + newLine );
        return newLine;

    }

    public CsvLine(){


    }


    public String createNewFileLine_4(int clientNumber){

        String newLine = "";

        String end =  this.generateNewLineEnd();

        System.out.println("vignette 4 was filled with " + vignetteFilled_4);

        String start = this.generateNewLineStartVignette_4(clientNumber);


        if(this.vignetteFilled_4 == 1){

            newLine = start + end + getV1_1() + ";" + getV1_2() + ";"
                    + getV1_3() + ";" + getV1_4() + ";" + getV1_5()
                    +";" + getV1_6() + ";" + getV1_7() + ";" + getV1_8()
                    + ";" + getV1_9() + ";" + getV1_10() ;

        }
        else if(vignetteFilled_4 == 2){
            newLine = start + end + getV2_1() + ";" + getV2_2() + ";"
                    + getV2_3() + ";" + getV2_4() + ";" + getV2_5()
                    +";" + getV2_6() + ";" + getV2_7() + ";" + getV2_8()
                    + ";" + getV2_9() + ";" + getV2_10() ;
        }
        else if(vignetteFilled_4 == 3){
            newLine = start + end + getV3_1() + ";" + getV3_2() + ";"
                    + getV3_3() + ";" + getV3_4() + ";" + getV3_5()
                    +";" + getV3_6() + ";" + getV3_7() + ";" + getV3_8()
                    + ";" + getV3_9() + ";" + getV3_10() ;
        }
        else if(vignetteFilled_4 == 4){
            newLine = start + end + getV4_1() + ";" + getV4_2() + ";"
                    + getV4_3() + ";" + getV4_4() + ";" + getV4_5()
                    +";" + getV4_6() + ";" + getV4_7() + ";" + getV4_8()
                    + ";" + getV4_9() + ";" + getV4_10() ;
        }
        else if(vignetteFilled_4 == 5){
            newLine = start + end + getV5_1() + ";" + getV5_2() + ";"
                    + getV5_3() + ";" + getV5_4() + ";" + getV5_5()
                    +";" + getV5_6() + ";" + getV5_7() + ";" + getV5_8()
                    + ";" + getV5_9() + ";" + getV5_10() ;
        }
        else if(vignetteFilled_4 == 6){
            newLine = start + end + getV6_1() + ";" + getV6_2() + ";"
                    + getV6_3() + ";" + getV6_4() + ";" + getV6_5()
                    +";" + getV6_6() + ";" + getV6_7() + ";" + getV6_8()
                    + ";" + getV6_9() + ";" + getV6_10() ;
        }
        else if(vignetteFilled_4 == 7){
            newLine = start + end + getV7_1() + ";" + getV7_2() + ";"
                    + getV7_3() + ";" + getV7_4() + ";" + getV7_5()
                    +";" + getV7_6() + ";" + getV7_7() + ";" + getV7_8()
                    + ";" + getV7_9() + ";" + getV7_10() ;
        }
        else if(vignetteFilled_4 == 8){
            newLine = start + end + getV8_1() + ";" + getV8_2() + ";"
                    + getV8_3() + ";" + getV8_4() + ";" + getV8_5()
                    +";" + getV8_6() + ";" + getV8_7() + ";" + getV8_8()
                    + ";" + getV8_9() + ";" + getV8_10() ;
        }
        else if(vignetteFilled_4 == 9){
            newLine = start + end + getV9_1() + ";" + getV9_2() + ";"
                    + getV9_3() + ";" + getV9_4() + ";" + getV9_5()
                    +";" + getV9_6() + ";" + getV9_7() + ";" + getV9_8()
                    + ";" + getV9_9() + ";" + getV9_10() ;
        }
        else if(vignetteFilled_4 == 10){
            newLine = start + end + getV10_1() + ";" + getV10_2() + ";"
                    + getV10_3() + ";" + getV10_4() + ";" + getV10_5()
                    +";" + getV10_6() + ";" + getV10_7() + ";" + getV10_8()
                    + ";" + getV10_9() + ";" + getV10_10() ;
        }


        else if(vignetteFilled_4 == 11){

            newLine = start + end + getV11_1() + ";" + getV11_2() + ";"
                    + getV11_3() + ";" + getV11_4() + ";" + getV11_5()
                    +";" + getV11_6() + ";" + getV11_7() + ";" + getV11_8()
                    + ";" + getV11_9() + ";" + getV11_10() ;

        }
        else if(vignetteFilled_4 == 12){
            newLine = start + end + getV12_1() + ";" + getV12_2() + ";"
                    + getV12_3() + ";" + getV12_4() + ";" + getV12_5()
                    +";" + getV12_6() + ";" + getV12_7() + ";" + getV12_8()
                    + ";" + getV12_9() + ";" + getV12_10() ;
        }
        else if(vignetteFilled_4 == 13){
            newLine = start + end + getV13_1() + ";" + getV13_2() + ";"
                    + getV13_3() + ";" + getV13_4() + ";" + getV13_5()
                    +";" + getV13_6() + ";" + getV13_7() + ";" + getV13_8()
                    + ";" + getV13_9() + ";" + getV13_10() ;
        }
        else if(vignetteFilled_4 == 14){
            newLine = start + end + getV14_1() + ";" + getV14_2() + ";"
                    + getV14_3() + ";" + getV14_4() + ";" + getV14_5()
                    +";" + getV14_6() + ";" + getV14_7() + ";" + getV14_8()
                    + ";" + getV14_9() + ";" + getV14_10() ;
        }
        else if(vignetteFilled_4 == 15){
            newLine = start + end + getV15_1() + ";" + getV15_2() + ";"
                    + getV15_3() + ";" + getV15_4() + ";" + getV15_5()
                    +";" + getV15_6() + ";" + getV15_7() + ";" + getV15_8()
                    + ";" + getV15_9() + ";" + getV15_10() ;
        }
        else if(vignetteFilled_4 == 16){
            newLine = start + end + getV16_1() + ";" + getV16_2() + ";"
                    + getV16_3() + ";" + getV16_4() + ";" + getV16_5()
                    +";" + getV16_6() + ";" + getV16_7() + ";" + getV16_8()
                    + ";" + getV16_9() + ";" + getV16_10() ;
        }
        else if(vignetteFilled_4 == 17){
            newLine = start + end + getV17_1() + ";" + getV17_2() + ";"
                    + getV17_3() + ";" + getV17_4() + ";" + getV17_5()
                    +";" + getV17_6() + ";" + getV17_7() + ";" + getV17_8()
                    + ";" + getV17_9() + ";" + getV17_10() ;
        }
        else if(vignetteFilled_4 == 18){
            newLine = start + end + getV18_1() + ";" + getV18_2() + ";"
                    + getV18_3() + ";" + getV18_4() + ";" + getV18_5()
                    +";" + getV18_6() + ";" + getV18_7() + ";" + getV18_8()
                    + ";" + getV18_9() + ";" + getV18_10() ;
        }
        else if(vignetteFilled_4 == 19){
            newLine = start + end + getV19_1() + ";" + getV19_2() + ";"
                    + getV19_3() + ";" + getV19_4() + ";" + getV19_5()
                    +";" + getV19_6() + ";" + getV19_7() + ";" + getV19_8()
                    + ";" + getV19_9() + ";" + getV19_10() ;
        }



        else if(vignetteFilled_4 == 20){
            newLine = start + end + getV20_1() + ";" + getV20_2() + ";"
                    + getV20_3() + ";" + getV20_4() + ";" + getV20_5()
                    +";" + getV20_6() + ";" + getV20_7() + ";" + getV20_8()
                    + ";" + getV20_9() + ";" + getV20_10() ;
        }
        else if(vignetteFilled_4 == 21){

            newLine = start + end + getV21_1() + ";" + getV21_2() + ";"
                    + getV21_3() + ";" + getV21_4() + ";" + getV21_5()
                    +";" + getV21_6() + ";" + getV21_7() + ";" + getV21_8()
                    + ";" + getV21_9() + ";" + getV21_10() ;

        }
        else if(vignetteFilled_4 == 22){
            newLine = start + end + getV22_1() + ";" + getV22_2() + ";"
                    + getV22_3() + ";" + getV22_4() + ";" + getV22_5()
                    +";" + getV22_6() + ";" + getV22_7() + ";" + getV22_8()
                    + ";" + getV22_9() + ";" + getV22_10() ;
        }
        else if(vignetteFilled_4 == 23){
            newLine = start + end + getV23_1() + ";" + getV23_2() + ";"
                    + getV23_3() + ";" + getV23_4() + ";" + getV23_5()
                    +";" + getV23_6() + ";" + getV23_7() + ";" + getV23_8()
                    + ";" + getV23_9() + ";" + getV23_10() ;
        }
        else if(vignetteFilled_4 == 24){
            newLine = start + end + getV24_1() + ";" + getV24_2() + ";"
                    + getV24_3() + ";" + getV24_4() + ";" + getV24_5()
                    +";" + getV24_6() + ";" + getV24_7() + ";" + getV24_8()
                    + ";" + getV24_9() + ";" + getV24_10() ;
        }
        else if(vignetteFilled_4 == 25){
            newLine = start + end + getV25_1() + ";" + getV25_2() + ";"
                    + getV25_3() + ";" + getV25_4() + ";" + getV25_5()
                    +";" + getV25_6() + ";" + getV25_7() + ";" + getV25_8()
                    + ";" + getV25_9() + ";" + getV25_10() ;
        }
        else if(vignetteFilled_4 == 26){
            newLine = start + end + getV26_1() + ";" + getV26_2() + ";"
                    + getV26_3() + ";" + getV26_4() + ";" + getV26_5()
                    +";" + getV26_6() + ";" + getV26_7() + ";" + getV26_8()
                    + ";" + getV26_9() + ";" + getV26_10() ;
        }
        else if(vignetteFilled_4 == 27){
            newLine = start + end + getV27_1() + ";" + getV27_2() + ";"
                    + getV27_3() + ";" + getV27_4() + ";" + getV27_5()
                    +";" + getV27_6() + ";" + getV27_7() + ";" + getV27_8()
                    + ";" + getV27_9() + ";" + getV27_10() ;
        }
        else if(vignetteFilled_4 == 28){
            newLine = start + end + getV28_1() + ";" + getV28_2() + ";"
                    + getV28_3() + ";" + getV28_4() + ";" + getV28_5()
                    +";" + getV28_6() + ";" + getV28_7() + ";" + getV28_8()
                    + ";" + getV28_9() + ";" + getV28_10() ;
        }
        else if(vignetteFilled_4 == 29){
            newLine = start + end + getV29_1() + ";" + getV29_2() + ";"
                    + getV29_3() + ";" + getV29_4() + ";" + getV29_5()
                    +";" + getV29_6() + ";" + getV29_7() + ";" + getV29_8()
                    + ";" + getV29_9() + ";" + getV29_10() ;
        }


        else if(vignetteFilled_4 == 30){
            newLine = start + end + getV30_1() + ";" + getV30_2() + ";"
                    + getV30_3() + ";" + getV30_4() + ";" + getV30_5()
                    +";" + getV30_6() + ";" + getV30_7() + ";" + getV30_8()
                    + ";" + getV30_9() + ";" + getV30_10() ;
        }
        else if(vignetteFilled_4 == 31){

            newLine = start + end + getV31_1() + ";" + getV31_2() + ";"
                    + getV31_3() + ";" + getV31_4() + ";" + getV31_5()
                    +";" + getV31_6() + ";" + getV31_7() + ";" + getV31_8()
                    + ";" + getV31_9() + ";" + getV31_10() ;

        }
        else if(vignetteFilled_4 == 32){
            newLine = start + end + getV32_1() + ";" + getV32_2() + ";"
                    + getV32_3() + ";" + getV32_4() + ";" + getV32_5()
                    +";" + getV32_6() + ";" + getV32_7() + ";" + getV32_8()
                    + ";" + getV32_9() + ";" + getV32_10() ;
        }
        else if(vignetteFilled_4 == 33){
            newLine = start + end + getV33_1() + ";" + getV33_2() + ";"
                    + getV33_3() + ";" + getV33_4() + ";" + getV33_5()
                    +";" + getV33_6() + ";" + getV33_7() + ";" + getV33_8()
                    + ";" + getV33_9() + ";" + getV33_10() ;
        }
        else if(vignetteFilled_4 == 34){
            newLine = start + end + getV34_1() + ";" + getV34_2() + ";"
                    + getV34_3() + ";" + getV34_4() + ";" + getV34_5()
                    +";" + getV34_6() + ";" + getV34_7() + ";" + getV34_8()
                    + ";" + getV34_9() + ";" + getV34_10() ;
        }
        else if(vignetteFilled_4 == 35){
            newLine = start + end + getV35_1() + ";" + getV35_2() + ";"
                    + getV35_3() + ";" + getV35_4() + ";" + getV35_5()
                    +";" + getV35_6() + ";" + getV35_7() + ";" + getV35_8()
                    + ";" + getV35_9() + ";" + getV35_10() ;
        }
        else if(vignetteFilled_4 == 36){
            newLine = start + end + getV36_1() + ";" + getV36_2() + ";"
                    + getV36_3() + ";" + getV36_4() + ";" + getV36_5()
                    +";" + getV36_6() + ";" + getV36_7() + ";" + getV36_8()
                    + ";" + getV36_9() + ";" + getV36_10() ;
        }
        else if(vignetteFilled_4 == 37){
            newLine = start + end + getV37_1() + ";" + getV37_2() + ";"
                    + getV37_3() + ";" + getV37_4() + ";" + getV37_5()
                    +";" + getV37_6() + ";" + getV37_7() + ";" + getV37_8()
                    + ";" + getV37_9() + ";" + getV37_10() ;
        }
        else if(vignetteFilled_4 == 38){
            newLine = start + end + getV38_1() + ";" + getV38_2() + ";"
                    + getV38_3() + ";" + getV38_4() + ";" + getV38_5()
                    +";" + getV38_6() + ";" + getV38_7() + ";" + getV38_8()
                    + ";" + getV38_9() + ";" + getV38_10() ;
        }
        else if(vignetteFilled_4 == 39){
            newLine = start + end + getV39_1() + ";" + getV39_2() + ";"
                    + getV39_3() + ";" + getV39_4() + ";" + getV39_5()
                    +";" + getV39_6() + ";" + getV39_7() + ";" + getV39_8()
                    + ";" + getV39_9() + ";" + getV39_10() ;
        }

        else if(vignetteFilled_4 == 40){
            newLine = start + end + getV40_1() + ";" + getV40_2() + ";"
                    + getV40_3() + ";" + getV40_4() + ";" + getV40_5()
                    +";" + getV40_6() + ";" + getV40_7() + ";" + getV40_8()
                    + ";" + getV40_9() + ";" + getV40_10() ;
        }
        else if(vignetteFilled_4 == 41){

            newLine = start + end + getV41_1() + ";" + getV41_2() + ";"
                    + getV41_3() + ";" + getV41_4() + ";" + getV41_5()
                    +";" + getV41_6() + ";" + getV41_7() + ";" + getV41_8()
                    + ";" + getV41_9() + ";" + getV41_10() ;

        }
        else if(vignetteFilled_4 == 42){
            newLine = start + end + getV42_1() + ";" + getV42_2() + ";"
                    + getV42_3() + ";" + getV42_4() + ";" + getV42_5()
                    +";" + getV42_6() + ";" + getV42_7() + ";" + getV42_8()
                    + ";" + getV42_9() + ";" + getV42_10() ;
        }
        else if(vignetteFilled_4 == 43){
            newLine = start + end + getV43_1() + ";" + getV43_2() + ";"
                    + getV43_3() + ";" + getV43_4() + ";" + getV43_5()
                    +";" + getV43_6() + ";" + getV43_7() + ";" + getV43_8()
                    + ";" + getV43_9() + ";" + getV43_10() ;
        }
        else if(vignetteFilled_4 == 44){
            newLine = start + end + getV44_1() + ";" + getV44_2() + ";"
                    + getV44_3() + ";" + getV44_4() + ";" + getV44_5()
                    +";" + getV44_6() + ";" + getV44_7() + ";" + getV44_8()
                    + ";" + getV44_9() + ";" + getV44_10() ;
        }
        else if(vignetteFilled_4 == 45){
            newLine = start + end + getV45_1() + ";" + getV45_2() + ";"
                    + getV45_3() + ";" + getV45_4() + ";" + getV45_5()
                    +";" + getV45_6() + ";" + getV45_7() + ";" + getV45_8()
                    + ";" + getV45_9() + ";" + getV45_10() ;
        }
        else if(vignetteFilled_4 == 46){
            newLine = start + end + getV46_1() + ";" + getV46_2() + ";"
                    + getV46_3() + ";" + getV46_4() + ";" + getV46_5()
                    +";" + getV46_6() + ";" + getV46_7() + ";" + getV46_8()
                    + ";" + getV46_9() + ";" + getV46_10() ;
        }
        else if(vignetteFilled_4 == 47){
            newLine = start + end + getV47_1() + ";" + getV47_2() + ";"
                    + getV47_3() + ";" + getV47_4() + ";" + getV47_5()
                    +";" + getV47_6() + ";" + getV47_7() + ";" + getV47_8()
                    + ";" + getV47_9() + ";" + getV47_10() ;
        }
        else if(vignetteFilled_4 == 48){
            newLine = start + end + getV48_1() + ";" + getV48_2() + ";"
                    + getV48_3() + ";" + getV48_4() + ";" + getV48_5()
                    +";" + getV48_6() + ";" + getV48_7() + ";" + getV48_8()
                    + ";" + getV48_9() + ";" + getV48_10() ;
        }
        else if(vignetteFilled_4 == 49){
            newLine = start + end + getV49_1() + ";" + getV49_2() + ";"
                    + getV49_3() + ";" + getV49_4() + ";" + getV49_5()
                    +";" + getV49_6() + ";" + getV49_7() + ";" + getV49_8()
                    + ";" + getV49_9() + ";" + getV49_10() ;
        }


        else if(vignetteFilled_4 == 50){
            newLine = start + end + getV50_1() + ";" + getV50_2() + ";"
                    + getV50_3() + ";" + getV50_4() + ";" + getV50_5()
                    +";" + getV50_6() + ";" + getV50_7() + ";" + getV50_8()
                    + ";" + getV50_9() + ";" + getV50_10() ;
        }
        else if(vignetteFilled_4 == 51){

            newLine = start + end + getV51_1() + ";" + getV51_2() + ";"
                    + getV51_3() + ";" + getV51_4() + ";" + getV51_5()
                    +";" + getV51_6() + ";" + getV51_7() + ";" + getV51_8()
                    + ";" + getV51_9() + ";" + getV51_10() ;

        }
        else if(vignetteFilled_4 == 52){
            newLine = start + end + getV52_1() + ";" + getV52_2() + ";"
                    + getV52_3() + ";" + getV52_4() + ";" + getV52_5()
                    +";" + getV52_6() + ";" + getV52_7() + ";" + getV52_8()
                    + ";" + getV52_9() + ";" + getV52_10() ;
        }
        else if(vignetteFilled_4 == 53){
            newLine = start + end + getV53_1() + ";" + getV53_2() + ";"
                    + getV53_3() + ";" + getV53_4() + ";" + getV53_5()
                    +";" + getV53_6() + ";" + getV53_7() + ";" + getV53_8()
                    + ";" + getV53_9() + ";" + getV53_10() ;
        }
        else if(vignetteFilled_4 == 54){
            newLine = start + end + getV54_1() + ";" + getV54_2() + ";"
                    + getV54_3() + ";" + getV54_4() + ";" + getV54_5()
                    +";" + getV54_6() + ";" + getV54_7() + ";" + getV54_8()
                    + ";" + getV54_9() + ";" + getV54_10() ;
        }
        else if(vignetteFilled_4 == 55){
            newLine = start + end + getV55_1() + ";" + getV55_2() + ";"
                    + getV55_3() + ";" + getV55_4() + ";" + getV55_5()
                    +";" + getV55_6() + ";" + getV55_7() + ";" + getV55_8()
                    + ";" + getV55_9() + ";" + getV55_10() ;
        }
        else if(vignetteFilled_4 == 56){
            newLine = start + end + getV56_1() + ";" + getV56_2() + ";"
                    + getV56_3() + ";" + getV56_4() + ";" + getV56_5()
                    +";" + getV56_6() + ";" + getV56_7() + ";" + getV56_8()
                    + ";" + getV56_9() + ";" + getV56_10() ;
        }
        else if(vignetteFilled_4 == 57){
            newLine = start + end + getV57_1() + ";" + getV57_2() + ";"
                    + getV57_3() + ";" + getV57_4() + ";" + getV57_5()
                    +";" + getV57_6() + ";" + getV57_7() + ";" + getV57_8()
                    + ";" + getV57_9() + ";" + getV57_10() ;
        }
        else if(vignetteFilled_4 == 58){
            newLine = start + end + getV58_1() + ";" + getV58_2() + ";"
                    + getV58_3() + ";" + getV58_4() + ";" + getV58_5()
                    +";" + getV58_6() + ";" + getV58_7() + ";" + getV58_8()
                    + ";" + getV58_9() + ";" + getV58_10() ;
        }
        else if(vignetteFilled_4 == 59){
            newLine = start + end + getV59_1() + ";" + getV59_2() + ";"
                    + getV59_3() + ";" + getV59_4() + ";" + getV59_5()
                    +";" + getV59_6() + ";" + getV59_7() + ";" + getV59_8()
                    + ";" + getV59_9() + ";" + getV59_10() ;
        }

        else if(vignetteFilled_4 == 60){
            newLine = start + end + getV60_1() + ";" + getV60_2() + ";"
                    + getV60_3() + ";" + getV60_4() + ";" + getV60_5()
                    +";" + getV60_6() + ";" + getV60_7() + ";" + getV60_8()
                    + ";" + getV60_9() + ";" + getV60_10() ;
        }
        else if(vignetteFilled_4 == 61){

            newLine = start + end + getV61_1() + ";" + getV61_2() + ";"
                    + getV61_3() + ";" + getV61_4() + ";" + getV61_5()
                    +";" + getV61_6() + ";" + getV61_7() + ";" + getV61_8()
                    + ";" + getV61_9() + ";" + getV61_10() ;

        }
        else if(vignetteFilled_4 == 62){
            newLine = start + end + getV62_1() + ";" + getV62_2() + ";"
                    + getV62_3() + ";" + getV62_4() + ";" + getV62_5()
                    +";" + getV62_6() + ";" + getV62_7() + ";" + getV62_8()
                    + ";" + getV62_9() + ";" + getV62_10() ;
        }
        else if(vignetteFilled_4 == 63){
            newLine = start + end + getV63_1() + ";" + getV63_2() + ";"
                    + getV63_3() + ";" + getV63_4() + ";" + getV63_5()
                    +";" + getV63_6() + ";" + getV63_7() + ";" + getV63_8()
                    + ";" + getV63_9() + ";" + getV63_10() ;
        }
        else if(vignetteFilled_4 == 64){
            newLine = start + end + getV64_1() + ";" + getV64_2() + ";"
                    + getV64_3() + ";" + getV64_4() + ";" + getV64_5()
                    +";" + getV64_6() + ";" + getV64_7() + ";" + getV64_8()
                    + ";" + getV64_9() + ";" + getV64_10() ;
        }

        else{
            throw new Error("No vignette one filled, impossible");
        }


        System.out.println("Resulting in new line : " + newLine );
        return newLine;

    }


    public String createNewFileLine_5(int clientNumber){

        String newLine = "";

        String end =  this.generateNewLineEnd();

        System.out.println("vignette 5 was filled with " + vignetteFilled_5);

        String start = this.generateNewLineStartVignette_5(clientNumber);


        if(this.vignetteFilled_5 == 1){

            newLine = start + end + getV1_1() + ";" + getV1_2() + ";"
                    + getV1_3() + ";" + getV1_4() + ";" + getV1_5()
                    +";" + getV1_6() + ";" + getV1_7() + ";" + getV1_8()
                    + ";" + getV1_9() + ";" + getV1_10() ;

        }
        else if(vignetteFilled_5 == 2){
            newLine = start + end + getV2_1() + ";" + getV2_2() + ";"
                    + getV2_3() + ";" + getV2_4() + ";" + getV2_5()
                    +";" + getV2_6() + ";" + getV2_7() + ";" + getV2_8()
                    + ";" + getV2_9() + ";" + getV2_10() ;
        }
        else if(vignetteFilled_5 == 3){
            newLine = start + end + getV3_1() + ";" + getV3_2() + ";"
                    + getV3_3() + ";" + getV3_4() + ";" + getV3_5()
                    +";" + getV3_6() + ";" + getV3_7() + ";" + getV3_8()
                    + ";" + getV3_9() + ";" + getV3_10() ;
        }
        else if(vignetteFilled_5 == 4){
            newLine = start + end + getV4_1() + ";" + getV4_2() + ";"
                    + getV4_3() + ";" + getV4_4() + ";" + getV4_5()
                    +";" + getV4_6() + ";" + getV4_7() + ";" + getV4_8()
                    + ";" + getV4_9() + ";" + getV4_10() ;
        }
        else if(vignetteFilled_5 == 5){
            newLine = start + end + getV5_1() + ";" + getV5_2() + ";"
                    + getV5_3() + ";" + getV5_4() + ";" + getV5_5()
                    +";" + getV5_6() + ";" + getV5_7() + ";" + getV5_8()
                    + ";" + getV5_9() + ";" + getV5_10() ;
        }
        else if(vignetteFilled_5 == 6){
            newLine = start + end + getV6_1() + ";" + getV6_2() + ";"
                    + getV6_3() + ";" + getV6_4() + ";" + getV6_5()
                    +";" + getV6_6() + ";" + getV6_7() + ";" + getV6_8()
                    + ";" + getV6_9() + ";" + getV6_10() ;
        }
        else if(vignetteFilled_5 == 7){
            newLine = start + end + getV7_1() + ";" + getV7_2() + ";"
                    + getV7_3() + ";" + getV7_4() + ";" + getV7_5()
                    +";" + getV7_6() + ";" + getV7_7() + ";" + getV7_8()
                    + ";" + getV7_9() + ";" + getV7_10() ;
        }
        else if(vignetteFilled_5 == 8){
            newLine = start + end + getV8_1() + ";" + getV8_2() + ";"
                    + getV8_3() + ";" + getV8_4() + ";" + getV8_5()
                    +";" + getV8_6() + ";" + getV8_7() + ";" + getV8_8()
                    + ";" + getV8_9() + ";" + getV8_10() ;
        }
        else if(vignetteFilled_5 == 9){
            newLine = start + end + getV9_1() + ";" + getV9_2() + ";"
                    + getV9_3() + ";" + getV9_4() + ";" + getV9_5()
                    +";" + getV9_6() + ";" + getV9_7() + ";" + getV9_8()
                    + ";" + getV9_9() + ";" + getV9_10() ;
        }
        else if(vignetteFilled_5 == 10){
            newLine = start + end + getV10_1() + ";" + getV10_2() + ";"
                    + getV10_3() + ";" + getV10_4() + ";" + getV10_5()
                    +";" + getV10_6() + ";" + getV10_7() + ";" + getV10_8()
                    + ";" + getV10_9() + ";" + getV10_10() ;
        }


        else if(vignetteFilled_5 == 11){

            newLine = start + end + getV11_1() + ";" + getV11_2() + ";"
                    + getV11_3() + ";" + getV11_4() + ";" + getV11_5()
                    +";" + getV11_6() + ";" + getV11_7() + ";" + getV11_8()
                    + ";" + getV11_9() + ";" + getV11_10() ;

        }
        else if(vignetteFilled_5 == 12){
            newLine = start + end + getV12_1() + ";" + getV12_2() + ";"
                    + getV12_3() + ";" + getV12_4() + ";" + getV12_5()
                    +";" + getV12_6() + ";" + getV12_7() + ";" + getV12_8()
                    + ";" + getV12_9() + ";" + getV12_10() ;
        }
        else if(vignetteFilled_5 == 13){
            newLine = start + end + getV13_1() + ";" + getV13_2() + ";"
                    + getV13_3() + ";" + getV13_4() + ";" + getV13_5()
                    +";" + getV13_6() + ";" + getV13_7() + ";" + getV13_8()
                    + ";" + getV13_9() + ";" + getV13_10() ;
        }
        else if(vignetteFilled_5 == 14){
            newLine = start + end + getV14_1() + ";" + getV14_2() + ";"
                    + getV14_3() + ";" + getV14_4() + ";" + getV14_5()
                    +";" + getV14_6() + ";" + getV14_7() + ";" + getV14_8()
                    + ";" + getV14_9() + ";" + getV14_10() ;
        }
        else if(vignetteFilled_5 == 15){
            newLine = start + end + getV15_1() + ";" + getV15_2() + ";"
                    + getV15_3() + ";" + getV15_4() + ";" + getV15_5()
                    +";" + getV15_6() + ";" + getV15_7() + ";" + getV15_8()
                    + ";" + getV15_9() + ";" + getV15_10() ;
        }
        else if(vignetteFilled_5 == 16){
            newLine = start + end + getV16_1() + ";" + getV16_2() + ";"
                    + getV16_3() + ";" + getV16_4() + ";" + getV16_5()
                    +";" + getV16_6() + ";" + getV16_7() + ";" + getV16_8()
                    + ";" + getV16_9() + ";" + getV16_10() ;
        }
        else if(vignetteFilled_5 == 17){
            newLine = start + end + getV17_1() + ";" + getV17_2() + ";"
                    + getV17_3() + ";" + getV17_4() + ";" + getV17_5()
                    +";" + getV17_6() + ";" + getV17_7() + ";" + getV17_8()
                    + ";" + getV17_9() + ";" + getV17_10() ;
        }
        else if(vignetteFilled_5 == 18){
            newLine = start + end + getV18_1() + ";" + getV18_2() + ";"
                    + getV18_3() + ";" + getV18_4() + ";" + getV18_5()
                    +";" + getV18_6() + ";" + getV18_7() + ";" + getV18_8()
                    + ";" + getV18_9() + ";" + getV18_10() ;
        }
        else if(vignetteFilled_5 == 19){
            newLine = start + end + getV19_1() + ";" + getV19_2() + ";"
                    + getV19_3() + ";" + getV19_4() + ";" + getV19_5()
                    +";" + getV19_6() + ";" + getV19_7() + ";" + getV19_8()
                    + ";" + getV19_9() + ";" + getV19_10() ;
        }



        else if(vignetteFilled_5 == 20){
            newLine = start + end + getV20_1() + ";" + getV20_2() + ";"
                    + getV20_3() + ";" + getV20_4() + ";" + getV20_5()
                    +";" + getV20_6() + ";" + getV20_7() + ";" + getV20_8()
                    + ";" + getV20_9() + ";" + getV20_10() ;
        }
        else if(vignetteFilled_5 == 21){

            newLine = start + end + getV21_1() + ";" + getV21_2() + ";"
                    + getV21_3() + ";" + getV21_4() + ";" + getV21_5()
                    +";" + getV21_6() + ";" + getV21_7() + ";" + getV21_8()
                    + ";" + getV21_9() + ";" + getV21_10() ;

        }
        else if(vignetteFilled_5 == 22){
            newLine = start + end + getV22_1() + ";" + getV22_2() + ";"
                    + getV22_3() + ";" + getV22_4() + ";" + getV22_5()
                    +";" + getV22_6() + ";" + getV22_7() + ";" + getV22_8()
                    + ";" + getV22_9() + ";" + getV22_10() ;
        }
        else if(vignetteFilled_5 == 23){
            newLine = start + end + getV23_1() + ";" + getV23_2() + ";"
                    + getV23_3() + ";" + getV23_4() + ";" + getV23_5()
                    +";" + getV23_6() + ";" + getV23_7() + ";" + getV23_8()
                    + ";" + getV23_9() + ";" + getV23_10() ;
        }
        else if(vignetteFilled_5 == 24){
            newLine = start + end + getV24_1() + ";" + getV24_2() + ";"
                    + getV24_3() + ";" + getV24_4() + ";" + getV24_5()
                    +";" + getV24_6() + ";" + getV24_7() + ";" + getV24_8()
                    + ";" + getV24_9() + ";" + getV24_10() ;
        }
        else if(vignetteFilled_5 == 25){
            newLine = start + end + getV25_1() + ";" + getV25_2() + ";"
                    + getV25_3() + ";" + getV25_4() + ";" + getV25_5()
                    +";" + getV25_6() + ";" + getV25_7() + ";" + getV25_8()
                    + ";" + getV25_9() + ";" + getV25_10() ;
        }
        else if(vignetteFilled_5 == 26){
            newLine = start + end + getV26_1() + ";" + getV26_2() + ";"
                    + getV26_3() + ";" + getV26_4() + ";" + getV26_5()
                    +";" + getV26_6() + ";" + getV26_7() + ";" + getV26_8()
                    + ";" + getV26_9() + ";" + getV26_10() ;
        }
        else if(vignetteFilled_5 == 27){
            newLine = start + end + getV27_1() + ";" + getV27_2() + ";"
                    + getV27_3() + ";" + getV27_4() + ";" + getV27_5()
                    +";" + getV27_6() + ";" + getV27_7() + ";" + getV27_8()
                    + ";" + getV27_9() + ";" + getV27_10() ;
        }
        else if(vignetteFilled_5 == 28){
            newLine = start + end + getV28_1() + ";" + getV28_2() + ";"
                    + getV28_3() + ";" + getV28_4() + ";" + getV28_5()
                    +";" + getV28_6() + ";" + getV28_7() + ";" + getV28_8()
                    + ";" + getV28_9() + ";" + getV28_10() ;
        }
        else if(vignetteFilled_5 == 29){
            newLine = start + end + getV29_1() + ";" + getV29_2() + ";"
                    + getV29_3() + ";" + getV29_4() + ";" + getV29_5()
                    +";" + getV29_6() + ";" + getV29_7() + ";" + getV29_8()
                    + ";" + getV29_9() + ";" + getV29_10() ;
        }


        else if(vignetteFilled_5 == 30){
            newLine = start + end + getV30_1() + ";" + getV30_2() + ";"
                    + getV30_3() + ";" + getV30_4() + ";" + getV30_5()
                    +";" + getV30_6() + ";" + getV30_7() + ";" + getV30_8()
                    + ";" + getV30_9() + ";" + getV30_10() ;
        }
        else if(vignetteFilled_5 == 31){

            newLine = start + end + getV31_1() + ";" + getV31_2() + ";"
                    + getV31_3() + ";" + getV31_4() + ";" + getV31_5()
                    +";" + getV31_6() + ";" + getV31_7() + ";" + getV31_8()
                    + ";" + getV31_9() + ";" + getV31_10() ;

        }
        else if(vignetteFilled_5 == 32){
            newLine = start + end + getV32_1() + ";" + getV32_2() + ";"
                    + getV32_3() + ";" + getV32_4() + ";" + getV32_5()
                    +";" + getV32_6() + ";" + getV32_7() + ";" + getV32_8()
                    + ";" + getV32_9() + ";" + getV32_10() ;
        }
        else if(vignetteFilled_5 == 33){
            newLine = start + end + getV33_1() + ";" + getV33_2() + ";"
                    + getV33_3() + ";" + getV33_4() + ";" + getV33_5()
                    +";" + getV33_6() + ";" + getV33_7() + ";" + getV33_8()
                    + ";" + getV33_9() + ";" + getV33_10() ;
        }
        else if(vignetteFilled_5 == 34){
            newLine = start + end + getV34_1() + ";" + getV34_2() + ";"
                    + getV34_3() + ";" + getV34_4() + ";" + getV34_5()
                    +";" + getV34_6() + ";" + getV34_7() + ";" + getV34_8()
                    + ";" + getV34_9() + ";" + getV34_10() ;
        }
        else if(vignetteFilled_5 == 35){
            newLine = start + end + getV35_1() + ";" + getV35_2() + ";"
                    + getV35_3() + ";" + getV35_4() + ";" + getV35_5()
                    +";" + getV35_6() + ";" + getV35_7() + ";" + getV35_8()
                    + ";" + getV35_9() + ";" + getV35_10() ;
        }
        else if(vignetteFilled_5 == 36){
            newLine = start + end + getV36_1() + ";" + getV36_2() + ";"
                    + getV36_3() + ";" + getV36_4() + ";" + getV36_5()
                    +";" + getV36_6() + ";" + getV36_7() + ";" + getV36_8()
                    + ";" + getV36_9() + ";" + getV36_10() ;
        }
        else if(vignetteFilled_5 == 37){
            newLine = start + end + getV37_1() + ";" + getV37_2() + ";"
                    + getV37_3() + ";" + getV37_4() + ";" + getV37_5()
                    +";" + getV37_6() + ";" + getV37_7() + ";" + getV37_8()
                    + ";" + getV37_9() + ";" + getV37_10() ;
        }
        else if(vignetteFilled_5 == 38){
            newLine = start + end + getV38_1() + ";" + getV38_2() + ";"
                    + getV38_3() + ";" + getV38_4() + ";" + getV38_5()
                    +";" + getV38_6() + ";" + getV38_7() + ";" + getV38_8()
                    + ";" + getV38_9() + ";" + getV38_10() ;
        }
        else if(vignetteFilled_5 == 39){
            newLine = start + end + getV39_1() + ";" + getV39_2() + ";"
                    + getV39_3() + ";" + getV39_4() + ";" + getV39_5()
                    +";" + getV39_6() + ";" + getV39_7() + ";" + getV39_8()
                    + ";" + getV39_9() + ";" + getV39_10() ;
        }

        else if(vignetteFilled_5 == 40){
            newLine = start + end + getV40_1() + ";" + getV40_2() + ";"
                    + getV40_3() + ";" + getV40_4() + ";" + getV40_5()
                    +";" + getV40_6() + ";" + getV40_7() + ";" + getV40_8()
                    + ";" + getV40_9() + ";" + getV40_10() ;
        }
        else if(vignetteFilled_5 == 41){

            newLine = start + end + getV41_1() + ";" + getV41_2() + ";"
                    + getV41_3() + ";" + getV41_4() + ";" + getV41_5()
                    +";" + getV41_6() + ";" + getV41_7() + ";" + getV41_8()
                    + ";" + getV41_9() + ";" + getV41_10() ;

        }
        else if(vignetteFilled_5 == 42){
            newLine = start + end + getV42_1() + ";" + getV42_2() + ";"
                    + getV42_3() + ";" + getV42_4() + ";" + getV42_5()
                    +";" + getV42_6() + ";" + getV42_7() + ";" + getV42_8()
                    + ";" + getV42_9() + ";" + getV42_10() ;
        }
        else if(vignetteFilled_5 == 43){
            newLine = start + end + getV43_1() + ";" + getV43_2() + ";"
                    + getV43_3() + ";" + getV43_4() + ";" + getV43_5()
                    +";" + getV43_6() + ";" + getV43_7() + ";" + getV43_8()
                    + ";" + getV43_9() + ";" + getV43_10() ;
        }
        else if(vignetteFilled_5 == 44){
            newLine = start + end + getV44_1() + ";" + getV44_2() + ";"
                    + getV44_3() + ";" + getV44_4() + ";" + getV44_5()
                    +";" + getV44_6() + ";" + getV44_7() + ";" + getV44_8()
                    + ";" + getV44_9() + ";" + getV44_10() ;
        }
        else if(vignetteFilled_5 == 45){
            newLine = start + end + getV45_1() + ";" + getV45_2() + ";"
                    + getV45_3() + ";" + getV45_4() + ";" + getV45_5()
                    +";" + getV45_6() + ";" + getV45_7() + ";" + getV45_8()
                    + ";" + getV45_9() + ";" + getV45_10() ;
        }
        else if(vignetteFilled_5 == 46){
            newLine = start + end + getV46_1() + ";" + getV46_2() + ";"
                    + getV46_3() + ";" + getV46_4() + ";" + getV46_5()
                    +";" + getV46_6() + ";" + getV46_7() + ";" + getV46_8()
                    + ";" + getV46_9() + ";" + getV46_10() ;
        }
        else if(vignetteFilled_5 == 47){
            newLine = start + end + getV47_1() + ";" + getV47_2() + ";"
                    + getV47_3() + ";" + getV47_4() + ";" + getV47_5()
                    +";" + getV47_6() + ";" + getV47_7() + ";" + getV47_8()
                    + ";" + getV47_9() + ";" + getV47_10() ;
        }
        else if(vignetteFilled_5 == 48){
            newLine = start + end + getV48_1() + ";" + getV48_2() + ";"
                    + getV48_3() + ";" + getV48_4() + ";" + getV48_5()
                    +";" + getV48_6() + ";" + getV48_7() + ";" + getV48_8()
                    + ";" + getV48_9() + ";" + getV48_10() ;
        }
        else if(vignetteFilled_5 == 49){
            newLine = start + end + getV49_1() + ";" + getV49_2() + ";"
                    + getV49_3() + ";" + getV49_4() + ";" + getV49_5()
                    +";" + getV49_6() + ";" + getV49_7() + ";" + getV49_8()
                    + ";" + getV49_9() + ";" + getV49_10() ;
        }


        else if(vignetteFilled_5 == 50){
            newLine = start + end + getV50_1() + ";" + getV50_2() + ";"
                    + getV50_3() + ";" + getV50_4() + ";" + getV50_5()
                    +";" + getV50_6() + ";" + getV50_7() + ";" + getV50_8()
                    + ";" + getV50_9() + ";" + getV50_10() ;
        }
        else if(vignetteFilled_5 == 51){

            newLine = start + end + getV51_1() + ";" + getV51_2() + ";"
                    + getV51_3() + ";" + getV51_4() + ";" + getV51_5()
                    +";" + getV51_6() + ";" + getV51_7() + ";" + getV51_8()
                    + ";" + getV51_9() + ";" + getV51_10() ;

        }
        else if(vignetteFilled_5 == 52){
            newLine = start + end + getV52_1() + ";" + getV52_2() + ";"
                    + getV52_3() + ";" + getV52_4() + ";" + getV52_5()
                    +";" + getV52_6() + ";" + getV52_7() + ";" + getV52_8()
                    + ";" + getV52_9() + ";" + getV52_10() ;
        }
        else if(vignetteFilled_5 == 53){
            newLine = start + end + getV53_1() + ";" + getV53_2() + ";"
                    + getV53_3() + ";" + getV53_4() + ";" + getV53_5()
                    +";" + getV53_6() + ";" + getV53_7() + ";" + getV53_8()
                    + ";" + getV53_9() + ";" + getV53_10() ;
        }
        else if(vignetteFilled_5 == 54){
            newLine = start + end + getV54_1() + ";" + getV54_2() + ";"
                    + getV54_3() + ";" + getV54_4() + ";" + getV54_5()
                    +";" + getV54_6() + ";" + getV54_7() + ";" + getV54_8()
                    + ";" + getV54_9() + ";" + getV54_10() ;
        }
        else if(vignetteFilled_5 == 55){
            newLine = start + end + getV55_1() + ";" + getV55_2() + ";"
                    + getV55_3() + ";" + getV55_4() + ";" + getV55_5()
                    +";" + getV55_6() + ";" + getV55_7() + ";" + getV55_8()
                    + ";" + getV55_9() + ";" + getV55_10() ;
        }
        else if(vignetteFilled_5 == 56){
            newLine = start + end + getV56_1() + ";" + getV56_2() + ";"
                    + getV56_3() + ";" + getV56_4() + ";" + getV56_5()
                    +";" + getV56_6() + ";" + getV56_7() + ";" + getV56_8()
                    + ";" + getV56_9() + ";" + getV56_10() ;
        }
        else if(vignetteFilled_5 == 57){
            newLine = start + end + getV57_1() + ";" + getV57_2() + ";"
                    + getV57_3() + ";" + getV57_4() + ";" + getV57_5()
                    +";" + getV57_6() + ";" + getV57_7() + ";" + getV57_8()
                    + ";" + getV57_9() + ";" + getV57_10() ;
        }
        else if(vignetteFilled_5 == 58){
            newLine = start + end + getV58_1() + ";" + getV58_2() + ";"
                    + getV58_3() + ";" + getV58_4() + ";" + getV58_5()
                    +";" + getV58_6() + ";" + getV58_7() + ";" + getV58_8()
                    + ";" + getV58_9() + ";" + getV58_10() ;
        }
        else if(vignetteFilled_5 == 59){
            newLine = start + end + getV59_1() + ";" + getV59_2() + ";"
                    + getV59_3() + ";" + getV59_4() + ";" + getV59_5()
                    +";" + getV59_6() + ";" + getV59_7() + ";" + getV59_8()
                    + ";" + getV59_9() + ";" + getV59_10() ;
        }

        else if(vignetteFilled_5 == 60){
            newLine = start + end + getV60_1() + ";" + getV60_2() + ";"
                    + getV60_3() + ";" + getV60_4() + ";" + getV60_5()
                    +";" + getV60_6() + ";" + getV60_7() + ";" + getV60_8()
                    + ";" + getV60_9() + ";" + getV60_10() ;
        }
        else if(vignetteFilled_5 == 61){

            newLine = start + end + getV61_1() + ";" + getV61_2() + ";"
                    + getV61_3() + ";" + getV61_4() + ";" + getV61_5()
                    +";" + getV61_6() + ";" + getV61_7() + ";" + getV61_8()
                    + ";" + getV61_9() + ";" + getV61_10() ;

        }
        else if(vignetteFilled_5 == 62){
            newLine = start + end + getV62_1() + ";" + getV62_2() + ";"
                    + getV62_3() + ";" + getV62_4() + ";" + getV62_5()
                    +";" + getV62_6() + ";" + getV62_7() + ";" + getV62_8()
                    + ";" + getV62_9() + ";" + getV62_10() ;
        }
        else if(vignetteFilled_5 == 63){
            newLine = start + end + getV63_1() + ";" + getV63_2() + ";"
                    + getV63_3() + ";" + getV63_4() + ";" + getV63_5()
                    +";" + getV63_6() + ";" + getV63_7() + ";" + getV63_8()
                    + ";" + getV63_9() + ";" + getV63_10() ;
        }
        else if(vignetteFilled_5 == 64){
            newLine = start + end + getV64_1() + ";" + getV64_2() + ";"
                    + getV64_3() + ";" + getV64_4() + ";" + getV64_5()
                    +";" + getV64_6() + ";" + getV64_7() + ";" + getV64_8()
                    + ";" + getV64_9() + ";" + getV64_10() ;
        }

        else{
            throw new Error("No vignette one filled, impossible");
        }


        System.out.println("Resulting in new line : " + newLine );
        return newLine;

    }




    public String createNewFileLine_6(int clientNumber){

        String newLine = "";

        String end =  this.generateNewLineEnd();

        System.out.println("vignette 6 was filled with " + vignetteFilled_6);

        String start = this.generateNewLineStartVignette_6(clientNumber);


        if(this.vignetteFilled_6 == 1){

            newLine = start + end + getV1_1() + ";" + getV1_2() + ";"
                    + getV1_3() + ";" + getV1_4() + ";" + getV1_5()
                    +";" + getV1_6() + ";" + getV1_7() + ";" + getV1_8()
                    + ";" + getV1_9() + ";" + getV1_10() ;

        }
        else if(vignetteFilled_6 == 2){
            newLine = start + end + getV2_1() + ";" + getV2_2() + ";"
                    + getV2_3() + ";" + getV2_4() + ";" + getV2_5()
                    +";" + getV2_6() + ";" + getV2_7() + ";" + getV2_8()
                    + ";" + getV2_9() + ";" + getV2_10() ;
        }
        else if(vignetteFilled_6 == 3){
            newLine = start + end + getV3_1() + ";" + getV3_2() + ";"
                    + getV3_3() + ";" + getV3_4() + ";" + getV3_5()
                    +";" + getV3_6() + ";" + getV3_7() + ";" + getV3_8()
                    + ";" + getV3_9() + ";" + getV3_10() ;
        }
        else if(vignetteFilled_6 == 4){
            newLine = start + end + getV4_1() + ";" + getV4_2() + ";"
                    + getV4_3() + ";" + getV4_4() + ";" + getV4_5()
                    +";" + getV4_6() + ";" + getV4_7() + ";" + getV4_8()
                    + ";" + getV4_9() + ";" + getV4_10() ;
        }
        else if(vignetteFilled_6 == 5){
            newLine = start + end + getV5_1() + ";" + getV5_2() + ";"
                    + getV5_3() + ";" + getV5_4() + ";" + getV5_5()
                    +";" + getV5_6() + ";" + getV5_7() + ";" + getV5_8()
                    + ";" + getV5_9() + ";" + getV5_10() ;
        }
        else if(vignetteFilled_6 == 6){
            newLine = start + end + getV6_1() + ";" + getV6_2() + ";"
                    + getV6_3() + ";" + getV6_4() + ";" + getV6_5()
                    +";" + getV6_6() + ";" + getV6_7() + ";" + getV6_8()
                    + ";" + getV6_9() + ";" + getV6_10() ;
        }
        else if(vignetteFilled_6 == 7){
            newLine = start + end + getV7_1() + ";" + getV7_2() + ";"
                    + getV7_3() + ";" + getV7_4() + ";" + getV7_5()
                    +";" + getV7_6() + ";" + getV7_7() + ";" + getV7_8()
                    + ";" + getV7_9() + ";" + getV7_10() ;
        }
        else if(vignetteFilled_6 == 8){
            newLine = start + end + getV8_1() + ";" + getV8_2() + ";"
                    + getV8_3() + ";" + getV8_4() + ";" + getV8_5()
                    +";" + getV8_6() + ";" + getV8_7() + ";" + getV8_8()
                    + ";" + getV8_9() + ";" + getV8_10() ;
        }
        else if(vignetteFilled_6 == 9){
            newLine = start + end + getV9_1() + ";" + getV9_2() + ";"
                    + getV9_3() + ";" + getV9_4() + ";" + getV9_5()
                    +";" + getV9_6() + ";" + getV9_7() + ";" + getV9_8()
                    + ";" + getV9_9() + ";" + getV9_10() ;
        }
        else if(vignetteFilled_6 == 10){
            newLine = start + end + getV10_1() + ";" + getV10_2() + ";"
                    + getV10_3() + ";" + getV10_4() + ";" + getV10_5()
                    +";" + getV10_6() + ";" + getV10_7() + ";" + getV10_8()
                    + ";" + getV10_9() + ";" + getV10_10() ;
        }


        else if(vignetteFilled_6 == 11){

            newLine = start + end + getV11_1() + ";" + getV11_2() + ";"
                    + getV11_3() + ";" + getV11_4() + ";" + getV11_5()
                    +";" + getV11_6() + ";" + getV11_7() + ";" + getV11_8()
                    + ";" + getV11_9() + ";" + getV11_10() ;

        }
        else if(vignetteFilled_6 == 12){
            newLine = start + end + getV12_1() + ";" + getV12_2() + ";"
                    + getV12_3() + ";" + getV12_4() + ";" + getV12_5()
                    +";" + getV12_6() + ";" + getV12_7() + ";" + getV12_8()
                    + ";" + getV12_9() + ";" + getV12_10() ;
        }
        else if(vignetteFilled_6 == 13){
            newLine = start + end + getV13_1() + ";" + getV13_2() + ";"
                    + getV13_3() + ";" + getV13_4() + ";" + getV13_5()
                    +";" + getV13_6() + ";" + getV13_7() + ";" + getV13_8()
                    + ";" + getV13_9() + ";" + getV13_10() ;
        }
        else if(vignetteFilled_6 == 14){
            newLine = start + end + getV14_1() + ";" + getV14_2() + ";"
                    + getV14_3() + ";" + getV14_4() + ";" + getV14_5()
                    +";" + getV14_6() + ";" + getV14_7() + ";" + getV14_8()
                    + ";" + getV14_9() + ";" + getV14_10() ;
        }
        else if(vignetteFilled_6 == 15){
            newLine = start + end + getV15_1() + ";" + getV15_2() + ";"
                    + getV15_3() + ";" + getV15_4() + ";" + getV15_5()
                    +";" + getV15_6() + ";" + getV15_7() + ";" + getV15_8()
                    + ";" + getV15_9() + ";" + getV15_10() ;
        }
        else if(vignetteFilled_6 == 16){
            newLine = start + end + getV16_1() + ";" + getV16_2() + ";"
                    + getV16_3() + ";" + getV16_4() + ";" + getV16_5()
                    +";" + getV16_6() + ";" + getV16_7() + ";" + getV16_8()
                    + ";" + getV16_9() + ";" + getV16_10() ;
        }
        else if(vignetteFilled_6 == 17){
            newLine = start + end + getV17_1() + ";" + getV17_2() + ";"
                    + getV17_3() + ";" + getV17_4() + ";" + getV17_5()
                    +";" + getV17_6() + ";" + getV17_7() + ";" + getV17_8()
                    + ";" + getV17_9() + ";" + getV17_10() ;
        }
        else if(vignetteFilled_6 == 18){
            newLine = start + end + getV18_1() + ";" + getV18_2() + ";"
                    + getV18_3() + ";" + getV18_4() + ";" + getV18_5()
                    +";" + getV18_6() + ";" + getV18_7() + ";" + getV18_8()
                    + ";" + getV18_9() + ";" + getV18_10() ;
        }
        else if(vignetteFilled_6 == 19){
            newLine = start + end + getV19_1() + ";" + getV19_2() + ";"
                    + getV19_3() + ";" + getV19_4() + ";" + getV19_5()
                    +";" + getV19_6() + ";" + getV19_7() + ";" + getV19_8()
                    + ";" + getV19_9() + ";" + getV19_10() ;
        }



        else if(vignetteFilled_6 == 20){
            newLine = start + end + getV20_1() + ";" + getV20_2() + ";"
                    + getV20_3() + ";" + getV20_4() + ";" + getV20_5()
                    +";" + getV20_6() + ";" + getV20_7() + ";" + getV20_8()
                    + ";" + getV20_9() + ";" + getV20_10() ;
        }
        else if(vignetteFilled_6 == 21){

            newLine = start + end + getV21_1() + ";" + getV21_2() + ";"
                    + getV21_3() + ";" + getV21_4() + ";" + getV21_5()
                    +";" + getV21_6() + ";" + getV21_7() + ";" + getV21_8()
                    + ";" + getV21_9() + ";" + getV21_10() ;

        }
        else if(vignetteFilled_6 == 22){
            newLine = start + end + getV22_1() + ";" + getV22_2() + ";"
                    + getV22_3() + ";" + getV22_4() + ";" + getV22_5()
                    +";" + getV22_6() + ";" + getV22_7() + ";" + getV22_8()
                    + ";" + getV22_9() + ";" + getV22_10() ;
        }
        else if(vignetteFilled_6 == 23){
            newLine = start + end + getV23_1() + ";" + getV23_2() + ";"
                    + getV23_3() + ";" + getV23_4() + ";" + getV23_5()
                    +";" + getV23_6() + ";" + getV23_7() + ";" + getV23_8()
                    + ";" + getV23_9() + ";" + getV23_10() ;
        }
        else if(vignetteFilled_6 == 24){
            newLine = start + end + getV24_1() + ";" + getV24_2() + ";"
                    + getV24_3() + ";" + getV24_4() + ";" + getV24_5()
                    +";" + getV24_6() + ";" + getV24_7() + ";" + getV24_8()
                    + ";" + getV24_9() + ";" + getV24_10() ;
        }
        else if(vignetteFilled_6 == 25){
            newLine = start + end + getV25_1() + ";" + getV25_2() + ";"
                    + getV25_3() + ";" + getV25_4() + ";" + getV25_5()
                    +";" + getV25_6() + ";" + getV25_7() + ";" + getV25_8()
                    + ";" + getV25_9() + ";" + getV25_10() ;
        }
        else if(vignetteFilled_6 == 26){
            newLine = start + end + getV26_1() + ";" + getV26_2() + ";"
                    + getV26_3() + ";" + getV26_4() + ";" + getV26_5()
                    +";" + getV26_6() + ";" + getV26_7() + ";" + getV26_8()
                    + ";" + getV26_9() + ";" + getV26_10() ;
        }
        else if(vignetteFilled_6 == 27){
            newLine = start + end + getV27_1() + ";" + getV27_2() + ";"
                    + getV27_3() + ";" + getV27_4() + ";" + getV27_5()
                    +";" + getV27_6() + ";" + getV27_7() + ";" + getV27_8()
                    + ";" + getV27_9() + ";" + getV27_10() ;
        }
        else if(vignetteFilled_6 == 28){
            newLine = start + end + getV28_1() + ";" + getV28_2() + ";"
                    + getV28_3() + ";" + getV28_4() + ";" + getV28_5()
                    +";" + getV28_6() + ";" + getV28_7() + ";" + getV28_8()
                    + ";" + getV28_9() + ";" + getV28_10() ;
        }
        else if(vignetteFilled_6 == 29){
            newLine = start + end + getV29_1() + ";" + getV29_2() + ";"
                    + getV29_3() + ";" + getV29_4() + ";" + getV29_5()
                    +";" + getV29_6() + ";" + getV29_7() + ";" + getV29_8()
                    + ";" + getV29_9() + ";" + getV29_10() ;
        }


        else if(vignetteFilled_6 == 30){
            newLine = start + end + getV30_1() + ";" + getV30_2() + ";"
                    + getV30_3() + ";" + getV30_4() + ";" + getV30_5()
                    +";" + getV30_6() + ";" + getV30_7() + ";" + getV30_8()
                    + ";" + getV30_9() + ";" + getV30_10() ;
        }
        else if(vignetteFilled_6 == 31){

            newLine = start + end + getV31_1() + ";" + getV31_2() + ";"
                    + getV31_3() + ";" + getV31_4() + ";" + getV31_5()
                    +";" + getV31_6() + ";" + getV31_7() + ";" + getV31_8()
                    + ";" + getV31_9() + ";" + getV31_10() ;

        }
        else if(vignetteFilled_6 == 32){
            newLine = start + end + getV32_1() + ";" + getV32_2() + ";"
                    + getV32_3() + ";" + getV32_4() + ";" + getV32_5()
                    +";" + getV32_6() + ";" + getV32_7() + ";" + getV32_8()
                    + ";" + getV32_9() + ";" + getV32_10() ;
        }
        else if(vignetteFilled_6 == 33){
            newLine = start + end + getV33_1() + ";" + getV33_2() + ";"
                    + getV33_3() + ";" + getV33_4() + ";" + getV33_5()
                    +";" + getV33_6() + ";" + getV33_7() + ";" + getV33_8()
                    + ";" + getV33_9() + ";" + getV33_10() ;
        }
        else if(vignetteFilled_6 == 34){
            newLine = start + end + getV34_1() + ";" + getV34_2() + ";"
                    + getV34_3() + ";" + getV34_4() + ";" + getV34_5()
                    +";" + getV34_6() + ";" + getV34_7() + ";" + getV34_8()
                    + ";" + getV34_9() + ";" + getV34_10() ;
        }
        else if(vignetteFilled_6 == 35){
            newLine = start + end + getV35_1() + ";" + getV35_2() + ";"
                    + getV35_3() + ";" + getV35_4() + ";" + getV35_5()
                    +";" + getV35_6() + ";" + getV35_7() + ";" + getV35_8()
                    + ";" + getV35_9() + ";" + getV35_10() ;
        }
        else if(vignetteFilled_6 == 36){
            newLine = start + end + getV36_1() + ";" + getV36_2() + ";"
                    + getV36_3() + ";" + getV36_4() + ";" + getV36_5()
                    +";" + getV36_6() + ";" + getV36_7() + ";" + getV36_8()
                    + ";" + getV36_9() + ";" + getV36_10() ;
        }
        else if(vignetteFilled_6 == 37){
            newLine = start + end + getV37_1() + ";" + getV37_2() + ";"
                    + getV37_3() + ";" + getV37_4() + ";" + getV37_5()
                    +";" + getV37_6() + ";" + getV37_7() + ";" + getV37_8()
                    + ";" + getV37_9() + ";" + getV37_10() ;
        }
        else if(vignetteFilled_6 == 38){
            newLine = start + end + getV38_1() + ";" + getV38_2() + ";"
                    + getV38_3() + ";" + getV38_4() + ";" + getV38_5()
                    +";" + getV38_6() + ";" + getV38_7() + ";" + getV38_8()
                    + ";" + getV38_9() + ";" + getV38_10() ;
        }
        else if(vignetteFilled_6 == 39){
            newLine = start + end + getV39_1() + ";" + getV39_2() + ";"
                    + getV39_3() + ";" + getV39_4() + ";" + getV39_5()
                    +";" + getV39_6() + ";" + getV39_7() + ";" + getV39_8()
                    + ";" + getV39_9() + ";" + getV39_10() ;
        }

        else if(vignetteFilled_6 == 40){
            newLine = start + end + getV40_1() + ";" + getV40_2() + ";"
                    + getV40_3() + ";" + getV40_4() + ";" + getV40_5()
                    +";" + getV40_6() + ";" + getV40_7() + ";" + getV40_8()
                    + ";" + getV40_9() + ";" + getV40_10() ;
        }
        else if(vignetteFilled_6 == 41){

            newLine = start + end + getV41_1() + ";" + getV41_2() + ";"
                    + getV41_3() + ";" + getV41_4() + ";" + getV41_5()
                    +";" + getV41_6() + ";" + getV41_7() + ";" + getV41_8()
                    + ";" + getV41_9() + ";" + getV41_10() ;

        }
        else if(vignetteFilled_6 == 42){
            newLine = start + end + getV42_1() + ";" + getV42_2() + ";"
                    + getV42_3() + ";" + getV42_4() + ";" + getV42_5()
                    +";" + getV42_6() + ";" + getV42_7() + ";" + getV42_8()
                    + ";" + getV42_9() + ";" + getV42_10() ;
        }
        else if(vignetteFilled_6 == 43){
            newLine = start + end + getV43_1() + ";" + getV43_2() + ";"
                    + getV43_3() + ";" + getV43_4() + ";" + getV43_5()
                    +";" + getV43_6() + ";" + getV43_7() + ";" + getV43_8()
                    + ";" + getV43_9() + ";" + getV43_10() ;
        }
        else if(vignetteFilled_6 == 44){
            newLine = start + end + getV44_1() + ";" + getV44_2() + ";"
                    + getV44_3() + ";" + getV44_4() + ";" + getV44_5()
                    +";" + getV44_6() + ";" + getV44_7() + ";" + getV44_8()
                    + ";" + getV44_9() + ";" + getV44_10() ;
        }
        else if(vignetteFilled_6 == 45){
            newLine = start + end + getV45_1() + ";" + getV45_2() + ";"
                    + getV45_3() + ";" + getV45_4() + ";" + getV45_5()
                    +";" + getV45_6() + ";" + getV45_7() + ";" + getV45_8()
                    + ";" + getV45_9() + ";" + getV45_10() ;
        }
        else if(vignetteFilled_6 == 46){
            newLine = start + end + getV46_1() + ";" + getV46_2() + ";"
                    + getV46_3() + ";" + getV46_4() + ";" + getV46_5()
                    +";" + getV46_6() + ";" + getV46_7() + ";" + getV46_8()
                    + ";" + getV46_9() + ";" + getV46_10() ;
        }
        else if(vignetteFilled_6 == 47){
            newLine = start + end + getV47_1() + ";" + getV47_2() + ";"
                    + getV47_3() + ";" + getV47_4() + ";" + getV47_5()
                    +";" + getV47_6() + ";" + getV47_7() + ";" + getV47_8()
                    + ";" + getV47_9() + ";" + getV47_10() ;
        }
        else if(vignetteFilled_6 == 48){
            newLine = start + end + getV48_1() + ";" + getV48_2() + ";"
                    + getV48_3() + ";" + getV48_4() + ";" + getV48_5()
                    +";" + getV48_6() + ";" + getV48_7() + ";" + getV48_8()
                    + ";" + getV48_9() + ";" + getV48_10() ;
        }
        else if(vignetteFilled_6 == 49){
            newLine = start + end + getV49_1() + ";" + getV49_2() + ";"
                    + getV49_3() + ";" + getV49_4() + ";" + getV49_5()
                    +";" + getV49_6() + ";" + getV49_7() + ";" + getV49_8()
                    + ";" + getV49_9() + ";" + getV49_10() ;
        }


        else if(vignetteFilled_6 == 50){
            newLine = start + end + getV50_1() + ";" + getV50_2() + ";"
                    + getV50_3() + ";" + getV50_4() + ";" + getV50_5()
                    +";" + getV50_6() + ";" + getV50_7() + ";" + getV50_8()
                    + ";" + getV50_9() + ";" + getV50_10() ;
        }
        else if(vignetteFilled_6 == 51){

            newLine = start + end + getV51_1() + ";" + getV51_2() + ";"
                    + getV51_3() + ";" + getV51_4() + ";" + getV51_5()
                    +";" + getV51_6() + ";" + getV51_7() + ";" + getV51_8()
                    + ";" + getV51_9() + ";" + getV51_10() ;

        }
        else if(vignetteFilled_6 == 52){
            newLine = start + end + getV52_1() + ";" + getV52_2() + ";"
                    + getV52_3() + ";" + getV52_4() + ";" + getV52_5()
                    +";" + getV52_6() + ";" + getV52_7() + ";" + getV52_8()
                    + ";" + getV52_9() + ";" + getV52_10() ;
        }
        else if(vignetteFilled_6 == 53){
            newLine = start + end + getV53_1() + ";" + getV53_2() + ";"
                    + getV53_3() + ";" + getV53_4() + ";" + getV53_5()
                    +";" + getV53_6() + ";" + getV53_7() + ";" + getV53_8()
                    + ";" + getV53_9() + ";" + getV53_10() ;
        }
        else if(vignetteFilled_6 == 54){
            newLine = start + end + getV54_1() + ";" + getV54_2() + ";"
                    + getV54_3() + ";" + getV54_4() + ";" + getV54_5()
                    +";" + getV54_6() + ";" + getV54_7() + ";" + getV54_8()
                    + ";" + getV54_9() + ";" + getV54_10() ;
        }
        else if(vignetteFilled_6 == 55){
            newLine = start + end + getV55_1() + ";" + getV55_2() + ";"
                    + getV55_3() + ";" + getV55_4() + ";" + getV55_5()
                    +";" + getV55_6() + ";" + getV55_7() + ";" + getV55_8()
                    + ";" + getV55_9() + ";" + getV55_10() ;
        }
        else if(vignetteFilled_6 == 56){
            newLine = start + end + getV56_1() + ";" + getV56_2() + ";"
                    + getV56_3() + ";" + getV56_4() + ";" + getV56_5()
                    +";" + getV56_6() + ";" + getV56_7() + ";" + getV56_8()
                    + ";" + getV56_9() + ";" + getV56_10() ;
        }
        else if(vignetteFilled_6 == 57){
            newLine = start + end + getV57_1() + ";" + getV57_2() + ";"
                    + getV57_3() + ";" + getV57_4() + ";" + getV57_5()
                    +";" + getV57_6() + ";" + getV57_7() + ";" + getV57_8()
                    + ";" + getV57_9() + ";" + getV57_10() ;
        }
        else if(vignetteFilled_6 == 58){
            newLine = start + end + getV58_1() + ";" + getV58_2() + ";"
                    + getV58_3() + ";" + getV58_4() + ";" + getV58_5()
                    +";" + getV58_6() + ";" + getV58_7() + ";" + getV58_8()
                    + ";" + getV58_9() + ";" + getV58_10() ;
        }
        else if(vignetteFilled_6 == 59){
            newLine = start + end + getV59_1() + ";" + getV59_2() + ";"
                    + getV59_3() + ";" + getV59_4() + ";" + getV59_5()
                    +";" + getV59_6() + ";" + getV59_7() + ";" + getV59_8()
                    + ";" + getV59_9() + ";" + getV59_10() ;
        }

        else if(vignetteFilled_6 == 60){
            newLine = start + end + getV60_1() + ";" + getV60_2() + ";"
                    + getV60_3() + ";" + getV60_4() + ";" + getV60_5()
                    +";" + getV60_6() + ";" + getV60_7() + ";" + getV60_8()
                    + ";" + getV60_9() + ";" + getV60_10() ;
        }
        else if(vignetteFilled_6 == 61){

            newLine = start + end + getV61_1() + ";" + getV61_2() + ";"
                    + getV61_3() + ";" + getV61_4() + ";" + getV61_5()
                    +";" + getV61_6() + ";" + getV61_7() + ";" + getV61_8()
                    + ";" + getV61_9() + ";" + getV61_10() ;

        }
        else if(vignetteFilled_6 == 62){
            newLine = start + end + getV62_1() + ";" + getV62_2() + ";"
                    + getV62_3() + ";" + getV62_4() + ";" + getV62_5()
                    +";" + getV62_6() + ";" + getV62_7() + ";" + getV62_8()
                    + ";" + getV62_9() + ";" + getV62_10() ;
        }
        else if(vignetteFilled_6 == 63){
            newLine = start + end + getV63_1() + ";" + getV63_2() + ";"
                    + getV63_3() + ";" + getV63_4() + ";" + getV63_5()
                    +";" + getV63_6() + ";" + getV63_7() + ";" + getV63_8()
                    + ";" + getV63_9() + ";" + getV63_10() ;
        }
        else if(vignetteFilled_6 == 64){
            newLine = start + end + getV64_1() + ";" + getV64_2() + ";"
                    + getV64_3() + ";" + getV64_4() + ";" + getV64_5()
                    +";" + getV64_6() + ";" + getV64_7() + ";" + getV64_8()
                    + ";" + getV64_9() + ";" + getV64_10() ;
        }

        else{
            throw new Error("No vignette one filled, impossible");
        }


        System.out.println("Resulting in new line : " + newLine );
        return newLine;

    }

}
