package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

public class Main {

    public static void main(String[] args) throws IOException {


        PrintWriter writer = new PrintWriter("C:\\Users\\domien.frederix\\Desktop\\onderElkaar.csv", "UTF-8");
        writer.println("volgNummer;persoonNummer;vignetteNummer;TRANSPARENCY;BASIS;TYPE;ETHICS;SEX;AGE;TENORG;TENLG;FUNCTIE;DIPL;MEDL;LMX_1;LMX_2;LMX_3;LMX_4;LMX_5;LMX_6;LMX_7;MANCHECK_1;MANCHECK_2;MANCHECK_3;MANCHECK_4;MANCHECK_5;MANCHECK_6;MANCHECK_7;MANCHECK_8;Vign_vraag_1;Vign_vraag_2;Vign_vraag_3;Vign_vraag_4;Vign_vraag_5;Vign_vraag_6;Vign_vraag_7;Vign_vraag_8;Vign_vraag_9;Vign_vraag_10;");


        parseCsv("C:\\Users\\domien.frederix\\Desktop\\V2.csv", writer);
        writer.close();


    }


    public static void parseCsv(String filePath, PrintWriter writer) throws IOException {

        BufferedReader csvReader = new BufferedReader(new FileReader(filePath));
        String row;
        int index = 0;
        // && index < 10


        while ((row = csvReader.readLine()) != null) {
            String[] data = row.split(";");

            String line = "";

            CsvLine csvLine = new CsvLine();
            csvLine.setVignetteFilled_1(0);
            csvLine.setVignetteFilled_2(0);
            csvLine.setVignetteFilled_3(0);
            csvLine.setVignetteFilled_4(0);
            csvLine.setVignetteFilled_5(0);
            csvLine.setVignetteFilled_6(0);

            csvLine.setSEX((data[0]));
            csvLine.setAGE((data[1]));
            csvLine.setTENORG((data[2]));
            csvLine.setTENLG((data[3]));
            csvLine.setFUNCTIE((data[4]));
            csvLine.setDIPL((data[5]));

                System.out.println("printing SEX : " + csvLine.getSEX());
                System.out.println("printing AGE : " + csvLine.getAGE());
                System.out.println("printing TENORG : " + csvLine.getTENORG());
                System.out.println("printing TENLG : " + csvLine.getTENLG());
                System.out.println("printing FUNCTIE : " + csvLine.getFUNCTIE());
                System.out.println("printing DIPL : " + csvLine.getDIPL());


            csvLine.setMEDL((data[646]));
            csvLine.setLMX_1((data[647]));
            csvLine.setLMX_2((data[648]));
            csvLine.setLMX_3((data[649]));
            csvLine.setLMX_4((data[650]));
            csvLine.setLMX_5((data[651]));
            csvLine.setLMX_6((data[652]));
            csvLine.setLMX_7((data[653]));
            csvLine.setMANCHECK_1((data[654]));
            csvLine.setMANCHECK_2((data[655]));
            csvLine.setMANCHECK_3((data[656]));
            csvLine.setMANCHECK_4((data[657]));
            csvLine.setMANCHECK_5((data[658]));
            csvLine.setMANCHECK_6((data[659]));
            csvLine.setMANCHECK_7((data[660]));
            csvLine.setMANCHECK_8((data[661]));



            System.out.println("printing MEDL : " + csvLine.getMEDL());
            System.out.println("printing LMX_1 : " + csvLine.getLMX_1());
            System.out.println("printing LMX_2 : " + csvLine.getLMX_2());
            System.out.println("printing LMX_3 : " + csvLine.getLMX_3());
            System.out.println("printing LMX_4 : " + csvLine.getLMX_4());
            System.out.println("printing LMX_5 : " + csvLine.getLMX_5());
            System.out.println("printing LMX_6 : " + csvLine.getLMX_6());
            System.out.println("printing LMX_7 : " + csvLine.getLMX_7());

            System.out.println("printing MANCHECK_1 : " + csvLine.getMANCHECK_1());
            System.out.println("printing MANCHECK_2 : " + csvLine.getMANCHECK_2());
            System.out.println("printing MANCHECK_3 : " + csvLine.getMANCHECK_3());
            System.out.println("printing MANCHECK_4 : " + csvLine.getMANCHECK_4());
            System.out.println("printing MANCHECK_5 : " + csvLine.getMANCHECK_5());
            System.out.println("printing MANCHECK_6 : " + csvLine.getMANCHECK_6());
            System.out.println("printing MANCHECK_7 : " + csvLine.getMANCHECK_7());
            System.out.println("printing MANCHECK_8 : " + csvLine.getMANCHECK_8());



            int i = 5;
            i++;
            csvLine.setV1_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 1);

            i++;
            csvLine.setV1_2(data[i]);
            i++;
            csvLine.setV1_3(data[i]);
            i++;
            csvLine.setV1_4(data[i]);
            i++;
            csvLine.setV1_5(data[i]);
            i++;
            csvLine.setV1_6(data[i]);
            i++;
            csvLine.setV1_7(data[i]);
            i++;
            csvLine.setV1_8(data[i]);
            i++;
            csvLine.setV1_9(data[i]);
            i++;
            csvLine.setV1_10(data[i]);

            System.out.println("printing V1_1 : " + csvLine.getV1_1());
            System.out.println("printing V1_2 : " + csvLine.getV1_2());
            System.out.println("printing V1_3 : " + csvLine.getV1_3());
            System.out.println("printing V1_4 : " + csvLine.getV1_4());
            System.out.println("printing V1_5 : " + csvLine.getV1_5());
            System.out.println("printing V1_6 : " + csvLine.getV1_6());
            System.out.println("printing V1_7 : " + csvLine.getV1_7());
            System.out.println("printing V1_8 : " + csvLine.getV1_8());
            System.out.println("printing V1_9 : " + csvLine.getV1_9());
            System.out.println("printing V1_10 : " + csvLine.getV1_10());


            i++;
            csvLine.setV2_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 2);

            i++;
            csvLine.setV2_2(data[i]);
            i++;
            csvLine.setV2_3(data[i]);
            i++;
            csvLine.setV2_4(data[i]);
            i++;
            csvLine.setV2_5(data[i]);
            i++;
            csvLine.setV2_6(data[i]);
            i++;
            csvLine.setV2_7(data[i]);
            i++;
            csvLine.setV2_8(data[i]);
            i++;
            csvLine.setV2_9(data[i]);
            i++;
            csvLine.setV2_10(data[i]);

            System.out.println("printing V2_1 : " + csvLine.getV2_1());
            System.out.println("printing V2_2 : " + csvLine.getV2_2());
            System.out.println("printing V2_3 : " + csvLine.getV2_3());
            System.out.println("printing V2_4 : " + csvLine.getV2_4());
            System.out.println("printing V2_5 : " + csvLine.getV2_5());
            System.out.println("printing V2_6 : " + csvLine.getV2_6());
            System.out.println("printing V2_7 : " + csvLine.getV2_7());
            System.out.println("printing V2_8 : " + csvLine.getV2_8());
            System.out.println("printing V2_9 : " + csvLine.getV2_9());
            System.out.println("printing V2_10 : " + csvLine.getV2_10());

            i++;
            csvLine.setV3_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 3);

            i++;
            csvLine.setV3_2(data[i]);
            i++;
            csvLine.setV3_3(data[i]);
            i++;
            csvLine.setV3_4(data[i]);
            i++;
            csvLine.setV3_5(data[i]);
            i++;
            csvLine.setV3_6(data[i]);
            i++;
            csvLine.setV3_7(data[i]);
            i++;
            csvLine.setV3_8(data[i]);
            i++;
            csvLine.setV3_9(data[i]);
            i++;
            csvLine.setV3_10(data[i]);

            System.out.println("printing V3_1 : " + csvLine.getV3_1());
            System.out.println("printing V3_2 : " + csvLine.getV3_2());
            System.out.println("printing V3_3 : " + csvLine.getV3_3());
            System.out.println("printing V3_4 : " + csvLine.getV3_4());
            System.out.println("printing V3_5 : " + csvLine.getV3_5());
            System.out.println("printing V3_6 : " + csvLine.getV3_6());
            System.out.println("printing V3_7 : " + csvLine.getV3_7());
            System.out.println("printing V3_8 : " + csvLine.getV3_8());
            System.out.println("printing V3_9 : " + csvLine.getV3_9());
            System.out.println("printing V3_10 : " + csvLine.getV3_10());

            i++;
            csvLine.setV4_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 4);

            i++;
            csvLine.setV4_2(data[i]);
            i++;
            csvLine.setV4_3(data[i]);
            i++;
            csvLine.setV4_4(data[i]);
            i++;
            csvLine.setV4_5(data[i]);
            i++;
            csvLine.setV4_6(data[i]);
            i++;
            csvLine.setV4_7(data[i]);
            i++;
            csvLine.setV4_8(data[i]);
            i++;
            csvLine.setV4_9(data[i]);
            i++;
            csvLine.setV4_10(data[i]);

            System.out.println("printing V4_1 : " + csvLine.getV4_1());
            System.out.println("printing V4_2 : " + csvLine.getV4_2());
            System.out.println("printing V4_3 : " + csvLine.getV4_3());
            System.out.println("printing V4_4 : " + csvLine.getV4_4());
            System.out.println("printing V4_5 : " + csvLine.getV4_5());
            System.out.println("printing V4_6 : " + csvLine.getV4_6());
            System.out.println("printing V4_7 : " + csvLine.getV4_7());
            System.out.println("printing V4_8 : " + csvLine.getV4_8());
            System.out.println("printing V4_9 : " + csvLine.getV4_9());
            System.out.println("printing V4_10 : " + csvLine.getV4_10());

            i++;
            csvLine.setV5_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 5);

            i++;
            csvLine.setV5_2(data[i]);
            i++;
            csvLine.setV5_3(data[i]);
            i++;
            csvLine.setV5_4(data[i]);
            i++;
            csvLine.setV5_5(data[i]);
            i++;
            csvLine.setV5_6(data[i]);
            i++;
            csvLine.setV5_7(data[i]);
            i++;
            csvLine.setV5_8(data[i]);
            i++;
            csvLine.setV5_9(data[i]);
            i++;
            csvLine.setV5_10(data[i]);

            System.out.println("printing V5_1 : " + csvLine.getV5_1());
            System.out.println("printing V5_2 : " + csvLine.getV5_2());
            System.out.println("printing V5_3 : " + csvLine.getV5_3());
            System.out.println("printing V5_4 : " + csvLine.getV5_4());
            System.out.println("printing V5_5 : " + csvLine.getV5_5());
            System.out.println("printing V5_6 : " + csvLine.getV5_6());
            System.out.println("printing V5_7 : " + csvLine.getV5_7());
            System.out.println("printing V5_8 : " + csvLine.getV5_8());
            System.out.println("printing V5_9 : " + csvLine.getV5_9());
            System.out.println("printing V5_10 : " + csvLine.getV5_10());

            i++;
            csvLine.setV6_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 6);

            i++;
            csvLine.setV6_2(data[i]);
            i++;
            csvLine.setV6_3(data[i]);
            i++;
            csvLine.setV6_4(data[i]);
            i++;
            csvLine.setV6_5(data[i]);
            i++;
            csvLine.setV6_6(data[i]);
            i++;
            csvLine.setV6_7(data[i]);
            i++;
            csvLine.setV6_8(data[i]);
            i++;
            csvLine.setV6_9(data[i]);
            i++;
            csvLine.setV6_10(data[i]);

            System.out.println("printing V6_1 : " + csvLine.getV6_1());
            System.out.println("printing V6_2 : " + csvLine.getV6_2());
            System.out.println("printing V6_3 : " + csvLine.getV6_3());
            System.out.println("printing V6_4 : " + csvLine.getV6_4());
            System.out.println("printing V6_5 : " + csvLine.getV6_5());
            System.out.println("printing V6_6 : " + csvLine.getV6_6());
            System.out.println("printing V6_7 : " + csvLine.getV6_7());
            System.out.println("printing V6_8 : " + csvLine.getV6_8());
            System.out.println("printing V6_9 : " + csvLine.getV6_9());
            System.out.println("printing V6_10 : " + csvLine.getV6_10());

            i++;
            csvLine.setV7_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 7);

            i++;
            csvLine.setV7_2(data[i]);
            i++;
            csvLine.setV7_3(data[i]);
            i++;
            csvLine.setV7_4(data[i]);
            i++;
            csvLine.setV7_5(data[i]);
            i++;
            csvLine.setV7_6(data[i]);
            i++;
            csvLine.setV7_7(data[i]);
            i++;
            csvLine.setV7_8(data[i]);
            i++;
            csvLine.setV7_9(data[i]);
            i++;
            csvLine.setV7_10(data[i]);

            System.out.println("printing V7_1 : " + csvLine.getV7_1());
            System.out.println("printing V7_2 : " + csvLine.getV7_2());
            System.out.println("printing V7_3 : " + csvLine.getV7_3());
            System.out.println("printing V7_4 : " + csvLine.getV7_4());
            System.out.println("printing V7_5 : " + csvLine.getV7_5());
            System.out.println("printing V7_6 : " + csvLine.getV7_6());
            System.out.println("printing V7_7 : " + csvLine.getV7_7());
            System.out.println("printing V7_8 : " + csvLine.getV7_8());
            System.out.println("printing V7_9 : " + csvLine.getV7_9());
            System.out.println("printing V7_10 : " + csvLine.getV7_10());


            i++;
            csvLine.setV8_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 8);

            i++;
            csvLine.setV8_2(data[i]);
            i++;
            csvLine.setV8_3(data[i]);
            i++;
            csvLine.setV8_4(data[i]);
            i++;
            csvLine.setV8_5(data[i]);
            i++;
            csvLine.setV8_6(data[i]);
            i++;
            csvLine.setV8_7(data[i]);
            i++;
            csvLine.setV8_8(data[i]);
            i++;
            csvLine.setV8_9(data[i]);
            i++;
            csvLine.setV8_10(data[i]);

            System.out.println("printing V8_1 : " + csvLine.getV8_1());
            System.out.println("printing V8_2 : " + csvLine.getV8_2());
            System.out.println("printing V8_3 : " + csvLine.getV8_3());
            System.out.println("printing V8_4 : " + csvLine.getV8_4());
            System.out.println("printing V8_5 : " + csvLine.getV8_5());
            System.out.println("printing V8_6 : " + csvLine.getV8_6());
            System.out.println("printing V8_7 : " + csvLine.getV8_7());
            System.out.println("printing V8_8 : " + csvLine.getV8_8());
            System.out.println("printing V8_9 : " + csvLine.getV8_9());
            System.out.println("printing V8_10 : " + csvLine.getV8_10());

            i++;
            csvLine.setV9_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 9);

            i++;
            csvLine.setV9_2(data[i]);
            i++;
            csvLine.setV9_3(data[i]);
            i++;
            csvLine.setV9_4(data[i]);
            i++;
            csvLine.setV9_5(data[i]);
            i++;
            csvLine.setV9_6(data[i]);
            i++;
            csvLine.setV9_7(data[i]);
            i++;
            csvLine.setV9_8(data[i]);
            i++;
            csvLine.setV9_9(data[i]);
            i++;
            csvLine.setV9_10(data[i]);

            System.out.println("printing V9_1 : " + csvLine.getV9_1());
            System.out.println("printing V9_2 : " + csvLine.getV9_2());
            System.out.println("printing V9_3 : " + csvLine.getV9_3());
            System.out.println("printing V9_4 : " + csvLine.getV9_4());
            System.out.println("printing V9_5 : " + csvLine.getV9_5());
            System.out.println("printing V9_6 : " + csvLine.getV9_6());
            System.out.println("printing V9_7 : " + csvLine.getV9_7());
            System.out.println("printing V9_8 : " + csvLine.getV9_8());
            System.out.println("printing V9_9 : " + csvLine.getV9_9());
            System.out.println("printing V9_10 : " + csvLine.getV9_10());

            i++;
            csvLine.setV10_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 10);

            i++;
            csvLine.setV10_2(data[i]);
            i++;
            csvLine.setV10_3(data[i]);
            i++;
            csvLine.setV10_4(data[i]);
            i++;
            csvLine.setV10_5(data[i]);
            i++;
            csvLine.setV10_6(data[i]);
            i++;
            csvLine.setV10_7(data[i]);
            i++;
            csvLine.setV10_8(data[i]);
            i++;
            csvLine.setV10_9(data[i]);
            i++;
            csvLine.setV10_10(data[i]);

            System.out.println("printing V10_1 : " + csvLine.getV10_1());
            System.out.println("printing V10_2 : " + csvLine.getV10_2());
            System.out.println("printing V10_3 : " + csvLine.getV10_3());
            System.out.println("printing V10_4 : " + csvLine.getV10_4());
            System.out.println("printing V10_5 : " + csvLine.getV10_5());
            System.out.println("printing V10_6 : " + csvLine.getV10_6());
            System.out.println("printing V10_7 : " + csvLine.getV10_7());
            System.out.println("printing V10_8 : " + csvLine.getV10_8());
            System.out.println("printing V10_9 : " + csvLine.getV10_9());
            System.out.println("printing V10_10 : " + csvLine.getV10_10());

            i++;
            csvLine.setV11_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 11);

            i++;
            csvLine.setV11_2(data[i]);
            i++;
            csvLine.setV11_3(data[i]);
            i++;
            csvLine.setV11_4(data[i]);
            i++;
            csvLine.setV11_5(data[i]);
            i++;
            csvLine.setV11_6(data[i]);
            i++;
            csvLine.setV11_7(data[i]);
            i++;
            csvLine.setV11_8(data[i]);
            i++;
            csvLine.setV11_9(data[i]);
            i++;
            csvLine.setV11_10(data[i]);

            System.out.println("printing V11_1 : " + csvLine.getV11_1());
            System.out.println("printing V11_2 : " + csvLine.getV11_2());
            System.out.println("printing V11_3 : " + csvLine.getV11_3());
            System.out.println("printing V11_4 : " + csvLine.getV11_4());
            System.out.println("printing V11_5 : " + csvLine.getV11_5());
            System.out.println("printing V11_6 : " + csvLine.getV11_6());
            System.out.println("printing V11_7 : " + csvLine.getV11_7());
            System.out.println("printing V11_8 : " + csvLine.getV11_8());
            System.out.println("printing V11_9 : " + csvLine.getV11_9());
            System.out.println("printing V11_10 : " + csvLine.getV11_10());

            i++;
            csvLine.setV12_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 12);

            i++;
            csvLine.setV12_2(data[i]);
            i++;
            csvLine.setV12_3(data[i]);
            i++;
            csvLine.setV12_4(data[i]);
            i++;
            csvLine.setV12_5(data[i]);
            i++;
            csvLine.setV12_6(data[i]);
            i++;
            csvLine.setV12_7(data[i]);
            i++;
            csvLine.setV12_8(data[i]);
            i++;
            csvLine.setV12_9(data[i]);
            i++;
            csvLine.setV12_10(data[i]);

            System.out.println("printing V12_1 : " + csvLine.getV12_1());
            System.out.println("printing V12_2 : " + csvLine.getV12_2());
            System.out.println("printing V12_3 : " + csvLine.getV12_3());
            System.out.println("printing V12_4 : " + csvLine.getV12_4());
            System.out.println("printing V12_5 : " + csvLine.getV12_5());
            System.out.println("printing V12_6 : " + csvLine.getV12_6());
            System.out.println("printing V12_7 : " + csvLine.getV12_7());
            System.out.println("printing V12_8 : " + csvLine.getV12_8());
            System.out.println("printing V12_9 : " + csvLine.getV12_9());
            System.out.println("printing V12_10 : " + csvLine.getV12_10());

            i++;
            csvLine.setV13_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 13);

            i++;
            csvLine.setV13_2(data[i]);
            i++;
            csvLine.setV13_3(data[i]);
            i++;
            csvLine.setV13_4(data[i]);
            i++;
            csvLine.setV13_5(data[i]);
            i++;
            csvLine.setV13_6(data[i]);
            i++;
            csvLine.setV13_7(data[i]);
            i++;
            csvLine.setV13_8(data[i]);
            i++;
            csvLine.setV13_9(data[i]);
            i++;
            csvLine.setV13_10(data[i]);

            System.out.println("printing V13_1 : " + csvLine.getV13_1());
            System.out.println("printing V13_2 : " + csvLine.getV13_2());
            System.out.println("printing V13_3 : " + csvLine.getV13_3());
            System.out.println("printing V13_4 : " + csvLine.getV13_4());
            System.out.println("printing V13_5 : " + csvLine.getV13_5());
            System.out.println("printing V13_6 : " + csvLine.getV13_6());
            System.out.println("printing V13_7 : " + csvLine.getV13_7());
            System.out.println("printing V13_8 : " + csvLine.getV13_8());
            System.out.println("printing V13_9 : " + csvLine.getV13_9());
            System.out.println("printing V13_10 : " + csvLine.getV13_10());

            i++;
            csvLine.setV14_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 14);

            i++;
            csvLine.setV14_2(data[i]);
            i++;
            csvLine.setV14_3(data[i]);
            i++;
            csvLine.setV14_4(data[i]);
            i++;
            csvLine.setV14_5(data[i]);
            i++;
            csvLine.setV14_6(data[i]);
            i++;
            csvLine.setV14_7(data[i]);
            i++;
            csvLine.setV14_8(data[i]);
            i++;
            csvLine.setV14_9(data[i]);
            i++;
            csvLine.setV14_10(data[i]);

            System.out.println("printing V14_1 : " + csvLine.getV14_1());
            System.out.println("printing V14_2 : " + csvLine.getV14_2());
            System.out.println("printing V14_3 : " + csvLine.getV14_3());
            System.out.println("printing V14_4 : " + csvLine.getV14_4());
            System.out.println("printing V14_5 : " + csvLine.getV14_5());
            System.out.println("printing V14_6 : " + csvLine.getV14_6());
            System.out.println("printing V14_7 : " + csvLine.getV14_7());
            System.out.println("printing V14_8 : " + csvLine.getV14_8());
            System.out.println("printing V14_9 : " + csvLine.getV14_9());
            System.out.println("printing V14_10 : " + csvLine.getV14_10());

            i++;
            csvLine.setV15_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 15);

            i++;
            csvLine.setV15_2(data[i]);
            i++;
            csvLine.setV15_3(data[i]);
            i++;
            csvLine.setV15_4(data[i]);
            i++;
            csvLine.setV15_5(data[i]);
            i++;
            csvLine.setV15_6(data[i]);
            i++;
            csvLine.setV15_7(data[i]);
            i++;
            csvLine.setV15_8(data[i]);
            i++;
            csvLine.setV15_9(data[i]);
            i++;
            csvLine.setV15_10(data[i]);

            System.out.println("printing V15_1 : " + csvLine.getV15_1());
            System.out.println("printing V15_2 : " + csvLine.getV15_2());
            System.out.println("printing V15_3 : " + csvLine.getV15_3());
            System.out.println("printing V15_4 : " + csvLine.getV15_4());
            System.out.println("printing V15_5 : " + csvLine.getV15_5());
            System.out.println("printing V15_6 : " + csvLine.getV15_6());
            System.out.println("printing V15_7 : " + csvLine.getV15_7());
            System.out.println("printing V15_8 : " + csvLine.getV15_8());
            System.out.println("printing V15_9 : " + csvLine.getV15_9());
            System.out.println("printing V15_10 : " + csvLine.getV15_10());

            i++;
            csvLine.setV16_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 16);

            i++;
            csvLine.setV16_2(data[i]);
            i++;
            csvLine.setV16_3(data[i]);
            i++;
            csvLine.setV16_4(data[i]);
            i++;
            csvLine.setV16_5(data[i]);
            i++;
            csvLine.setV16_6(data[i]);
            i++;
            csvLine.setV16_7(data[i]);
            i++;
            csvLine.setV16_8(data[i]);
            i++;
            csvLine.setV16_9(data[i]);
            i++;
            csvLine.setV16_10(data[i]);

            System.out.println("printing V16_1 : " + csvLine.getV16_1());
            System.out.println("printing V16_2 : " + csvLine.getV16_2());
            System.out.println("printing V16_3 : " + csvLine.getV16_3());
            System.out.println("printing V16_4 : " + csvLine.getV16_4());
            System.out.println("printing V16_5 : " + csvLine.getV16_5());
            System.out.println("printing V16_6 : " + csvLine.getV16_6());
            System.out.println("printing V16_7 : " + csvLine.getV16_7());
            System.out.println("printing V16_8 : " + csvLine.getV16_8());
            System.out.println("printing V16_9 : " + csvLine.getV16_9());
            System.out.println("printing V16_10 : " + csvLine.getV16_10());

            i++;
            csvLine.setV17_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 17);

            i++;
            csvLine.setV17_2(data[i]);
            i++;
            csvLine.setV17_3(data[i]);
            i++;
            csvLine.setV17_4(data[i]);
            i++;
            csvLine.setV17_5(data[i]);
            i++;
            csvLine.setV17_6(data[i]);
            i++;
            csvLine.setV17_7(data[i]);
            i++;
            csvLine.setV17_8(data[i]);
            i++;
            csvLine.setV17_9(data[i]);
            i++;
            csvLine.setV17_10(data[i]);

            System.out.println("printing V17_1 : " + csvLine.getV17_1());
            System.out.println("printing V17_2 : " + csvLine.getV17_2());
            System.out.println("printing V17_3 : " + csvLine.getV17_3());
            System.out.println("printing V17_4 : " + csvLine.getV17_4());
            System.out.println("printing V17_5 : " + csvLine.getV17_5());
            System.out.println("printing V17_6 : " + csvLine.getV17_6());
            System.out.println("printing V17_7 : " + csvLine.getV17_7());
            System.out.println("printing V17_8 : " + csvLine.getV17_8());
            System.out.println("printing V17_9 : " + csvLine.getV17_9());
            System.out.println("printing V17_10 : " + csvLine.getV17_10());


            i++;
            csvLine.setV18_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 18);

            i++;
            csvLine.setV18_2(data[i]);
            i++;
            csvLine.setV18_3(data[i]);
            i++;
            csvLine.setV18_4(data[i]);
            i++;
            csvLine.setV18_5(data[i]);
            i++;
            csvLine.setV18_6(data[i]);
            i++;
            csvLine.setV18_7(data[i]);
            i++;
            csvLine.setV18_8(data[i]);
            i++;
            csvLine.setV18_9(data[i]);
            i++;
            csvLine.setV18_10(data[i]);

            System.out.println("printing V18_1 : " + csvLine.getV18_1());
            System.out.println("printing V18_2 : " + csvLine.getV18_2());
            System.out.println("printing V18_3 : " + csvLine.getV18_3());
            System.out.println("printing V18_4 : " + csvLine.getV18_4());
            System.out.println("printing V18_5 : " + csvLine.getV18_5());
            System.out.println("printing V18_6 : " + csvLine.getV18_6());
            System.out.println("printing V18_7 : " + csvLine.getV18_7());
            System.out.println("printing V18_8 : " + csvLine.getV18_8());
            System.out.println("printing V18_9 : " + csvLine.getV18_9());
            System.out.println("printing V18_10 : " + csvLine.getV18_10());

            i++;
            csvLine.setV19_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 19);

            i++;
            csvLine.setV19_2(data[i]);
            i++;
            csvLine.setV19_3(data[i]);
            i++;
            csvLine.setV19_4(data[i]);
            i++;
            csvLine.setV19_5(data[i]);
            i++;
            csvLine.setV19_6(data[i]);
            i++;
            csvLine.setV19_7(data[i]);
            i++;
            csvLine.setV19_8(data[i]);
            i++;
            csvLine.setV19_9(data[i]);
            i++;
            csvLine.setV19_10(data[i]);

            System.out.println("printing V19_1 : " + csvLine.getV19_1());
            System.out.println("printing V19_2 : " + csvLine.getV19_2());
            System.out.println("printing V19_3 : " + csvLine.getV19_3());
            System.out.println("printing V19_4 : " + csvLine.getV19_4());
            System.out.println("printing V19_5 : " + csvLine.getV19_5());
            System.out.println("printing V19_6 : " + csvLine.getV19_6());
            System.out.println("printing V19_7 : " + csvLine.getV19_7());
            System.out.println("printing V19_8 : " + csvLine.getV19_8());
            System.out.println("printing V19_9 : " + csvLine.getV19_9());
            System.out.println("printing V19_10 : " + csvLine.getV19_10());

            i++;
            csvLine.setV20_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 20);

            i++;
            csvLine.setV20_2(data[i]);
            i++;
            csvLine.setV20_3(data[i]);
            i++;
            csvLine.setV20_4(data[i]);
            i++;
            csvLine.setV20_5(data[i]);
            i++;
            csvLine.setV20_6(data[i]);
            i++;
            csvLine.setV20_7(data[i]);
            i++;
            csvLine.setV20_8(data[i]);
            i++;
            csvLine.setV20_9(data[i]);
            i++;
            csvLine.setV20_10(data[i]);

            System.out.println("printing V20_1 : " + csvLine.getV20_1());
            System.out.println("printing V20_2 : " + csvLine.getV20_2());
            System.out.println("printing V20_3 : " + csvLine.getV20_3());
            System.out.println("printing V20_4 : " + csvLine.getV20_4());
            System.out.println("printing V20_5 : " + csvLine.getV20_5());
            System.out.println("printing V20_6 : " + csvLine.getV20_6());
            System.out.println("printing V20_7 : " + csvLine.getV20_7());
            System.out.println("printing V20_8 : " + csvLine.getV20_8());
            System.out.println("printing V20_9 : " + csvLine.getV20_9());
            System.out.println("printing V20_10 : " + csvLine.getV20_10());

            i++;
            csvLine.setV21_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 21);

            i++;
            csvLine.setV21_2(data[i]);
            i++;
            csvLine.setV21_3(data[i]);
            i++;
            csvLine.setV21_4(data[i]);
            i++;
            csvLine.setV21_5(data[i]);
            i++;
            csvLine.setV21_6(data[i]);
            i++;
            csvLine.setV21_7(data[i]);
            i++;
            csvLine.setV21_8(data[i]);
            i++;
            csvLine.setV21_9(data[i]);
            i++;
            csvLine.setV21_10(data[i]);

            System.out.println("printing V21_1 : " + csvLine.getV21_1());
            System.out.println("printing V21_2 : " + csvLine.getV21_2());
            System.out.println("printing V21_3 : " + csvLine.getV21_3());
            System.out.println("printing V21_4 : " + csvLine.getV21_4());
            System.out.println("printing V21_5 : " + csvLine.getV21_5());
            System.out.println("printing V21_6 : " + csvLine.getV21_6());
            System.out.println("printing V21_7 : " + csvLine.getV21_7());
            System.out.println("printing V21_8 : " + csvLine.getV21_8());
            System.out.println("printing V21_9 : " + csvLine.getV21_9());
            System.out.println("printing V21_10 : " + csvLine.getV21_10());

            i++;
            csvLine.setV22_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 22);

            i++;
            csvLine.setV22_2(data[i]);
            i++;
            csvLine.setV22_3(data[i]);
            i++;
            csvLine.setV22_4(data[i]);
            i++;
            csvLine.setV22_5(data[i]);
            i++;
            csvLine.setV22_6(data[i]);
            i++;
            csvLine.setV22_7(data[i]);
            i++;
            csvLine.setV22_8(data[i]);
            i++;
            csvLine.setV22_9(data[i]);
            i++;
            csvLine.setV22_10(data[i]);

            System.out.println("printing V22_1 : " + csvLine.getV22_1());
            System.out.println("printing V22_2 : " + csvLine.getV22_2());
            System.out.println("printing V22_3 : " + csvLine.getV22_3());
            System.out.println("printing V22_4 : " + csvLine.getV22_4());
            System.out.println("printing V22_5 : " + csvLine.getV22_5());
            System.out.println("printing V22_6 : " + csvLine.getV22_6());
            System.out.println("printing V22_7 : " + csvLine.getV22_7());
            System.out.println("printing V22_8 : " + csvLine.getV22_8());
            System.out.println("printing V22_9 : " + csvLine.getV22_9());
            System.out.println("printing V22_10 : " + csvLine.getV22_10());

            i++;
            csvLine.setV23_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 23);

            i++;
            csvLine.setV23_2(data[i]);
            i++;
            csvLine.setV23_3(data[i]);
            i++;
            csvLine.setV23_4(data[i]);
            i++;
            csvLine.setV23_5(data[i]);
            i++;
            csvLine.setV23_6(data[i]);
            i++;
            csvLine.setV23_7(data[i]);
            i++;
            csvLine.setV23_8(data[i]);
            i++;
            csvLine.setV23_9(data[i]);
            i++;
            csvLine.setV23_10(data[i]);

            System.out.println("printing V23_1 : " + csvLine.getV23_1());
            System.out.println("printing V23_2 : " + csvLine.getV23_2());
            System.out.println("printing V23_3 : " + csvLine.getV23_3());
            System.out.println("printing V23_4 : " + csvLine.getV23_4());
            System.out.println("printing V23_5 : " + csvLine.getV23_5());
            System.out.println("printing V23_6 : " + csvLine.getV23_6());
            System.out.println("printing V23_7 : " + csvLine.getV23_7());
            System.out.println("printing V23_8 : " + csvLine.getV23_8());
            System.out.println("printing V23_9 : " + csvLine.getV23_9());
            System.out.println("printing V23_10 : " + csvLine.getV23_10());

            i++;
            csvLine.setV24_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 24);

            i++;
            csvLine.setV24_2(data[i]);
            i++;
            csvLine.setV24_3(data[i]);
            i++;
            csvLine.setV24_4(data[i]);
            i++;
            csvLine.setV24_5(data[i]);
            i++;
            csvLine.setV24_6(data[i]);
            i++;
            csvLine.setV24_7(data[i]);
            i++;
            csvLine.setV24_8(data[i]);
            i++;
            csvLine.setV24_9(data[i]);
            i++;
            csvLine.setV24_10(data[i]);

            System.out.println("printing V24_1 : " + csvLine.getV24_1());
            System.out.println("printing V24_2 : " + csvLine.getV24_2());
            System.out.println("printing V24_3 : " + csvLine.getV24_3());
            System.out.println("printing V24_4 : " + csvLine.getV24_4());
            System.out.println("printing V24_5 : " + csvLine.getV24_5());
            System.out.println("printing V24_6 : " + csvLine.getV24_6());
            System.out.println("printing V24_7 : " + csvLine.getV24_7());
            System.out.println("printing V24_8 : " + csvLine.getV24_8());
            System.out.println("printing V24_9 : " + csvLine.getV24_9());
            System.out.println("printing V24_10 : " + csvLine.getV24_10());

            i++;
            csvLine.setV25_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 25);

            i++;
            csvLine.setV25_2(data[i]);
            i++;
            csvLine.setV25_3(data[i]);
            i++;
            csvLine.setV25_4(data[i]);
            i++;
            csvLine.setV25_5(data[i]);
            i++;
            csvLine.setV25_6(data[i]);
            i++;
            csvLine.setV25_7(data[i]);
            i++;
            csvLine.setV25_8(data[i]);
            i++;
            csvLine.setV25_9(data[i]);
            i++;
            csvLine.setV25_10(data[i]);

            System.out.println("printing V25_1 : " + csvLine.getV25_1());
            System.out.println("printing V25_2 : " + csvLine.getV25_2());
            System.out.println("printing V25_3 : " + csvLine.getV25_3());
            System.out.println("printing V25_4 : " + csvLine.getV25_4());
            System.out.println("printing V25_5 : " + csvLine.getV25_5());
            System.out.println("printing V25_6 : " + csvLine.getV25_6());
            System.out.println("printing V25_7 : " + csvLine.getV25_7());
            System.out.println("printing V25_8 : " + csvLine.getV25_8());
            System.out.println("printing V25_9 : " + csvLine.getV25_9());
            System.out.println("printing V25_10 : " + csvLine.getV25_10());

            i++;
            csvLine.setV26_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 26);

            i++;
            csvLine.setV26_2(data[i]);
            i++;
            csvLine.setV26_3(data[i]);
            i++;
            csvLine.setV26_4(data[i]);
            i++;
            csvLine.setV26_5(data[i]);
            i++;
            csvLine.setV26_6(data[i]);
            i++;
            csvLine.setV26_7(data[i]);
            i++;
            csvLine.setV26_8(data[i]);
            i++;
            csvLine.setV26_9(data[i]);
            i++;
            csvLine.setV26_10(data[i]);

            System.out.println("printing V26_1 : " + csvLine.getV26_1());
            System.out.println("printing V26_2 : " + csvLine.getV26_2());
            System.out.println("printing V26_3 : " + csvLine.getV26_3());
            System.out.println("printing V26_4 : " + csvLine.getV26_4());
            System.out.println("printing V26_5 : " + csvLine.getV26_5());
            System.out.println("printing V26_6 : " + csvLine.getV26_6());
            System.out.println("printing V26_7 : " + csvLine.getV26_7());
            System.out.println("printing V26_8 : " + csvLine.getV26_8());
            System.out.println("printing V26_9 : " + csvLine.getV26_9());
            System.out.println("printing V26_10 : " + csvLine.getV26_10());

            i++;
            csvLine.setV27_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 27);

            i++;
            csvLine.setV27_2(data[i]);
            i++;
            csvLine.setV27_3(data[i]);
            i++;
            csvLine.setV27_4(data[i]);
            i++;
            csvLine.setV27_5(data[i]);
            i++;
            csvLine.setV27_6(data[i]);
            i++;
            csvLine.setV27_7(data[i]);
            i++;
            csvLine.setV27_8(data[i]);
            i++;
            csvLine.setV27_9(data[i]);
            i++;
            csvLine.setV27_10(data[i]);

            System.out.println("printing V27_1 : " + csvLine.getV27_1());
            System.out.println("printing V27_2 : " + csvLine.getV27_2());
            System.out.println("printing V27_3 : " + csvLine.getV27_3());
            System.out.println("printing V27_4 : " + csvLine.getV27_4());
            System.out.println("printing V27_5 : " + csvLine.getV27_5());
            System.out.println("printing V27_6 : " + csvLine.getV27_6());
            System.out.println("printing V27_7 : " + csvLine.getV27_7());
            System.out.println("printing V27_8 : " + csvLine.getV27_8());
            System.out.println("printing V27_9 : " + csvLine.getV27_9());
            System.out.println("printing V27_10 : " + csvLine.getV27_10());


            i++;
            csvLine.setV28_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 28);

            i++;
            csvLine.setV28_2(data[i]);
            i++;
            csvLine.setV28_3(data[i]);
            i++;
            csvLine.setV28_4(data[i]);
            i++;
            csvLine.setV28_5(data[i]);
            i++;
            csvLine.setV28_6(data[i]);
            i++;
            csvLine.setV28_7(data[i]);
            i++;
            csvLine.setV28_8(data[i]);
            i++;
            csvLine.setV28_9(data[i]);
            i++;
            csvLine.setV28_10(data[i]);

            System.out.println("printing V28_1 : " + csvLine.getV28_1());
            System.out.println("printing V28_2 : " + csvLine.getV28_2());
            System.out.println("printing V28_3 : " + csvLine.getV28_3());
            System.out.println("printing V28_4 : " + csvLine.getV28_4());
            System.out.println("printing V28_5 : " + csvLine.getV28_5());
            System.out.println("printing V28_6 : " + csvLine.getV28_6());
            System.out.println("printing V28_7 : " + csvLine.getV28_7());
            System.out.println("printing V28_8 : " + csvLine.getV28_8());
            System.out.println("printing V28_9 : " + csvLine.getV28_9());
            System.out.println("printing V28_10 : " + csvLine.getV28_10());

            i++;
            csvLine.setV29_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 29);

            i++;
            csvLine.setV29_2(data[i]);
            i++;
            csvLine.setV29_3(data[i]);
            i++;
            csvLine.setV29_4(data[i]);
            i++;
            csvLine.setV29_5(data[i]);
            i++;
            csvLine.setV29_6(data[i]);
            i++;
            csvLine.setV29_7(data[i]);
            i++;
            csvLine.setV29_8(data[i]);
            i++;
            csvLine.setV29_9(data[i]);
            i++;
            csvLine.setV29_10(data[i]);

            System.out.println("printing V29_1 : " + csvLine.getV29_1());
            System.out.println("printing V29_2 : " + csvLine.getV29_2());
            System.out.println("printing V29_3 : " + csvLine.getV29_3());
            System.out.println("printing V29_4 : " + csvLine.getV29_4());
            System.out.println("printing V29_5 : " + csvLine.getV29_5());
            System.out.println("printing V29_6 : " + csvLine.getV29_6());
            System.out.println("printing V29_7 : " + csvLine.getV29_7());
            System.out.println("printing V29_8 : " + csvLine.getV29_8());
            System.out.println("printing V29_9 : " + csvLine.getV29_9());
            System.out.println("printing V29_10 : " + csvLine.getV29_10());


            i++;
            csvLine.setV30_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 30);

            i++;
            csvLine.setV30_2(data[i]);
            i++;
            csvLine.setV30_3(data[i]);
            i++;
            csvLine.setV30_4(data[i]);
            i++;
            csvLine.setV30_5(data[i]);
            i++;
            csvLine.setV30_6(data[i]);
            i++;
            csvLine.setV30_7(data[i]);
            i++;
            csvLine.setV30_8(data[i]);
            i++;
            csvLine.setV30_9(data[i]);
            i++;
            csvLine.setV30_10(data[i]);

            System.out.println("printing V30_1 : " + csvLine.getV30_1());
            System.out.println("printing V30_2 : " + csvLine.getV30_2());
            System.out.println("printing V30_3 : " + csvLine.getV30_3());
            System.out.println("printing V30_4 : " + csvLine.getV30_4());
            System.out.println("printing V30_5 : " + csvLine.getV30_5());
            System.out.println("printing V30_6 : " + csvLine.getV30_6());
            System.out.println("printing V30_7 : " + csvLine.getV30_7());
            System.out.println("printing V30_8 : " + csvLine.getV30_8());
            System.out.println("printing V30_9 : " + csvLine.getV30_9());
            System.out.println("printing V30_10 : " + csvLine.getV30_10());

            i++;
            csvLine.setV31_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 31);

            i++;
            csvLine.setV31_2(data[i]);
            i++;
            csvLine.setV31_3(data[i]);
            i++;
            csvLine.setV31_4(data[i]);
            i++;
            csvLine.setV31_5(data[i]);
            i++;
            csvLine.setV31_6(data[i]);
            i++;
            csvLine.setV31_7(data[i]);
            i++;
            csvLine.setV31_8(data[i]);
            i++;
            csvLine.setV31_9(data[i]);
            i++;
            csvLine.setV31_10(data[i]);

            System.out.println("printing V31_1 : " + csvLine.getV31_1());
            System.out.println("printing V31_2 : " + csvLine.getV31_2());
            System.out.println("printing V31_3 : " + csvLine.getV31_3());
            System.out.println("printing V31_4 : " + csvLine.getV31_4());
            System.out.println("printing V31_5 : " + csvLine.getV31_5());
            System.out.println("printing V31_6 : " + csvLine.getV31_6());
            System.out.println("printing V31_7 : " + csvLine.getV31_7());
            System.out.println("printing V31_8 : " + csvLine.getV31_8());
            System.out.println("printing V31_9 : " + csvLine.getV31_9());
            System.out.println("printing V31_10 : " + csvLine.getV31_10());

            i++;
            csvLine.setV32_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 32);

            i++;
            csvLine.setV32_2(data[i]);
            i++;
            csvLine.setV32_3(data[i]);
            i++;
            csvLine.setV32_4(data[i]);
            i++;
            csvLine.setV32_5(data[i]);
            i++;
            csvLine.setV32_6(data[i]);
            i++;
            csvLine.setV32_7(data[i]);
            i++;
            csvLine.setV32_8(data[i]);
            i++;
            csvLine.setV32_9(data[i]);
            i++;
            csvLine.setV32_10(data[i]);

            System.out.println("printing V32_1 : " + csvLine.getV32_1());
            System.out.println("printing V32_2 : " + csvLine.getV32_2());
            System.out.println("printing V32_3 : " + csvLine.getV32_3());
            System.out.println("printing V32_4 : " + csvLine.getV32_4());
            System.out.println("printing V32_5 : " + csvLine.getV32_5());
            System.out.println("printing V32_6 : " + csvLine.getV32_6());
            System.out.println("printing V32_7 : " + csvLine.getV32_7());
            System.out.println("printing V32_8 : " + csvLine.getV32_8());
            System.out.println("printing V32_9 : " + csvLine.getV32_9());
            System.out.println("printing V32_10 : " + csvLine.getV32_10());

            i++;
            csvLine.setV33_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 33);

            i++;
            csvLine.setV33_2(data[i]);
            i++;
            csvLine.setV33_3(data[i]);
            i++;
            csvLine.setV33_4(data[i]);
            i++;
            csvLine.setV33_5(data[i]);
            i++;
            csvLine.setV33_6(data[i]);
            i++;
            csvLine.setV33_7(data[i]);
            i++;
            csvLine.setV33_8(data[i]);
            i++;
            csvLine.setV33_9(data[i]);
            i++;
            csvLine.setV33_10(data[i]);

            System.out.println("printing V33_1 : " + csvLine.getV33_1());
            System.out.println("printing V33_2 : " + csvLine.getV33_2());
            System.out.println("printing V33_3 : " + csvLine.getV33_3());
            System.out.println("printing V33_4 : " + csvLine.getV33_4());
            System.out.println("printing V33_5 : " + csvLine.getV33_5());
            System.out.println("printing V33_6 : " + csvLine.getV33_6());
            System.out.println("printing V33_7 : " + csvLine.getV33_7());
            System.out.println("printing V33_8 : " + csvLine.getV33_8());
            System.out.println("printing V33_9 : " + csvLine.getV33_9());
            System.out.println("printing V33_10 : " + csvLine.getV33_10());

            i++;
            csvLine.setV34_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 34);

            i++;
            csvLine.setV34_2(data[i]);
            i++;
            csvLine.setV34_3(data[i]);
            i++;
            csvLine.setV34_4(data[i]);
            i++;
            csvLine.setV34_5(data[i]);
            i++;
            csvLine.setV34_6(data[i]);
            i++;
            csvLine.setV34_7(data[i]);
            i++;
            csvLine.setV34_8(data[i]);
            i++;
            csvLine.setV34_9(data[i]);
            i++;
            csvLine.setV34_10(data[i]);

            System.out.println("printing V34_1 : " + csvLine.getV34_1());
            System.out.println("printing V34_2 : " + csvLine.getV34_2());
            System.out.println("printing V34_3 : " + csvLine.getV34_3());
            System.out.println("printing V34_4 : " + csvLine.getV34_4());
            System.out.println("printing V34_5 : " + csvLine.getV34_5());
            System.out.println("printing V34_6 : " + csvLine.getV34_6());
            System.out.println("printing V34_7 : " + csvLine.getV34_7());
            System.out.println("printing V34_8 : " + csvLine.getV34_8());
            System.out.println("printing V34_9 : " + csvLine.getV34_9());
            System.out.println("printing V34_10 : " + csvLine.getV34_10());

            i++;
            csvLine.setV35_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 35);

            i++;
            csvLine.setV35_2(data[i]);
            i++;
            csvLine.setV35_3(data[i]);
            i++;
            csvLine.setV35_4(data[i]);
            i++;
            csvLine.setV35_5(data[i]);
            i++;
            csvLine.setV35_6(data[i]);
            i++;
            csvLine.setV35_7(data[i]);
            i++;
            csvLine.setV35_8(data[i]);
            i++;
            csvLine.setV35_9(data[i]);
            i++;
            csvLine.setV35_10(data[i]);

            System.out.println("printing V35_1 : " + csvLine.getV35_1());
            System.out.println("printing V35_2 : " + csvLine.getV35_2());
            System.out.println("printing V35_3 : " + csvLine.getV35_3());
            System.out.println("printing V35_4 : " + csvLine.getV35_4());
            System.out.println("printing V35_5 : " + csvLine.getV35_5());
            System.out.println("printing V35_6 : " + csvLine.getV35_6());
            System.out.println("printing V35_7 : " + csvLine.getV35_7());
            System.out.println("printing V35_8 : " + csvLine.getV35_8());
            System.out.println("printing V35_9 : " + csvLine.getV35_9());
            System.out.println("printing V35_10 : " + csvLine.getV35_10());

            i++;
            csvLine.setV36_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 36);

            i++;
            csvLine.setV36_2(data[i]);
            i++;
            csvLine.setV36_3(data[i]);
            i++;
            csvLine.setV36_4(data[i]);
            i++;
            csvLine.setV36_5(data[i]);
            i++;
            csvLine.setV36_6(data[i]);
            i++;
            csvLine.setV36_7(data[i]);
            i++;
            csvLine.setV36_8(data[i]);
            i++;
            csvLine.setV36_9(data[i]);
            i++;
            csvLine.setV36_10(data[i]);

            System.out.println("printing V36_1 : " + csvLine.getV36_1());
            System.out.println("printing V36_2 : " + csvLine.getV36_2());
            System.out.println("printing V36_3 : " + csvLine.getV36_3());
            System.out.println("printing V36_4 : " + csvLine.getV36_4());
            System.out.println("printing V36_5 : " + csvLine.getV36_5());
            System.out.println("printing V36_6 : " + csvLine.getV36_6());
            System.out.println("printing V36_7 : " + csvLine.getV36_7());
            System.out.println("printing V36_8 : " + csvLine.getV36_8());
            System.out.println("printing V36_9 : " + csvLine.getV36_9());
            System.out.println("printing V36_10 : " + csvLine.getV36_10());

            i++;
            csvLine.setV37_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 37);

            i++;
            csvLine.setV37_2(data[i]);
            i++;
            csvLine.setV37_3(data[i]);
            i++;
            csvLine.setV37_4(data[i]);
            i++;
            csvLine.setV37_5(data[i]);
            i++;
            csvLine.setV37_6(data[i]);
            i++;
            csvLine.setV37_7(data[i]);
            i++;
            csvLine.setV37_8(data[i]);
            i++;
            csvLine.setV37_9(data[i]);
            i++;
            csvLine.setV37_10(data[i]);

            System.out.println("printing V37_1 : " + csvLine.getV37_1());
            System.out.println("printing V37_2 : " + csvLine.getV37_2());
            System.out.println("printing V37_3 : " + csvLine.getV37_3());
            System.out.println("printing V37_4 : " + csvLine.getV37_4());
            System.out.println("printing V37_5 : " + csvLine.getV37_5());
            System.out.println("printing V37_6 : " + csvLine.getV37_6());
            System.out.println("printing V37_7 : " + csvLine.getV37_7());
            System.out.println("printing V37_8 : " + csvLine.getV37_8());
            System.out.println("printing V37_9 : " + csvLine.getV37_9());
            System.out.println("printing V37_10 : " + csvLine.getV37_10());


            i++;
            csvLine.setV38_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 38);

            i++;
            csvLine.setV38_2(data[i]);
            i++;
            csvLine.setV38_3(data[i]);
            i++;
            csvLine.setV38_4(data[i]);
            i++;
            csvLine.setV38_5(data[i]);
            i++;
            csvLine.setV38_6(data[i]);
            i++;
            csvLine.setV38_7(data[i]);
            i++;
            csvLine.setV38_8(data[i]);
            i++;
            csvLine.setV38_9(data[i]);
            i++;
            csvLine.setV38_10(data[i]);

            System.out.println("printing V38_1 : " + csvLine.getV38_1());
            System.out.println("printing V38_2 : " + csvLine.getV38_2());
            System.out.println("printing V38_3 : " + csvLine.getV38_3());
            System.out.println("printing V38_4 : " + csvLine.getV38_4());
            System.out.println("printing V38_5 : " + csvLine.getV38_5());
            System.out.println("printing V38_6 : " + csvLine.getV38_6());
            System.out.println("printing V38_7 : " + csvLine.getV38_7());
            System.out.println("printing V38_8 : " + csvLine.getV38_8());
            System.out.println("printing V38_9 : " + csvLine.getV38_9());
            System.out.println("printing V38_10 : " + csvLine.getV38_10());

            i++;
            csvLine.setV39_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 39);

            i++;
            csvLine.setV39_2(data[i]);
            i++;
            csvLine.setV39_3(data[i]);
            i++;
            csvLine.setV39_4(data[i]);
            i++;
            csvLine.setV39_5(data[i]);
            i++;
            csvLine.setV39_6(data[i]);
            i++;
            csvLine.setV39_7(data[i]);
            i++;
            csvLine.setV39_8(data[i]);
            i++;
            csvLine.setV39_9(data[i]);
            i++;
            csvLine.setV39_10(data[i]);

            System.out.println("printing V39_1 : " + csvLine.getV39_1());
            System.out.println("printing V39_2 : " + csvLine.getV39_2());
            System.out.println("printing V39_3 : " + csvLine.getV39_3());
            System.out.println("printing V39_4 : " + csvLine.getV39_4());
            System.out.println("printing V39_5 : " + csvLine.getV39_5());
            System.out.println("printing V39_6 : " + csvLine.getV39_6());
            System.out.println("printing V39_7 : " + csvLine.getV39_7());
            System.out.println("printing V39_8 : " + csvLine.getV39_8());
            System.out.println("printing V39_9 : " + csvLine.getV39_9());
            System.out.println("printing V39_10 : " + csvLine.getV39_10());


            i++;
            csvLine.setV40_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 40);

            i++;
            csvLine.setV40_2(data[i]);
            i++;
            csvLine.setV40_3(data[i]);
            i++;
            csvLine.setV40_4(data[i]);
            i++;
            csvLine.setV40_5(data[i]);
            i++;
            csvLine.setV40_6(data[i]);
            i++;
            csvLine.setV40_7(data[i]);
            i++;
            csvLine.setV40_8(data[i]);
            i++;
            csvLine.setV40_9(data[i]);
            i++;
            csvLine.setV40_10(data[i]);

            System.out.println("printing V40_1 : " + csvLine.getV40_1());
            System.out.println("printing V40_2 : " + csvLine.getV40_2());
            System.out.println("printing V40_3 : " + csvLine.getV40_3());
            System.out.println("printing V40_4 : " + csvLine.getV40_4());
            System.out.println("printing V40_5 : " + csvLine.getV40_5());
            System.out.println("printing V40_6 : " + csvLine.getV40_6());
            System.out.println("printing V40_7 : " + csvLine.getV40_7());
            System.out.println("printing V40_8 : " + csvLine.getV40_8());
            System.out.println("printing V40_9 : " + csvLine.getV40_9());
            System.out.println("printing V40_10 : " + csvLine.getV40_10());

            i++;
            csvLine.setV41_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 41);

            i++;
            csvLine.setV41_2(data[i]);
            i++;
            csvLine.setV41_3(data[i]);
            i++;
            csvLine.setV41_4(data[i]);
            i++;
            csvLine.setV41_5(data[i]);
            i++;
            csvLine.setV41_6(data[i]);
            i++;
            csvLine.setV41_7(data[i]);
            i++;
            csvLine.setV41_8(data[i]);
            i++;
            csvLine.setV41_9(data[i]);
            i++;
            csvLine.setV41_10(data[i]);

            System.out.println("printing V41_1 : " + csvLine.getV41_1());
            System.out.println("printing V41_2 : " + csvLine.getV41_2());
            System.out.println("printing V41_3 : " + csvLine.getV41_3());
            System.out.println("printing V41_4 : " + csvLine.getV41_4());
            System.out.println("printing V41_5 : " + csvLine.getV41_5());
            System.out.println("printing V41_6 : " + csvLine.getV41_6());
            System.out.println("printing V41_7 : " + csvLine.getV41_7());
            System.out.println("printing V41_8 : " + csvLine.getV41_8());
            System.out.println("printing V41_9 : " + csvLine.getV41_9());
            System.out.println("printing V41_10 : " + csvLine.getV41_10());

            i++;
            csvLine.setV42_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 42);

            i++;
            csvLine.setV42_2(data[i]);
            i++;
            csvLine.setV42_3(data[i]);
            i++;
            csvLine.setV42_4(data[i]);
            i++;
            csvLine.setV42_5(data[i]);
            i++;
            csvLine.setV42_6(data[i]);
            i++;
            csvLine.setV42_7(data[i]);
            i++;
            csvLine.setV42_8(data[i]);
            i++;
            csvLine.setV42_9(data[i]);
            i++;
            csvLine.setV42_10(data[i]);

            System.out.println("printing V42_1 : " + csvLine.getV42_1());
            System.out.println("printing V42_2 : " + csvLine.getV42_2());
            System.out.println("printing V42_3 : " + csvLine.getV42_3());
            System.out.println("printing V42_4 : " + csvLine.getV42_4());
            System.out.println("printing V42_5 : " + csvLine.getV42_5());
            System.out.println("printing V42_6 : " + csvLine.getV42_6());
            System.out.println("printing V42_7 : " + csvLine.getV42_7());
            System.out.println("printing V42_8 : " + csvLine.getV42_8());
            System.out.println("printing V42_9 : " + csvLine.getV42_9());
            System.out.println("printing V42_10 : " + csvLine.getV42_10());

            i++;
            csvLine.setV43_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 43);

            i++;
            csvLine.setV43_2(data[i]);
            i++;
            csvLine.setV43_3(data[i]);
            i++;
            csvLine.setV43_4(data[i]);
            i++;
            csvLine.setV43_5(data[i]);
            i++;
            csvLine.setV43_6(data[i]);
            i++;
            csvLine.setV43_7(data[i]);
            i++;
            csvLine.setV43_8(data[i]);
            i++;
            csvLine.setV43_9(data[i]);
            i++;
            csvLine.setV43_10(data[i]);

            System.out.println("printing V43_1 : " + csvLine.getV43_1());
            System.out.println("printing V43_2 : " + csvLine.getV43_2());
            System.out.println("printing V43_3 : " + csvLine.getV43_3());
            System.out.println("printing V43_4 : " + csvLine.getV43_4());
            System.out.println("printing V43_5 : " + csvLine.getV43_5());
            System.out.println("printing V43_6 : " + csvLine.getV43_6());
            System.out.println("printing V43_7 : " + csvLine.getV43_7());
            System.out.println("printing V43_8 : " + csvLine.getV43_8());
            System.out.println("printing V43_9 : " + csvLine.getV43_9());
            System.out.println("printing V43_10 : " + csvLine.getV43_10());

            i++;
            csvLine.setV44_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 44);

            i++;
            csvLine.setV44_2(data[i]);
            i++;
            csvLine.setV44_3(data[i]);
            i++;
            csvLine.setV44_4(data[i]);
            i++;
            csvLine.setV44_5(data[i]);
            i++;
            csvLine.setV44_6(data[i]);
            i++;
            csvLine.setV44_7(data[i]);
            i++;
            csvLine.setV44_8(data[i]);
            i++;
            csvLine.setV44_9(data[i]);
            i++;
            csvLine.setV44_10(data[i]);

            System.out.println("printing V44_1 : " + csvLine.getV44_1());
            System.out.println("printing V44_2 : " + csvLine.getV44_2());
            System.out.println("printing V44_3 : " + csvLine.getV44_3());
            System.out.println("printing V44_4 : " + csvLine.getV44_4());
            System.out.println("printing V44_5 : " + csvLine.getV44_5());
            System.out.println("printing V44_6 : " + csvLine.getV44_6());
            System.out.println("printing V44_7 : " + csvLine.getV44_7());
            System.out.println("printing V44_8 : " + csvLine.getV44_8());
            System.out.println("printing V44_9 : " + csvLine.getV44_9());
            System.out.println("printing V44_10 : " + csvLine.getV44_10());

            i++;
            csvLine.setV45_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 45);

            i++;
            csvLine.setV45_2(data[i]);
            i++;
            csvLine.setV45_3(data[i]);
            i++;
            csvLine.setV45_4(data[i]);
            i++;
            csvLine.setV45_5(data[i]);
            i++;
            csvLine.setV45_6(data[i]);
            i++;
            csvLine.setV45_7(data[i]);
            i++;
            csvLine.setV45_8(data[i]);
            i++;
            csvLine.setV45_9(data[i]);
            i++;
            csvLine.setV45_10(data[i]);

            System.out.println("printing V45_1 : " + csvLine.getV45_1());
            System.out.println("printing V45_2 : " + csvLine.getV45_2());
            System.out.println("printing V45_3 : " + csvLine.getV45_3());
            System.out.println("printing V45_4 : " + csvLine.getV45_4());
            System.out.println("printing V45_5 : " + csvLine.getV45_5());
            System.out.println("printing V45_6 : " + csvLine.getV45_6());
            System.out.println("printing V45_7 : " + csvLine.getV45_7());
            System.out.println("printing V45_8 : " + csvLine.getV45_8());
            System.out.println("printing V45_9 : " + csvLine.getV45_9());
            System.out.println("printing V45_10 : " + csvLine.getV45_10());

            i++;
            csvLine.setV46_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 46);

            i++;
            csvLine.setV46_2(data[i]);
            i++;
            csvLine.setV46_3(data[i]);
            i++;
            csvLine.setV46_4(data[i]);
            i++;
            csvLine.setV46_5(data[i]);
            i++;
            csvLine.setV46_6(data[i]);
            i++;
            csvLine.setV46_7(data[i]);
            i++;
            csvLine.setV46_8(data[i]);
            i++;
            csvLine.setV46_9(data[i]);
            i++;
            csvLine.setV46_10(data[i]);

            System.out.println("printing V46_1 : " + csvLine.getV46_1());
            System.out.println("printing V46_2 : " + csvLine.getV46_2());
            System.out.println("printing V46_3 : " + csvLine.getV46_3());
            System.out.println("printing V46_4 : " + csvLine.getV46_4());
            System.out.println("printing V46_5 : " + csvLine.getV46_5());
            System.out.println("printing V46_6 : " + csvLine.getV46_6());
            System.out.println("printing V46_7 : " + csvLine.getV46_7());
            System.out.println("printing V46_8 : " + csvLine.getV46_8());
            System.out.println("printing V46_9 : " + csvLine.getV46_9());
            System.out.println("printing V46_10 : " + csvLine.getV46_10());

            i++;
            csvLine.setV47_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 47);

            i++;
            csvLine.setV47_2(data[i]);
            i++;
            csvLine.setV47_3(data[i]);
            i++;
            csvLine.setV47_4(data[i]);
            i++;
            csvLine.setV47_5(data[i]);
            i++;
            csvLine.setV47_6(data[i]);
            i++;
            csvLine.setV47_7(data[i]);
            i++;
            csvLine.setV47_8(data[i]);
            i++;
            csvLine.setV47_9(data[i]);
            i++;
            csvLine.setV47_10(data[i]);

            System.out.println("printing V47_1 : " + csvLine.getV47_1());
            System.out.println("printing V47_2 : " + csvLine.getV47_2());
            System.out.println("printing V47_3 : " + csvLine.getV47_3());
            System.out.println("printing V47_4 : " + csvLine.getV47_4());
            System.out.println("printing V47_5 : " + csvLine.getV47_5());
            System.out.println("printing V47_6 : " + csvLine.getV47_6());
            System.out.println("printing V47_7 : " + csvLine.getV47_7());
            System.out.println("printing V47_8 : " + csvLine.getV47_8());
            System.out.println("printing V47_9 : " + csvLine.getV47_9());
            System.out.println("printing V47_10 : " + csvLine.getV47_10());


            i++;
            csvLine.setV48_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 48);

            i++;
            csvLine.setV48_2(data[i]);
            i++;
            csvLine.setV48_3(data[i]);
            i++;
            csvLine.setV48_4(data[i]);
            i++;
            csvLine.setV48_5(data[i]);
            i++;
            csvLine.setV48_6(data[i]);
            i++;
            csvLine.setV48_7(data[i]);
            i++;
            csvLine.setV48_8(data[i]);
            i++;
            csvLine.setV48_9(data[i]);
            i++;
            csvLine.setV48_10(data[i]);

            System.out.println("printing V48_1 : " + csvLine.getV48_1());
            System.out.println("printing V48_2 : " + csvLine.getV48_2());
            System.out.println("printing V48_3 : " + csvLine.getV48_3());
            System.out.println("printing V48_4 : " + csvLine.getV48_4());
            System.out.println("printing V48_5 : " + csvLine.getV48_5());
            System.out.println("printing V48_6 : " + csvLine.getV48_6());
            System.out.println("printing V48_7 : " + csvLine.getV48_7());
            System.out.println("printing V48_8 : " + csvLine.getV48_8());
            System.out.println("printing V48_9 : " + csvLine.getV48_9());
            System.out.println("printing V48_10 : " + csvLine.getV48_10());

            i++;
            csvLine.setV49_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 49);

            i++;
            csvLine.setV49_2(data[i]);
            i++;
            csvLine.setV49_3(data[i]);
            i++;
            csvLine.setV49_4(data[i]);
            i++;
            csvLine.setV49_5(data[i]);
            i++;
            csvLine.setV49_6(data[i]);
            i++;
            csvLine.setV49_7(data[i]);
            i++;
            csvLine.setV49_8(data[i]);
            i++;
            csvLine.setV49_9(data[i]);
            i++;
            csvLine.setV49_10(data[i]);

            System.out.println("printing V49_1 : " + csvLine.getV49_1());
            System.out.println("printing V49_2 : " + csvLine.getV49_2());
            System.out.println("printing V49_3 : " + csvLine.getV49_3());
            System.out.println("printing V49_4 : " + csvLine.getV49_4());
            System.out.println("printing V49_5 : " + csvLine.getV49_5());
            System.out.println("printing V49_6 : " + csvLine.getV49_6());
            System.out.println("printing V49_7 : " + csvLine.getV49_7());
            System.out.println("printing V49_8 : " + csvLine.getV49_8());
            System.out.println("printing V49_9 : " + csvLine.getV49_9());
            System.out.println("printing V49_10 : " + csvLine.getV49_10());


            i++;
            csvLine.setV50_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 50);

            i++;
            csvLine.setV50_2(data[i]);
            i++;
            csvLine.setV50_3(data[i]);
            i++;
            csvLine.setV50_4(data[i]);
            i++;
            csvLine.setV50_5(data[i]);
            i++;
            csvLine.setV50_6(data[i]);
            i++;
            csvLine.setV50_7(data[i]);
            i++;
            csvLine.setV50_8(data[i]);
            i++;
            csvLine.setV50_9(data[i]);
            i++;
            csvLine.setV50_10(data[i]);

            System.out.println("printing V50_1 : " + csvLine.getV50_1());
            System.out.println("printing V50_2 : " + csvLine.getV50_2());
            System.out.println("printing V50_3 : " + csvLine.getV50_3());
            System.out.println("printing V50_4 : " + csvLine.getV50_4());
            System.out.println("printing V50_5 : " + csvLine.getV50_5());
            System.out.println("printing V50_6 : " + csvLine.getV50_6());
            System.out.println("printing V50_7 : " + csvLine.getV50_7());
            System.out.println("printing V50_8 : " + csvLine.getV50_8());
            System.out.println("printing V50_9 : " + csvLine.getV50_9());
            System.out.println("printing V50_10 : " + csvLine.getV50_10());

            i++;
            csvLine.setV51_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 51);

            i++;
            csvLine.setV51_2(data[i]);
            i++;
            csvLine.setV51_3(data[i]);
            i++;
            csvLine.setV51_4(data[i]);
            i++;
            csvLine.setV51_5(data[i]);
            i++;
            csvLine.setV51_6(data[i]);
            i++;
            csvLine.setV51_7(data[i]);
            i++;
            csvLine.setV51_8(data[i]);
            i++;
            csvLine.setV51_9(data[i]);
            i++;
            csvLine.setV51_10(data[i]);

            System.out.println("printing V51_1 : " + csvLine.getV51_1());
            System.out.println("printing V51_2 : " + csvLine.getV51_2());
            System.out.println("printing V51_3 : " + csvLine.getV51_3());
            System.out.println("printing V51_4 : " + csvLine.getV51_4());
            System.out.println("printing V51_5 : " + csvLine.getV51_5());
            System.out.println("printing V51_6 : " + csvLine.getV51_6());
            System.out.println("printing V51_7 : " + csvLine.getV51_7());
            System.out.println("printing V51_8 : " + csvLine.getV51_8());
            System.out.println("printing V51_9 : " + csvLine.getV51_9());
            System.out.println("printing V51_10 : " + csvLine.getV51_10());

            i++;
            csvLine.setV52_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 52);

            i++;
            csvLine.setV52_2(data[i]);
            i++;
            csvLine.setV52_3(data[i]);
            i++;
            csvLine.setV52_4(data[i]);
            i++;
            csvLine.setV52_5(data[i]);
            i++;
            csvLine.setV52_6(data[i]);
            i++;
            csvLine.setV52_7(data[i]);
            i++;
            csvLine.setV52_8(data[i]);
            i++;
            csvLine.setV52_9(data[i]);
            i++;
            csvLine.setV52_10(data[i]);

            System.out.println("printing V52_1 : " + csvLine.getV52_1());
            System.out.println("printing V52_2 : " + csvLine.getV52_2());
            System.out.println("printing V52_3 : " + csvLine.getV52_3());
            System.out.println("printing V52_4 : " + csvLine.getV52_4());
            System.out.println("printing V52_5 : " + csvLine.getV52_5());
            System.out.println("printing V52_6 : " + csvLine.getV52_6());
            System.out.println("printing V52_7 : " + csvLine.getV52_7());
            System.out.println("printing V52_8 : " + csvLine.getV52_8());
            System.out.println("printing V52_9 : " + csvLine.getV52_9());
            System.out.println("printing V52_10 : " + csvLine.getV52_10());

            i++;
            csvLine.setV53_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 53);

            i++;
            csvLine.setV53_2(data[i]);
            i++;
            csvLine.setV53_3(data[i]);
            i++;
            csvLine.setV53_4(data[i]);
            i++;
            csvLine.setV53_5(data[i]);
            i++;
            csvLine.setV53_6(data[i]);
            i++;
            csvLine.setV53_7(data[i]);
            i++;
            csvLine.setV53_8(data[i]);
            i++;
            csvLine.setV53_9(data[i]);
            i++;
            csvLine.setV53_10(data[i]);

            System.out.println("printing V53_1 : " + csvLine.getV53_1());
            System.out.println("printing V53_2 : " + csvLine.getV53_2());
            System.out.println("printing V53_3 : " + csvLine.getV53_3());
            System.out.println("printing V53_4 : " + csvLine.getV53_4());
            System.out.println("printing V53_5 : " + csvLine.getV53_5());
            System.out.println("printing V53_6 : " + csvLine.getV53_6());
            System.out.println("printing V53_7 : " + csvLine.getV53_7());
            System.out.println("printing V53_8 : " + csvLine.getV53_8());
            System.out.println("printing V53_9 : " + csvLine.getV53_9());
            System.out.println("printing V53_10 : " + csvLine.getV53_10());

            i++;
            csvLine.setV54_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 54);

            i++;
            csvLine.setV54_2(data[i]);
            i++;
            csvLine.setV54_3(data[i]);
            i++;
            csvLine.setV54_4(data[i]);
            i++;
            csvLine.setV54_5(data[i]);
            i++;
            csvLine.setV54_6(data[i]);
            i++;
            csvLine.setV54_7(data[i]);
            i++;
            csvLine.setV54_8(data[i]);
            i++;
            csvLine.setV54_9(data[i]);
            i++;
            csvLine.setV54_10(data[i]);

            System.out.println("printing V54_1 : " + csvLine.getV54_1());
            System.out.println("printing V54_2 : " + csvLine.getV54_2());
            System.out.println("printing V54_3 : " + csvLine.getV54_3());
            System.out.println("printing V54_4 : " + csvLine.getV54_4());
            System.out.println("printing V54_5 : " + csvLine.getV54_5());
            System.out.println("printing V54_6 : " + csvLine.getV54_6());
            System.out.println("printing V54_7 : " + csvLine.getV54_7());
            System.out.println("printing V54_8 : " + csvLine.getV54_8());
            System.out.println("printing V54_9 : " + csvLine.getV54_9());
            System.out.println("printing V54_10 : " + csvLine.getV54_10());

            i++;
            csvLine.setV55_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 55);

            i++;
            csvLine.setV55_2(data[i]);
            i++;
            csvLine.setV55_3(data[i]);
            i++;
            csvLine.setV55_4(data[i]);
            i++;
            csvLine.setV55_5(data[i]);
            i++;
            csvLine.setV55_6(data[i]);
            i++;
            csvLine.setV55_7(data[i]);
            i++;
            csvLine.setV55_8(data[i]);
            i++;
            csvLine.setV55_9(data[i]);
            i++;
            csvLine.setV55_10(data[i]);

            System.out.println("printing V55_1 : " + csvLine.getV55_1());
            System.out.println("printing V55_2 : " + csvLine.getV55_2());
            System.out.println("printing V55_3 : " + csvLine.getV55_3());
            System.out.println("printing V55_4 : " + csvLine.getV55_4());
            System.out.println("printing V55_5 : " + csvLine.getV55_5());
            System.out.println("printing V55_6 : " + csvLine.getV55_6());
            System.out.println("printing V55_7 : " + csvLine.getV55_7());
            System.out.println("printing V55_8 : " + csvLine.getV55_8());
            System.out.println("printing V55_9 : " + csvLine.getV55_9());
            System.out.println("printing V55_10 : " + csvLine.getV55_10());

            i++;
            csvLine.setV56_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 56);

            i++;
            csvLine.setV56_2(data[i]);
            i++;
            csvLine.setV56_3(data[i]);
            i++;
            csvLine.setV56_4(data[i]);
            i++;
            csvLine.setV56_5(data[i]);
            i++;
            csvLine.setV56_6(data[i]);
            i++;
            csvLine.setV56_7(data[i]);
            i++;
            csvLine.setV56_8(data[i]);
            i++;
            csvLine.setV56_9(data[i]);
            i++;
            csvLine.setV56_10(data[i]);

            System.out.println("printing V56_1 : " + csvLine.getV56_1());
            System.out.println("printing V56_2 : " + csvLine.getV56_2());
            System.out.println("printing V56_3 : " + csvLine.getV56_3());
            System.out.println("printing V56_4 : " + csvLine.getV56_4());
            System.out.println("printing V56_5 : " + csvLine.getV56_5());
            System.out.println("printing V56_6 : " + csvLine.getV56_6());
            System.out.println("printing V56_7 : " + csvLine.getV56_7());
            System.out.println("printing V56_8 : " + csvLine.getV56_8());
            System.out.println("printing V56_9 : " + csvLine.getV56_9());
            System.out.println("printing V56_10 : " + csvLine.getV56_10());

            i++;
            csvLine.setV57_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 57);

            i++;
            csvLine.setV57_2(data[i]);
            i++;
            csvLine.setV57_3(data[i]);
            i++;
            csvLine.setV57_4(data[i]);
            i++;
            csvLine.setV57_5(data[i]);
            i++;
            csvLine.setV57_6(data[i]);
            i++;
            csvLine.setV57_7(data[i]);
            i++;
            csvLine.setV57_8(data[i]);
            i++;
            csvLine.setV57_9(data[i]);
            i++;
            csvLine.setV57_10(data[i]);

            System.out.println("printing V57_1 : " + csvLine.getV57_1());
            System.out.println("printing V57_2 : " + csvLine.getV57_2());
            System.out.println("printing V57_3 : " + csvLine.getV57_3());
            System.out.println("printing V57_4 : " + csvLine.getV57_4());
            System.out.println("printing V57_5 : " + csvLine.getV57_5());
            System.out.println("printing V57_6 : " + csvLine.getV57_6());
            System.out.println("printing V57_7 : " + csvLine.getV57_7());
            System.out.println("printing V57_8 : " + csvLine.getV57_8());
            System.out.println("printing V57_9 : " + csvLine.getV57_9());
            System.out.println("printing V57_10 : " + csvLine.getV57_10());


            i++;
            csvLine.setV58_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 58);

            i++;
            csvLine.setV58_2(data[i]);
            i++;
            csvLine.setV58_3(data[i]);
            i++;
            csvLine.setV58_4(data[i]);
            i++;
            csvLine.setV58_5(data[i]);
            i++;
            csvLine.setV58_6(data[i]);
            i++;
            csvLine.setV58_7(data[i]);
            i++;
            csvLine.setV58_8(data[i]);
            i++;
            csvLine.setV58_9(data[i]);
            i++;
            csvLine.setV58_10(data[i]);

            System.out.println("printing V58_1 : " + csvLine.getV58_1());
            System.out.println("printing V58_2 : " + csvLine.getV58_2());
            System.out.println("printing V58_3 : " + csvLine.getV58_3());
            System.out.println("printing V58_4 : " + csvLine.getV58_4());
            System.out.println("printing V58_5 : " + csvLine.getV58_5());
            System.out.println("printing V58_6 : " + csvLine.getV58_6());
            System.out.println("printing V58_7 : " + csvLine.getV58_7());
            System.out.println("printing V58_8 : " + csvLine.getV58_8());
            System.out.println("printing V58_9 : " + csvLine.getV58_9());
            System.out.println("printing V58_10 : " + csvLine.getV58_10());

            i++;
            csvLine.setV59_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 59);

            i++;
            csvLine.setV59_2(data[i]);
            i++;
            csvLine.setV59_3(data[i]);
            i++;
            csvLine.setV59_4(data[i]);
            i++;
            csvLine.setV59_5(data[i]);
            i++;
            csvLine.setV59_6(data[i]);
            i++;
            csvLine.setV59_7(data[i]);
            i++;
            csvLine.setV59_8(data[i]);
            i++;
            csvLine.setV59_9(data[i]);
            i++;
            csvLine.setV59_10(data[i]);

            System.out.println("printing V59_1 : " + csvLine.getV59_1());
            System.out.println("printing V59_2 : " + csvLine.getV59_2());
            System.out.println("printing V59_3 : " + csvLine.getV59_3());
            System.out.println("printing V59_4 : " + csvLine.getV59_4());
            System.out.println("printing V59_5 : " + csvLine.getV59_5());
            System.out.println("printing V59_6 : " + csvLine.getV59_6());
            System.out.println("printing V59_7 : " + csvLine.getV59_7());
            System.out.println("printing V59_8 : " + csvLine.getV59_8());
            System.out.println("printing V59_9 : " + csvLine.getV59_9());
            System.out.println("printing V59_10 : " + csvLine.getV59_10());


            i++;
            csvLine.setV60_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 60);

            i++;
            csvLine.setV60_2(data[i]);
            i++;
            csvLine.setV60_3(data[i]);
            i++;
            csvLine.setV60_4(data[i]);
            i++;
            csvLine.setV60_5(data[i]);
            i++;
            csvLine.setV60_6(data[i]);
            i++;
            csvLine.setV60_7(data[i]);
            i++;
            csvLine.setV60_8(data[i]);
            i++;
            csvLine.setV60_9(data[i]);
            i++;
            csvLine.setV60_10(data[i]);

            System.out.println("printing V60_1 : " + csvLine.getV60_1());
            System.out.println("printing V60_2 : " + csvLine.getV60_2());
            System.out.println("printing V60_3 : " + csvLine.getV60_3());
            System.out.println("printing V60_4 : " + csvLine.getV60_4());
            System.out.println("printing V60_5 : " + csvLine.getV60_5());
            System.out.println("printing V60_6 : " + csvLine.getV60_6());
            System.out.println("printing V60_7 : " + csvLine.getV60_7());
            System.out.println("printing V60_8 : " + csvLine.getV60_8());
            System.out.println("printing V60_9 : " + csvLine.getV60_9());
            System.out.println("printing V60_10 : " + csvLine.getV60_10());

            i++;
            csvLine.setV61_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 61);

            i++;
            csvLine.setV61_2(data[i]);
            i++;
            csvLine.setV61_3(data[i]);
            i++;
            csvLine.setV61_4(data[i]);
            i++;
            csvLine.setV61_5(data[i]);
            i++;
            csvLine.setV61_6(data[i]);
            i++;
            csvLine.setV61_7(data[i]);
            i++;
            csvLine.setV61_8(data[i]);
            i++;
            csvLine.setV61_9(data[i]);
            i++;
            csvLine.setV61_10(data[i]);

            System.out.println("printing V61_1 : " + csvLine.getV61_1());
            System.out.println("printing V61_2 : " + csvLine.getV61_2());
            System.out.println("printing V61_3 : " + csvLine.getV61_3());
            System.out.println("printing V61_4 : " + csvLine.getV61_4());
            System.out.println("printing V61_5 : " + csvLine.getV61_5());
            System.out.println("printing V61_6 : " + csvLine.getV61_6());
            System.out.println("printing V61_7 : " + csvLine.getV61_7());
            System.out.println("printing V61_8 : " + csvLine.getV61_8());
            System.out.println("printing V61_9 : " + csvLine.getV61_9());
            System.out.println("printing V61_10 : " + csvLine.getV61_10());

            i++;
            csvLine.setV62_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 62);

            i++;
            csvLine.setV62_2(data[i]);
            i++;
            csvLine.setV62_3(data[i]);
            i++;
            csvLine.setV62_4(data[i]);
            i++;
            csvLine.setV62_5(data[i]);
            i++;
            csvLine.setV62_6(data[i]);
            i++;
            csvLine.setV62_7(data[i]);
            i++;
            csvLine.setV62_8(data[i]);
            i++;
            csvLine.setV62_9(data[i]);
            i++;
            csvLine.setV62_10(data[i]);

            System.out.println("printing V62_1 : " + csvLine.getV62_1());
            System.out.println("printing V62_2 : " + csvLine.getV62_2());
            System.out.println("printing V62_3 : " + csvLine.getV62_3());
            System.out.println("printing V62_4 : " + csvLine.getV62_4());
            System.out.println("printing V62_5 : " + csvLine.getV62_5());
            System.out.println("printing V62_6 : " + csvLine.getV62_6());
            System.out.println("printing V62_7 : " + csvLine.getV62_7());
            System.out.println("printing V62_8 : " + csvLine.getV62_8());
            System.out.println("printing V62_9 : " + csvLine.getV62_9());
            System.out.println("printing V62_10 : " + csvLine.getV62_10());

            i++;
            csvLine.setV63_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 63);

            i++;
            csvLine.setV63_2(data[i]);
            i++;
            csvLine.setV63_3(data[i]);
            i++;
            csvLine.setV63_4(data[i]);
            i++;
            csvLine.setV63_5(data[i]);
            i++;
            csvLine.setV63_6(data[i]);
            i++;
            csvLine.setV63_7(data[i]);
            i++;
            csvLine.setV63_8(data[i]);
            i++;
            csvLine.setV63_9(data[i]);
            i++;
            csvLine.setV63_10(data[i]);

            System.out.println("printing V63_1 : " + csvLine.getV63_1());
            System.out.println("printing V63_2 : " + csvLine.getV63_2());
            System.out.println("printing V63_3 : " + csvLine.getV63_3());
            System.out.println("printing V63_4 : " + csvLine.getV63_4());
            System.out.println("printing V63_5 : " + csvLine.getV63_5());
            System.out.println("printing V63_6 : " + csvLine.getV63_6());
            System.out.println("printing V63_7 : " + csvLine.getV63_7());
            System.out.println("printing V63_8 : " + csvLine.getV63_8());
            System.out.println("printing V63_9 : " + csvLine.getV63_9());
            System.out.println("printing V63_10 : " + csvLine.getV63_10());

            i++;
            csvLine.setV64_1(data[i]);

            csvLine.setVignetteFilled(i, data[i], 64);

            i++;
            csvLine.setV64_2(data[i]);
            i++;
            csvLine.setV64_3(data[i]);
            i++;
            csvLine.setV64_4(data[i]);
            i++;
            csvLine.setV64_5(data[i]);
            i++;
            csvLine.setV64_6(data[i]);
            i++;
            csvLine.setV64_7(data[i]);
            i++;
            csvLine.setV64_8(data[i]);
            i++;
            csvLine.setV64_9(data[i]);
            i++;
            csvLine.setV64_10(data[i]);

            System.out.println("printing V64_1 : " + csvLine.getV64_1());
            System.out.println("printing V64_2 : " + csvLine.getV64_2());
            System.out.println("printing V64_3 : " + csvLine.getV64_3());
            System.out.println("printing V64_4 : " + csvLine.getV64_4());
            System.out.println("printing V64_5 : " + csvLine.getV64_5());
            System.out.println("printing V64_6 : " + csvLine.getV64_6());
            System.out.println("printing V64_7 : " + csvLine.getV64_7());
            System.out.println("printing V64_8 : " + csvLine.getV64_8());
            System.out.println("printing V64_9 : " + csvLine.getV64_9());
            System.out.println("printing V64_10 : " + csvLine.getV64_10());





            System.out.println("=======================");
            System.out.println(line);
            System.out.println("=======================");

            if(index > 0) {

                String newLine1 = csvLine.createNewFileLine_1(index);
                String newLine2 = csvLine.createNewFileLine_2(index);
                String newLine3 = csvLine.createNewFileLine_3(index);
                String newLine4 = csvLine.createNewFileLine_4(index);
                String newLine5 = csvLine.createNewFileLine_5(index);
                String newLine6 = csvLine.createNewFileLine_6(index);


                writer.println(newLine1);
                writer.println(newLine2);
                writer.println(newLine3);
                writer.println(newLine4);
                writer.println(newLine5);
                writer.println(newLine6);

            }

            index++;
            System.out.println("Index raised to " + index);
        }
        csvReader.close();

    }


}
